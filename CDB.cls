VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public rs As ADODB.Recordset

Public Sub MoveFirst()
  rs.MoveFirst
End Sub

Public Sub MoveNext()
  rs.MoveNext
End Sub

Public Property Get EOF() As Long
  If Not (rs Is Nothing) Then
    EOF = rs.EOF
  Else
    EOF = -1
  End If
End Property

Public Property Get BOF() As Long
  If Not (rs Is Nothing) Then
    BOF = rs.BOF
  Else
    BOF = -1
  End If
End Property

Public Property Get State() As Long
  If Not (rs Is Nothing) Then
    State = CLng(rs.State)
  Else
    State = -1
  End If
End Property


Public Property Get RecordCount() As Long
  If Not (rs Is Nothing) Then
    RecordCount = CLng(rs.RecordCount)
  Else
    RecordCount = -1
  End If
End Property

Public Sub Update()
  rs.Update
End Sub

Public Sub ConnectDB()
  
  If Conn Is Nothing Then Set Conn = New ADODB.Connection
  
  If Conn.State = 0 Then
    Set Conn = New ADODB.Connection
  
    Conn.ConnectionString = "Provider=MSDASQL.1;Persist Security Info=False;Mode=ReadWrite;" & _
      "Extended Properties=""DBQ=" & App.Path & "\WorldBuilder.mdb;DefaultDir=" & App.Path & ";" & _
      "Driver={Microsoft Access Driver (*.mdb)};DriverId=25;FIL=MS Access;" & _
      "FILEDSN=" & App.Path & "\WorldBuilder.dsn;MaxBufferSize=2048;MaxScanRows=8;" & _
      "PageTimeout=5;SafeTransactions=0;Threads=3;UID=admin;UserCommitSync=Yes;"";" & _
      "Initial Catalog=" & App.Path & "\WorldBuilder"
  
    Conn.Open
  End If
End Sub

Public Sub OpenDB(strSQL As String, Optional blnReadWrite As Boolean = False)
  
  ' check for connection
  If Conn Is Nothing Then ConnectDB
  
  If Conn.State > 0 Then
    If rs Is Nothing Then Set rs = New ADODB.Recordset
    
    ' close the recordset if it's already open
    If rs.State > 0 Then
      rs.Close
    End If
    
    If blnReadWrite = False Then
      rs.Open strSQL, Conn, adOpenStatic, adLockReadOnly
    Else
      rs.Open strSQL, Conn, adOpenStatic, adLockOptimistic
    End If
  End If
End Sub

Public Sub CloseDB()
  If rs.State > 0 Then
    rs.Close
  End If
End Sub

Private Sub Class_Initialize()
  If rs Is Nothing Then Set rs = New ADODB.Recordset
  ConnectDB
End Sub

Private Sub Class_Terminate()
  If Not (rs Is Nothing) Then
    If rs.State > 0 Then
      rs.Close
    End If
  End If
  Set rs = Nothing
  If Not (Conn Is Nothing) Then
    If Conn.State > 0 Then
      Conn.Close
    End If
    Set Conn = Nothing
  End If
End Sub
