VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCharacter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mvarstrName As String
Private mvarstrRace As String
Private mvarstrAlignment As String
Private mvarenuAlignment As Byte
Private mvarenuSize As Byte
Private mvarlngHeightIn As Long
Private mvarlngAgeYr As Long
Private mvarlngWtLb As Long
Private mvarrgbEyes As Long
Private mvarrgbHair As Long
Private mvarrgbSkin As Long
Private mvarbytDex As Byte
Private mvarenuGender As Byte
Private mvarbytCon As Byte
Private mvarbytInt As Byte
Private mvarbytStr As Byte
Private mvarbytWis As Byte
Private mvarbytCha As Byte
Private mvarintHP As Integer
Private mvarbytAC As Byte
Private mvarbytSpeed As Byte
Private mvarbytFort As Byte
Private mvarbytRef As Byte
Private mvarbytWill As Byte
Private mvarbytBAB As Byte
Private mvarbytSR As Byte
Private mvarbytGrapple As Byte
Private mvarlngXP As Long
Private mvarbytCharacterLevel As Byte

Private mvarstrClassList() As String
Private mvarbytClassLevelList() As Byte
Private mvarstrFeatList() As String

Public Function GetClassList() As String()
  GetClassList = mvarstrClassList
End Function
Public Sub SetClassList(v() As String)
  mvarstrClassList = v
End Sub

Public Function GetClassLevelList() As Byte()
  GetClassLevelList = mvarbytClassLevelList
End Function
Public Sub SetClassLevelList(v() As Byte)
  mvarbytClassLevelList = v
End Sub

Public Function GetFeatList() As String()
  GetFeatList = mvarstrFeatList
End Function
Public Sub SetFeatList(v() As String)
  mvarstrFeatList = v
End Sub


Public Property Let bytCharacterLevel(ByVal vData As Byte)
    mvarbytCharacterLevel = vData
End Property
Public Property Get bytCharacterLevel() As Byte
    bytCharacterLevel = mvarbytCharacterLevel
End Property



Public Property Let lngXP(ByVal vData As Long)
    mvarlngXP = vData
End Property
Public Property Get lngXP() As Long
    lngXP = mvarlngXP
End Property



Public Property Let bytGrapple(ByVal vData As Byte)
    mvarbytGrapple = vData
End Property
Public Property Get bytGrapple() As Byte
    bytGrapple = mvarbytGrapple
End Property



Public Property Let bytSR(ByVal vData As Byte)
    mvarbytSR = vData
End Property
Public Property Get bytSR() As Byte
    bytSR = mvarbytSR
End Property



Public Property Let bytBAB(ByVal vData As Byte)
    mvarbytBAB = vData
End Property
Public Property Get bytBAB() As Byte
    bytBAB = mvarbytBAB
End Property



Public Property Let bytWill(ByVal vData As Byte)
    mvarbytWill = vData
End Property
Public Property Get bytWill() As Byte
    bytWill = mvarbytWill
End Property



Public Property Let bytRef(ByVal vData As Byte)
    mvarbytRef = vData
End Property
Public Property Get bytRef() As Byte
    bytRef = mvarbytRef
End Property



Public Property Let bytFort(ByVal vData As Byte)
    mvarbytFort = vData
End Property
Public Property Get bytFort() As Byte
    bytFort = mvarbytFort
End Property



Public Property Let bytSpeed(ByVal vData As Byte)
    mvarbytSpeed = vData
End Property
Public Property Get bytSpeed() As Byte
    bytSpeed = mvarbytSpeed
End Property



Public Property Let bytAC(ByVal vData As Byte)
    mvarbytAC = vData
End Property
Public Property Get bytAC() As Byte
    bytAC = mvarbytAC
End Property



Public Property Let intHP(ByVal vData As Integer)
    mvarintHP = vData
End Property
Public Property Get intHP() As Integer
    intHP = mvarintHP
End Property



Public Property Let bytCha(ByVal vData As Byte)
    mvarbytCha = vData
End Property
Public Property Get bytCha() As Byte
    bytCha = mvarbytCha
End Property



Public Property Let bytWis(ByVal vData As Byte)
    mvarbytWis = vData
End Property
Public Property Get bytWis() As Byte
    bytWis = mvarbytWis
End Property



Public Property Let bytStr(ByVal vData As Byte)
    mvarbytStr = vData
End Property
Public Property Get bytStr() As Byte
    bytStr = mvarbytStr
End Property



Public Property Let bytInt(ByVal vData As Byte)
    mvarbytInt = vData
End Property
Public Property Get bytInt() As Byte
    bytInt = mvarbytInt
End Property



Public Property Let bytCon(ByVal vData As Byte)
    mvarbytCon = vData
End Property
Public Property Get bytCon() As Byte
    bytCon = mvarbytCon
End Property



Public Property Let enuGender(ByVal vData As EGender)
    mvarenuGender = CByte(vData)
End Property
Public Property Get enuGender() As EGender
    enuGender = mvarenuGender
End Property



Public Property Let bytDex(ByVal vData As Byte)
    mvarbytDex = vData
End Property
Public Property Get bytDex() As Byte
    bytDex = mvarbytDex
End Property



Public Property Let rgbSkin(ByVal vData As Long)
    mvarrgbSkin = vData
End Property
Public Property Get rgbSkin() As Long
    rgbSkin = mvarrgbSkin
End Property



Public Property Let rgbHair(ByVal vData As Long)
    mvarrgbHair = vData
End Property
Public Property Get rgbHair() As Long
    rgbHair = mvarrgbHair
End Property



Public Property Let rgbEyes(ByVal vData As Long)
    mvarrgbEyes = vData
End Property
Public Property Get rgbEyes() As Long
    rgbEyes = mvarrgbEyes
End Property



Public Property Let lngWtLb(ByVal vData As Long)
    mvarlngWtLb = vData
End Property
Public Property Get lngWtLb() As Long
    lngWtLb = mvarlngWtLb
End Property



Public Property Let lngAgeYr(ByVal vData As Long)
    mvarlngAgeYr = vData
End Property
Public Property Get lngAgeYr() As Long
    lngAgeYr = mvarlngAgeYr
End Property



Public Property Let lngHeightIn(ByVal vData As Long)
    mvarlngHeightIn = vData
End Property
Public Property Get lngHeightIn() As Long
    lngHeightIn = mvarlngHeightIn
End Property



Public Property Let enuSize(ByVal vData As ESize)
    mvarenuSize = CByte(vData)
End Property
Public Property Get enuSize() As ESize
    enuSize = mvarenuSize
End Property



Public Property Let enuAlignment(ByVal vData As EAlignment)
    mvarenuAlignment = CByte(vData)
End Property
Public Property Get enuAlignment() As EAlignment
    enuAlignment = mvarenuAlignment
End Property



Public Property Let strAlignment(ByVal vData As String)
    mvarstrAlignment = vData
End Property
Public Property Get strAlignment() As String
    strAlignment = mvarstrAlignment
End Property



Public Property Let strRace(ByVal vData As String)
    mvarstrRace = vData
End Property
Public Property Get strRace() As String
    strRace = mvarstrRace
End Property



Public Property Let strName(ByVal vData As String)
    mvarstrName = vData
End Property
Public Property Get strName() As String
    strName = mvarstrName
End Property



