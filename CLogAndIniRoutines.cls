VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLogAndIniRoutines"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
'****************************************************************************************
Private Const ClassName = "CLogAndIniRoutines"
Private Const Description = "Common routines for writing log files and reading ini files."
'   Description:
'   Implemented Interfaces:
'   Referenced Components:
'
'   Public Properties:
'       Name                    Access    Description
'       -------------------     ------    ---------------------------------------------
'       bWriteLog Boolean If FALSE, writeLogEntry only displays the message in the debug window.
'       strCommentCharacter  String The character used to denote comments in the ini file. For the OSMD-CAS, this value is "#".
'       strIniFileName  String  The name of the ini file.
'       strLogFileName  String  The name of the log file.
'
'   Public Methods:
'       Name                    Argument    Description
'       -------------------     --------    ---------------------------------------------
'       extractValueFromIniFile strSearch   This searches the Ini file for "strSearch=" and returns all characters on that line until a comment character is reached.
'       FileExist               strFileName Returns TRUE if the file exists.
'       writeLogEntry           strMessage  Appends strMessage to the end of the Log file.
'
'   History:
'       Date            Init.   Version/Rev Description
'       -----------     -----   ----------- ---------------------------------------------
'
'****************************************************************************************
' Change History
'----------------------------------------------------------------------------------------
' Change ID:
' Initials:
' Func/Proc:
' Description:
' Date:
' Version/Rev:

'local variable(s) to hold property value(s)
Private mvarstrIniFileName As String 'local copy
Private mvarstrLogFileName As String 'local copy
Private mvarstrCommentCharacter As String 'local copy
Private mvarbWriteLog As Boolean 'local copy


Public Property Let bWriteLog(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bWriteLog = 5
    mvarbWriteLog = vData
End Property


Public Property Get bWriteLog() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bWriteLog
    bWriteLog = mvarbWriteLog
End Property





Public Property Let strCommentCharacter(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strCommentCharacter = 5
    mvarstrCommentCharacter = vData
End Property


Public Property Get strCommentCharacter() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strCommentCharacter
    strCommentCharacter = mvarstrCommentCharacter
End Property





Public Property Let strLogFileName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strLogFileName = 5
    mvarstrLogFileName = vData
End Property


Public Property Get strLogFileName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strLogFileName
    strLogFileName = mvarstrLogFileName
End Property



Public Property Let strIniFileName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strIniFileName = 5
    mvarstrIniFileName = vData
End Property


Public Property Get strIniFileName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strIniFileName
    strIniFileName = mvarstrIniFileName
End Property


Public Function FileExist(ByVal strFileName As String) As Boolean
  
  On Error GoTo errDoesFileExist
  
  'check to see if the file exist in the specified directory
  'first check to see if strFileName is just a directory.
  'directories will return true
  If Mid(strFileName, Len(strFileName)) = "\" Then
    'this is just a directory
    FileExist = False
    Exit Function
  End If
  
  'then check to see if the file exists.
  FileExist = IIf(Dir$(strFileName) <> "", True, False)

  Exit Function

errDoesFileExist:

End Function

Public Function DirExist(ByVal strFileName As String) As Boolean
  
  On Error GoTo errDoesFileExist
  
  'check to see if the file exist in the specified directory
  'first check to see if strFileName is just a directory.
  'directories will return true
  
  'then check to see if the file exists.
  DirExist = IIf(Dir$(strFileName, vbDirectory) <> "", True, False)

  Exit Function

errDoesFileExist:
End Function

Public Sub writeLogEntry(strMessage As String)
  ' displays a message  in teh immediate pane, or adds it to the log file
  If mvarbWriteLog = True Then
    Dim ts As Object
    Dim fso As Object
    
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set ts = fso.OpenTextFile(mvarstrLogFileName, 8, True)
      
    ' write the line to the log file
    ts.WriteLine strMessage
    ts.Close
  Else
    Debug.Print strMessage
  End If
End Sub

Private Function ParseIniFileString(strIniLine As String, strVariableName As String) As String
  ' extracts the value as a string from the ini file string of the format _
    " variablename=value # comment "
  Dim offset As Long, strDiff As Long
  Dim commentOffset As Long
  Dim strIniLine2 As String, strVariableName2 As String
  
  strIniLine2 = Trim(strIniLine)
  strVariableName2 = Trim(strVariableName)
  offset = Len(strVariableName2) + 1
  strDiff = StrComp(LEFT(strIniLine2, Len(strVariableName2)), strVariableName2)
  If strDiff = 0 Then
    commentOffset = InStr(offset, strIniLine2, mvarstrCommentCharacter)
    If commentOffset > 0 Then
      ParseIniFileString = Trim(Mid(strIniLine2, offset + 1, commentOffset - offset - 2))
    Else
      ParseIniFileString = Trim(Mid(strIniLine2, offset, Len(strIniLine2) - offset + 1))
    End If
  End If
End Function

Public Function extractValueFromIniFile(ByVal strSearch As String) As String
  
  Dim strTemp As String
  Dim fso As Object, ts As Object
  Dim strBasePath As String
  
  ' append an "=" sign if it's not present
  If Right(strSearch, 1) <> "=" Then
    strSearch = strSearch & "="
  End If
  
  ' searches the osmd-cas.ini file and returns the value
  If FileExist(mvarstrIniFileName) = True Then
    Set fso = CreateObject("Scripting.FileSystemObject")
  
    ' read the computer name
    Set ts = fso.OpenTextFile(mvarstrIniFileName)
    Do
      strBasePath = ts.Readline
    Loop Until StrComp(UCase(LEFT(strBasePath, Len(strSearch))), UCase(strSearch)) = 0 Or ts.AtEndOfStream = True
    extractValueFromIniFile = ParseIniFileString(strBasePath, strSearch)
    ts.Close
    
    ' remove any comments
    
    ' read the log flag
    Set ts = Nothing
  Else
    Debug.Print "File " & mvarstrIniFileName & " is missing.", vbCritical
    extractValueFromIniFile = ""
  End If
  Set fso = Nothing
End Function


Private Sub Class_Initialize()
  mvarbWriteLog = False
  mvarstrCommentCharacter = "#"
End Sub

