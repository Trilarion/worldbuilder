VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDungeonRoom"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvarstrTrapName As String 'local copy
Private mvarstrEncounterName As String 'local copy
Private mvarbytNumMinorFeatures As Byte 'local copy
Private mvarbytNumMajorFeatures As Byte 'local copy
Private mvarstrMinorFeatures() As String
Private mvarstrMajorFeatures() As String
Private mvarobjTreasure As New CTreasureHoards
Private mvarMonsterEncounters As CMonsterEncounters 'local copy

Public Property Set MonsterEncounters(ByVal vData As CMonsterEncounters)
'used when assigning an Object to the property, on the left side of a Set statement.
'Syntax: Set x.MonsterEncounters = Form1
    Set mvarMonsterEncounters = vData
End Property


Public Property Get MonsterEncounters() As CMonsterEncounters
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.MonsterEncounters
    Set MonsterEncounters = mvarMonsterEncounters
End Property




Public Property Set Treasure(vData As CTreasureHoards)
  Set mvarobjTreasure = vData
End Property
Public Property Get Treasure() As CTreasureHoards
  Set Treasure = mvarobjTreasure
End Property

Public Function GetMinorFeatures(inx As Long) As String
  GetMinorFeatures = mvarstrMinorFeatures(inx)
End Function

Public Function GetMajorFeatures(inx As Long) As String
  GetMajorFeatures = mvarstrMajorFeatures(inx)
End Function

Public Sub SetMinorFeatures(strFeatures() As String)
  mvarbytNumMinorFeatures = UBound(strFeatures)
  mvarstrMinorFeatures = strFeatures
End Sub

Public Sub SetMajorFeatures(strFeatures() As String)
  mvarbytNumMajorFeatures = UBound(strFeatures)
  mvarstrMajorFeatures = strFeatures
End Sub


Public Property Let bytNumMajorFeatures(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytNumMajorFeatures = 5
    mvarbytNumMajorFeatures = vData
End Property


Public Property Get bytNumMajorFeatures() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytNumMajorFeatures
    bytNumMajorFeatures = mvarbytNumMajorFeatures
End Property



Public Property Let bytNumMinorFeatures(ByVal vData As Byte)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.bytNumMinorFeatures = 5
    mvarbytNumMinorFeatures = vData
End Property


Public Property Get bytNumMinorFeatures() As Byte
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.bytNumMinorFeatures
    bytNumMinorFeatures = mvarbytNumMinorFeatures
End Property



Public Property Let strEncounterName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strEncounterName = 5
    mvarstrEncounterName = vData
End Property


Public Property Get strEncounterName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strEncounterName
    strEncounterName = mvarstrEncounterName
End Property



Public Property Let strTrapName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.strTrapName = 5
    mvarstrTrapName = vData
End Property


Public Property Get strTrapName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.strTrapName
    strTrapName = mvarstrTrapName
End Property



