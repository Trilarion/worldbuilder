VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPlate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'local variable(s) to hold property value(s)
Private mvardx As Long
Private mvardy As Long
Private mvarodx As Long
Private mvarody As Long
Private mvarrx As Long
Private mvarry As Long
Private mvarage As Long
Private mvararea As Long
Private mvarid As Long
Private mvarNextPlate As Long 'local copy

Public Property Let NextPlate(ByVal vData As Long)
    mvarNextPlate = vData
End Property


Public Property Get NextPlate() As Long
    NextPlate = mvarNextPlate
End Property




Public Property Let id(ByVal vData As Long)
    mvarid = vData
End Property

Public Property Get id() As Long
    id = mvarid
End Property



Public Property Let area(ByVal vData As Long)
    mvararea = vData
End Property

Public Property Get area() As Long
    area = mvararea
End Property



Public Property Let age(ByVal vData As Long)
    mvarage = vData
End Property

Public Property Get age() As Long
    age = mvarage
End Property



Public Property Let ry(ByVal vData As Long)
    mvarry = vData
End Property

Public Property Get ry() As Long
    ry = mvarry
End Property



Public Property Let rx(ByVal vData As Long)
    mvarrx = vData
End Property

Public Property Get rx() As Long
    rx = mvarrx
End Property



Public Property Let ody(ByVal vData As Long)
    mvarody = vData
End Property

Public Property Get ody() As Long
    ody = mvarody
End Property



Public Property Let odx(ByVal vData As Long)
    mvarodx = vData
End Property

Public Property Get odx() As Long
    odx = mvarodx
End Property



Public Property Let dy(ByVal vData As Long)
    mvardy = vData
End Property

Public Property Get dy() As Long
    dy = mvardy
End Property



Public Property Let dx(ByVal vData As Long)
    mvardx = vData
End Property

Public Property Get dx() As Long
    dx = mvardx
End Property



