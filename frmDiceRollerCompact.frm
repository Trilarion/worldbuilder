VERSION 5.00
Begin VB.Form frmDiceRollerCompact 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "New Character"
   ClientHeight    =   3240
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7440
   Icon            =   "frmDiceRollerCompact.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   3240
   ScaleWidth      =   7440
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame4 
      Caption         =   "Suggested Scores"
      Height          =   1875
      Left            =   3660
      TabIndex        =   24
      Top             =   0
      Width           =   3555
      Begin VB.Label lblMod 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "+0"
         Height          =   255
         Left            =   1260
         TabIndex        =   38
         Top             =   240
         Width           =   315
      End
      Begin VB.Label lblSuggested 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Index           =   5
         Left            =   900
         TabIndex        =   36
         Top             =   1440
         Width           =   300
      End
      Begin VB.Label lblSuggested 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Index           =   4
         Left            =   900
         TabIndex        =   35
         Top             =   1200
         Width           =   300
      End
      Begin VB.Label lblSuggested 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Index           =   3
         Left            =   900
         TabIndex        =   34
         Top             =   960
         Width           =   300
      End
      Begin VB.Label lblSuggested 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Index           =   2
         Left            =   900
         TabIndex        =   33
         Top             =   720
         Width           =   300
      End
      Begin VB.Label lblSuggested 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Index           =   1
         Left            =   900
         TabIndex        =   32
         Top             =   480
         Width           =   300
      End
      Begin VB.Label lblSuggested 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0"
         Height          =   255
         Index           =   0
         Left            =   900
         TabIndex        =   31
         Top             =   240
         Width           =   300
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "CHA"
         Height          =   195
         Index           =   5
         Left            =   360
         TabIndex        =   30
         Top             =   1470
         Width           =   330
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "WIS"
         Height          =   195
         Index           =   4
         Left            =   360
         TabIndex        =   29
         Top             =   1230
         Width           =   330
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "INT"
         Height          =   195
         Index           =   3
         Left            =   360
         TabIndex        =   28
         Top             =   990
         Width           =   330
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "CON"
         Height          =   195
         Index           =   2
         Left            =   360
         TabIndex        =   27
         Top             =   750
         Width           =   330
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "DEX"
         Height          =   195
         Index           =   1
         Left            =   360
         TabIndex        =   26
         Top             =   510
         Width           =   330
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "STR"
         Height          =   195
         Index           =   0
         Left            =   360
         TabIndex        =   25
         Top             =   270
         Width           =   330
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Array"
      Height          =   1875
      Left            =   1980
      TabIndex        =   17
      Top             =   0
      Width           =   1635
      Begin VB.OptionButton optArray 
         Caption         =   "Deific array"
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   22
         Top             =   1440
         Width           =   1395
      End
      Begin VB.OptionButton optArray 
         Caption         =   "Elite array"
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   21
         Top             =   1140
         Width           =   1395
      End
      Begin VB.OptionButton optArray 
         Caption         =   "Nonelite array"
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   20
         Top             =   840
         Width           =   1335
      End
      Begin VB.OptionButton optArray 
         Caption         =   "Base array"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   19
         Top             =   540
         Width           =   1275
      End
      Begin VB.OptionButton optArray 
         Caption         =   "4d6 drop low"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Value           =   -1  'True
         Width           =   1395
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Starting Gold"
      Height          =   1275
      Left            =   60
      TabIndex        =   12
      Top             =   1920
      Width           =   1875
      Begin VB.TextBox txtLevel 
         Height          =   285
         Left            =   600
         TabIndex        =   16
         Text            =   "1"
         Top             =   600
         Width           =   1155
      End
      Begin VB.ComboBox cboClass 
         Height          =   315
         ItemData        =   "frmDiceRollerCompact.frx":014A
         Left            =   120
         List            =   "frmDiceRollerCompact.frx":018A
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   240
         Width           =   1635
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "Level:"
         Height          =   195
         Left            =   120
         TabIndex        =   15
         Top             =   600
         Width           =   435
      End
      Begin VB.Label lblGP 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "0 gp"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   900
         Width           =   1635
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ability Scores"
      Height          =   1875
      Left            =   60
      TabIndex        =   1
      Top             =   0
      Width           =   1875
      Begin VB.ComboBox txtMinModifier 
         Height          =   315
         ItemData        =   "frmDiceRollerCompact.frx":0242
         Left            =   1260
         List            =   "frmDiceRollerCompact.frx":024F
         Style           =   2  'Dropdown List
         TabIndex        =   37
         Top             =   1500
         Width           =   555
      End
      Begin VB.TextBox txtMinPts 
         Height          =   285
         Left            =   1380
         TabIndex        =   2
         Text            =   "0"
         Top             =   1080
         Width           =   315
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Min. Modifier"
         Height          =   195
         Left            =   180
         TabIndex        =   23
         Top             =   1500
         Width           =   900
      End
      Begin VB.Label lblRoll 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "10"
         Height          =   255
         Index           =   0
         Left            =   180
         TabIndex        =   11
         Top             =   240
         Width           =   240
      End
      Begin VB.Label lblRoll 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "10"
         Height          =   255
         Index           =   1
         Left            =   450
         TabIndex        =   10
         Top             =   240
         Width           =   240
      End
      Begin VB.Label lblRoll 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "10"
         Height          =   255
         Index           =   2
         Left            =   735
         TabIndex        =   9
         Top             =   240
         Width           =   240
      End
      Begin VB.Label lblRoll 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "10"
         Height          =   255
         Index           =   3
         Left            =   1005
         TabIndex        =   8
         Top             =   240
         Width           =   240
      End
      Begin VB.Label lblRoll 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "10"
         Height          =   255
         Index           =   4
         Left            =   1290
         TabIndex        =   7
         Top             =   240
         Width           =   240
      End
      Begin VB.Label lblRoll 
         BorderStyle     =   1  'Fixed Single
         Caption         =   "10"
         Height          =   255
         Index           =   5
         Left            =   1560
         TabIndex        =   6
         Top             =   240
         Width           =   240
      End
      Begin VB.Label lblTotalPts 
         Caption         =   "Total Points: 0"
         Height          =   195
         Left            =   180
         TabIndex        =   5
         Top             =   540
         Width           =   1620
      End
      Begin VB.Label lblHighestMod 
         Caption         =   "Highest Modifier: 0"
         Height          =   195
         Left            =   180
         TabIndex        =   4
         Top             =   780
         Width           =   1620
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Min. Stat Total:"
         Height          =   195
         Left            =   180
         TabIndex        =   3
         Top             =   1125
         Width           =   1080
      End
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Roll"
      Height          =   435
      Left            =   3060
      TabIndex        =   0
      Top             =   2400
      Width           =   1095
   End
End
Attribute VB_Name = "frmDiceRollerCompact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim vals(5) As Integer

Private Enum EAbils
  STRE = 0
  DEX
  CON
  INTE
  WIS
  CHA
End Enum
  
  

Private Sub cboClass_Click()
  RollOptions
  SuggestOrder
End Sub

Private Sub Command1_Click()
  Dim i As Long
  
  txtMinPts.Enabled = False
  'txtMaxPts.Enabled = False
  Command1.Enabled = False
  cboClass.Enabled = False
  txtLevel.Enabled = False
  txtMinModifier.Enabled = False

'  For i = 0 To 4
'    optArray(i).Enabled = False
'  Next i
  
  Me.MousePointer = vbHourglass
  
  RollOptions
  SuggestOrder
  RollGold
  
  txtMinPts.Enabled = True
  'txtMaxPts.Enabled = True
  Command1.Enabled = True
  cboClass.Enabled = True
  txtLevel.Enabled = True
  txtMinModifier.Enabled = True

'  For i = 0 To 4
'    optArray(i).Enabled = True
'  Next i
  
  Me.MousePointer = vbDefault
  
End Sub


Private Sub SuggestOrder()
  ' sort the values

  Select Case cboClass.list(cboClass.ListIndex)
    Case "Artificer"
      ' INT, CHA, DEX, WIS, STR, CON
      lblSuggested(INTE).Caption = vals(5)
      lblSuggested(CHA).Caption = vals(4)
      lblSuggested(DEX).Caption = vals(3)
      lblSuggested(WIS).Caption = vals(2)
      lblSuggested(STRE).Caption = vals(1)
      lblSuggested(CON).Caption = vals(0)
      
    Case "Barbarian"
      ' STR, DEX, CON, WIS, INT, CHA
      lblSuggested(STRE).Caption = vals(5)
      lblSuggested(DEX).Caption = vals(4)
      lblSuggested(CON).Caption = vals(3)
      lblSuggested(WIS).Caption = vals(2)
      lblSuggested(INTE).Caption = vals(1)
      lblSuggested(CHA).Caption = vals(0)
    Case "Bard"
      ' CHA, INT, DEX, CON, STR, WIS
      lblSuggested(CHA).Caption = vals(5)
      lblSuggested(INTE).Caption = vals(4)
      lblSuggested(DEX).Caption = vals(3)
      lblSuggested(CON).Caption = vals(2)
      lblSuggested(STRE).Caption = vals(1)
      lblSuggested(WIS).Caption = vals(0)
    Case "Cleric"
      ' WIS, CHA, STR, INT, CON, DEX
      lblSuggested(WIS).Caption = vals(5)
      lblSuggested(CHA).Caption = vals(4)
      lblSuggested(STRE).Caption = vals(3)
      lblSuggested(INTE).Caption = vals(2)
      lblSuggested(CON).Caption = vals(1)
      lblSuggested(DEX).Caption = vals(0)
    Case "Druid"
      ' WIS, CHA, STR, INT, CON, DEX
      lblSuggested(WIS).Caption = vals(5)
      lblSuggested(CHA).Caption = vals(4)
      lblSuggested(STRE).Caption = vals(3)
      lblSuggested(INTE).Caption = vals(2)
      lblSuggested(CON).Caption = vals(1)
      lblSuggested(DEX).Caption = vals(0)
    Case "Fighter"
      ' STR, DEX, CON, INT, WIS, CHA
      lblSuggested(STRE).Caption = vals(5)
      lblSuggested(DEX).Caption = vals(4)
      lblSuggested(CON).Caption = vals(3)
      lblSuggested(INTE).Caption = vals(2)
      lblSuggested(WIS).Caption = vals(1)
      lblSuggested(CHA).Caption = vals(0)
    Case "Monk"
      ' DEX, WIS, STR, CON, CHA, INT
      lblSuggested(DEX).Caption = vals(5)
      lblSuggested(WIS).Caption = vals(4)
      lblSuggested(STRE).Caption = vals(3)
      lblSuggested(CON).Caption = vals(2)
      lblSuggested(CHA).Caption = vals(1)
      lblSuggested(INTE).Caption = vals(0)
    Case "Paladin"
      ' CHA, WIS, STR, INT, DEX, CON
      lblSuggested(CHA).Caption = vals(5)
      lblSuggested(WIS).Caption = vals(4)
      lblSuggested(STRE).Caption = vals(3)
      lblSuggested(INTE).Caption = vals(2)
      lblSuggested(DEX).Caption = vals(1)
      lblSuggested(CON).Caption = vals(0)
    Case "Psion"
      ' INT, CON, DEX, WIS, CHA, STR
      lblSuggested(INTE).Caption = vals(5)
      lblSuggested(CON).Caption = vals(4)
      lblSuggested(DEX).Caption = vals(3)
      lblSuggested(WIS).Caption = vals(2)
      lblSuggested(CHA).Caption = vals(1)
      lblSuggested(STRE).Caption = vals(0)
    Case "Psychic Warrior"
      ' INT, STR, CON, DEX, WIS, CHA
      lblSuggested(INTE).Caption = vals(5)
      lblSuggested(STRE).Caption = vals(4)
      lblSuggested(CON).Caption = vals(3)
      lblSuggested(DEX).Caption = vals(2)
      lblSuggested(WIS).Caption = vals(1)
      lblSuggested(CHA).Caption = vals(0)
    Case "Ranger"
      ' STR, CON, DEX, WIS, INT, CHA
      lblSuggested(STRE).Caption = vals(5)
      lblSuggested(CON).Caption = vals(4)
      lblSuggested(DEX).Caption = vals(3)
      lblSuggested(WIS).Caption = vals(2)
      lblSuggested(INTE).Caption = vals(1)
      lblSuggested(CHA).Caption = vals(0)
    Case "Rogue"
      ' DEX, CHA, INT, CON, STR, WIS
      lblSuggested(DEX).Caption = vals(5)
      lblSuggested(CHA).Caption = vals(4)
      lblSuggested(INTE).Caption = vals(3)
      lblSuggested(CON).Caption = vals(2)
      lblSuggested(STRE).Caption = vals(1)
      lblSuggested(WIS).Caption = vals(0)
    Case "Sorcerer"
      ' CHA, INT, CON, DEX, STR, WIS
      lblSuggested(CHA).Caption = vals(5)
      lblSuggested(INTE).Caption = vals(4)
      lblSuggested(CON).Caption = vals(3)
      lblSuggested(DEX).Caption = vals(2)
      lblSuggested(STRE).Caption = vals(1)
      lblSuggested(WIS).Caption = vals(0)
    Case "Soulknife"
      ' STR, DEX, CON, INT, WIS, CHA
      lblSuggested(STRE).Caption = vals(5)
      lblSuggested(DEX).Caption = vals(4)
      lblSuggested(CON).Caption = vals(3)
      lblSuggested(INTE).Caption = vals(2)
      lblSuggested(WIS).Caption = vals(1)
      lblSuggested(CHA).Caption = vals(0)
    Case "Wilder"
      ' CHA, INT, CON, DEX, STR, WIS
      lblSuggested(CHA).Caption = vals(5)
      lblSuggested(INTE).Caption = vals(4)
      lblSuggested(CON).Caption = vals(3)
      lblSuggested(DEX).Caption = vals(2)
      lblSuggested(STRE).Caption = vals(1)
      lblSuggested(WIS).Caption = vals(0)
    Case "Wizard"
      ' INT, DEX, CON, STR, WIS, CHA
      lblSuggested(INTE).Caption = vals(5)
      lblSuggested(DEX).Caption = vals(4)
      lblSuggested(CON).Caption = vals(3)
      lblSuggested(STRE).Caption = vals(2)
      lblSuggested(WIS).Caption = vals(1)
      lblSuggested(CHA).Caption = vals(0)
    Case "Commoner"
      ' STR, DEX, CON, INT, WIS, CHA
      lblSuggested(STRE).Caption = vals(5)
      lblSuggested(DEX).Caption = vals(4)
      lblSuggested(CON).Caption = vals(3)
      lblSuggested(INTE).Caption = vals(2)
      lblSuggested(WIS).Caption = vals(1)
      lblSuggested(CHA).Caption = vals(0)
    Case "Expert"
      ' INT, WIS, CHA, STR, DEX, CON
      lblSuggested(INTE).Caption = vals(5)
      lblSuggested(WIS).Caption = vals(4)
      lblSuggested(CHA).Caption = vals(3)
      lblSuggested(STRE).Caption = vals(2)
      lblSuggested(DEX).Caption = vals(1)
      lblSuggested(CON).Caption = vals(0)
    Case "Adept"
      ' WIS, INT, CHA, STR, DEX, CON
      lblSuggested(WIS).Caption = vals(5)
      lblSuggested(INTE).Caption = vals(4)
      lblSuggested(CHA).Caption = vals(3)
      lblSuggested(STRE).Caption = vals(2)
      lblSuggested(DEX).Caption = vals(1)
      lblSuggested(CON).Caption = vals(0)
    Case "Aristocrat"
      ' CHA, INT, DEX, WIS, CON, STR
      lblSuggested(CHA).Caption = vals(5)
      lblSuggested(INTE).Caption = vals(4)
      lblSuggested(DEX).Caption = vals(3)
      lblSuggested(WIS).Caption = vals(2)
      lblSuggested(CON).Caption = vals(1)
      lblSuggested(STRE).Caption = vals(0)
  End Select
End Sub

Private Sub FourD6()
  Dim i As Long
  Dim minRoll As Long
  Dim thisroll As Long
  Dim j As Long
  Dim lngsum As Long
  Dim totalpoints As Long
  Dim highestmod As Long
  Dim totalmod As Long
  
  highestmod = -4
  totalmod = 0
  
  Do Until totalpoints >= CLng(txtMinPts.Text) And highestmod > 1 And totalmod > 0 And highestmod >= CLng(txtMinModifier.Text)
    highestmod = 0
    totalmod = 0
    
    For j = 0 To 5
      minRoll = 7
      lngsum = 0
      For i = 0 To 3
        thisroll = Int(Rnd * 6) + 1
        If thisroll < minRoll Then minRoll = thisroll
        lngsum = lngsum + thisroll
      Next i
      lngsum = lngsum - minRoll
      lblRoll(j).Caption = lngsum
      
    Next j
    
    totalpoints = 0
    For j = 0 To 5
      totalpoints = totalpoints + Int(lblRoll(j).Caption)
    Next j
    lblTotalPts.Caption = "Total points = " & CStr(totalpoints)
    
    highestmod = Int((Int(lblRoll(0).Caption) - 10) / 2)
    
    vals(0) = Int(lblRoll(0).Caption)
    
    For j = 1 To 5
      vals(j) = Int(lblRoll(j).Caption)
      totalmod = totalmod + Int(lblRoll(j).Caption / 2) - 5
      If Int(Int((lblRoll(j).Caption) - 10) / 2) > highestmod Then highestmod = Int((Int(lblRoll(j).Caption) - 10) / 2)
    Next j
    lblHighestMod.Caption = "Highest modifier = " & CStr(highestmod)
    
    DoEvents
  Loop
  
  Quicksort vals, 0, 5
  For j = 0 To 5
    lblRoll(j).Caption = vals(j)
  Next j
  
End Sub

Private Sub Form_Load()
  Randomize Timer
  txtMinModifier.ListIndex = 0
  cboClass.ListIndex = 0
End Sub

Private Function d4(n As Long) As Long
  Dim i As Long
  Dim j As Long
  i = 0
  For j = 1 To n
    i = i + Int(Rnd * 4) + 1
  Next
  d4 = i
End Function

Private Sub RollGold()
  Dim lnggp As Long
  lnggp = 0
  Select Case CLng(txtLevel.Text)
    Case 1
      Select Case cboClass.list(cboClass.ListIndex)
        Case "Artificer"
          lnggp = d4(4) * 10
        Case "Barbarian"
          lnggp = d4(4) * 10
        Case "Bard"
          lnggp = d4(4) * 10
        Case "Cleric"
          lnggp = d4(5) * 10
        Case "Druid"
          lnggp = d4(2) * 10
        Case "Fighter"
          lnggp = d4(6) * 10
        Case "Monk"
          lnggp = d4(5)
        Case "Paladin"
          lnggp = d4(6) * 10
        Case "Psion"
          lnggp = d4(3) * 10
        Case "Psychic Warrior"
          lnggp = d4(5) * 10
        Case "Ranger"
          lnggp = d4(6) * 10
        Case "Rogue"
          lnggp = d4(5) * 10
        Case "Sorcerer"
          lnggp = d4(3) * 10
        Case "Soulknife"
          lnggp = d4(5) * 10
        Case "Wilder"
          lnggp = d4(4) * 10
        Case "Wizard"
          lnggp = d4(3) * 10
        Case "Commoner"
          lnggp = d4(2) * 5
        Case "Expert"
          lnggp = d4(2) * 10
        Case "Adept"
          lnggp = d4(2) * 10
        Case "Aristocrat"
          lnggp = d4(2) * 100
      End Select
    Case 2
      lnggp = 900
    Case 3
      lnggp = 2700
    Case 4
      lnggp = 5400
    Case 5
      lnggp = 9000
    Case 6
      lnggp = 13000
    Case 7
      lnggp = 19000
    Case 8
      lnggp = 27000
    Case 9
      lnggp = 36000
    Case 10
      lnggp = 49000
    Case 11
      lnggp = 66000
    Case 12
      lnggp = 88000
    Case 13
      lnggp = 110000
    Case 14
      lnggp = 150000
    Case 15
      lnggp = 200000
    Case 16
      lnggp = 260000
    Case 17
      lnggp = 340000
    Case 18
      lnggp = 440000
    Case 19
      lnggp = 580000
    Case 20
      lnggp = 760000
    Case 21
      lnggp = 975000
    Case 22
      lnggp = 1200000
    Case 23
      lnggp = 1500000
    Case 24
      lnggp = 1800000
    Case 25
      lnggp = 2100000
    Case 26
      lnggp = 2500000
    Case 27
      lnggp = 2900000
    Case 28
      lnggp = 3300000
    Case 29
      lnggp = 3800000
    Case 30
      lnggp = 4300000
    Case 31
      lnggp = 4900000
    Case 32
      lnggp = 5600000
    Case 33
      lnggp = 6300000
    Case 34
      lnggp = 7000000
    Case 35
      lnggp = 7900000
    Case 36
      lnggp = 8800000
    Case 37
      lnggp = 9900000
    Case 38
      lnggp = 11000000
    Case 39
      lnggp = 12300000
    Case 40
      lnggp = 13600000
    Case Is > 40
      lnggp = Int(13600000 * 1.1 ^ (CLng(txtLevel.Text) - 40) / 100000) * 100000
  End Select
  lblGP.Caption = Format(lnggp, "#,###,###,##0") & " gp"
  
End Sub

Private Sub CheckEnable()
  Dim strtmp As String
  Command1.Enabled = False
  If Not IsNumeric(txtMinPts.Text) Then MsgBox "Please enter a numeric value into the min. pts. box"
  'If Not IsNumeric(txtMaxPts.Text) Then MsgBox "Please enter a numeric value into the max. pts. box"
  
  If IsNumeric(txtMinPts.Text) Then
    If CLng(txtMinPts.Text) > 108 Then txtMinPts.Text = "108"
    
    If Not IsNumeric(txtLevel.Text) Then
      MsgBox "Please enter a numeric value into the level box"
    Else
      If CLng(txtLevel.Text) > 93 Then txtLevel.Text = "93"
      
      If cboClass.ListIndex >= 0 Then
        Command1.Enabled = True
      Else
        MsgBox "Please select a class"
      End If
    End If
  End If
End Sub


'Private Sub txtLevel_LostFocus()
'  CheckEnable
'End Sub
'
'Private Sub txtMaxPts_LostFocus()
'  CheckEnable
'End Sub
'
'Private Sub txtMinPts_LostFocus()
'  CheckEnable
'End Sub

Private Sub optArray_Click(Index As Integer)
  RollOptions
  SuggestOrder
End Sub



Private Sub RollOptions()
  If optArray(0).Value = True Then
    FourD6
  ElseIf optArray(1).Value = True Then  ' base array
    vals(0) = 10
    vals(1) = 10
    vals(2) = 10
    vals(3) = 11
    vals(4) = 11
    vals(5) = 11
    lblRoll(0).Caption = "11"
    lblRoll(1).Caption = "11"
    lblRoll(2).Caption = "11"
    lblRoll(3).Caption = "10"
    lblRoll(4).Caption = "10"
    lblRoll(5).Caption = "10"
    lblTotalPts.Caption = "Total points = 63"
    lblHighestMod.Caption = "Highest modifier = 0"
  ElseIf optArray(2).Value = True Then  ' nonelite array
    vals(0) = 8
    vals(1) = 9
    vals(2) = 10
    vals(3) = 11
    vals(4) = 12
    vals(5) = 13
    lblRoll(0).Caption = "13"
    lblRoll(1).Caption = "12"
    lblRoll(2).Caption = "11"
    lblRoll(3).Caption = "10"
    lblRoll(4).Caption = "9"
    lblRoll(5).Caption = "8"
    lblTotalPts.Caption = "Total points = 63"
    lblHighestMod.Caption = "Highest modifier = 1"
  ElseIf optArray(3).Value = True Then  ' elite array
    lblRoll(0).Caption = "15"
    lblRoll(1).Caption = "14"
    lblRoll(2).Caption = "13"
    lblRoll(3).Caption = "12"
    lblRoll(4).Caption = "10"
    lblRoll(5).Caption = "8"
    vals(0) = 8
    vals(1) = 10
    vals(2) = 12
    vals(3) = 13
    vals(4) = 14
    vals(5) = 15
    lblTotalPts.Caption = "Total points = 72"
    lblHighestMod.Caption = "Highest modifier = 2"
  ElseIf optArray(4).Value = True Then  ' deific array
    lblRoll(0).Caption = "35"
    lblRoll(1).Caption = "29"
    lblRoll(2).Caption = "25"
    lblRoll(3).Caption = "24"
    lblRoll(4).Caption = "24"
    lblRoll(5).Caption = "24"
    vals(0) = 24
    vals(1) = 24
    vals(2) = 24
    vals(3) = 25
    vals(4) = 29
    vals(5) = 35
    lblTotalPts.Caption = "Total points = 161"
    lblHighestMod.Caption = "Highest modifier = 12"
  End If
End Sub


Public Sub Quicksort(list() As Integer, ByVal min As Long, _
    ByVal max As Long)
    
Dim med_value As Long
Dim hi As Long
Dim lo As Long
Dim i As Long

    ' If min >= max, the list contains 0 or 1 items so it
    ' is sorted.
    If min >= max Then Exit Sub

    ' Pick the dividing value.
    i = Int((max - min + 1) * Rnd + min)
    med_value = list(i)

    ' Swap it to the front.
    list(i) = list(min)

    lo = min
    hi = max
    Do
        ' Look down from hi for a value < med_value.
        Do While list(hi) >= med_value
            hi = hi - 1
            If hi <= lo Then Exit Do
        Loop
        If hi <= lo Then
            list(lo) = med_value
            Exit Do
        End If

        ' Swap the lo and hi values.
        list(lo) = list(hi)
        
        ' Look up from lo for a value >= med_value.
        lo = lo + 1
        Do While list(lo) < med_value
            lo = lo + 1
            If lo >= hi Then Exit Do
        Loop
        If lo >= hi Then
            lo = hi
            list(hi) = med_value
            Exit Do
        End If
        
        ' Swap the lo and hi values.
        list(hi) = list(lo)
    Loop
    
    ' Sort the two sublists.
    Quicksort list(), min, lo - 1
    Quicksort list(), lo + 1, max
End Sub

