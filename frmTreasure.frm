VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmTreasure 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Random Treasure"
   ClientHeight    =   8985
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9075
   Icon            =   "frmTreasure.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8985
   ScaleWidth      =   9075
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdRandNPCParty 
      Caption         =   "Random NPC Party"
      Height          =   315
      Left            =   4380
      TabIndex        =   6
      Top             =   60
      Width           =   1575
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   255
      Left            =   60
      TabIndex        =   5
      Top             =   8640
      Width           =   8895
      _ExtentX        =   15690
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
      Scrolling       =   1
   End
   Begin VB.CommandButton cmdRandNPC 
      Caption         =   "Random NPC"
      Height          =   315
      Left            =   2640
      TabIndex        =   4
      Top             =   60
      Width           =   1575
   End
   Begin VB.ListBox lstTreasure 
      Height          =   8055
      Left            =   60
      TabIndex        =   3
      Top             =   420
      Width           =   8895
   End
   Begin VB.CommandButton cmdRoll 
      Caption         =   "Monster Hoard"
      Height          =   315
      Left            =   1080
      TabIndex        =   2
      Top             =   60
      Width           =   1395
   End
   Begin VB.TextBox txtCR 
      Height          =   285
      Left            =   300
      TabIndex        =   1
      Text            =   "1"
      Top             =   75
      Width           =   735
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "CR:"
      Height          =   195
      Left            =   60
      TabIndex        =   0
      Top             =   120
      Width           =   270
   End
End
Attribute VB_Name = "frmTreasure"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Function RandomNPCInventory(lngLevel As Long) As CTreasureHoards
  Dim Inventory As New CTreasureHoards
  Dim i As Long
  Dim nEnc As Long
  Dim ctr As Byte
  
  nEnc = (lngLevel - 1) * 14
  If nEnc > 0 Then
    ctr = 0
    ProgressBar1.Value = ProgressBar1.min
    ProgressBar1.max = nEnc
    
    For i = 1 To nEnc
      ctr = ctr + 1
      If ctr = 4 Then
        Inventory.Add
        Inventory(Inventory.Count).sngCR = CSng(ceil(i / 14))
        Inventory(Inventory.Count).RollTreasure
        ctr = 0
      End If
      ProgressBar1.Value = i
    Next i
    Inventory.Tabulate
    Debug.Print CStr(Inventory.lngTotalCoinValue + Inventory.lngTotalArtValue + Inventory.lngTotalGemValue + Inventory.lngTotalItemValue) & " gp"
  End If
  Set RandomNPCInventory = Inventory
End Function

Private Function RandomNPCPartyInventory(lngLevel As Long) As CTreasureHoards
  ' same as RandomNPC inventory but not split up
  
  Dim Inventory As New CTreasureHoards
  Dim i As Long
  Dim nEnc As Long
  
  nEnc = (lngLevel - 1) * 14
  If nEnc > 0 Then
    ProgressBar1.Value = ProgressBar1.min
    ProgressBar1.max = nEnc
    
    For i = 1 To nEnc
      Inventory.Add
      Inventory(Inventory.Count).sngCR = CSng(ceil(i / 14))
      Inventory(Inventory.Count).RollTreasure
      ProgressBar1.Value = i
    Next i
    Inventory.Tabulate
    Debug.Print CStr(Inventory.lngTotalCoinValue + Inventory.lngTotalArtValue + Inventory.lngTotalGemValue + Inventory.lngTotalItemValue) & " gp"
  End If
  Set RandomNPCPartyInventory = Inventory
End Function

Private Sub cmdRandNPCParty_Click()
  Dim lngHoardID As Long
  Dim i As Long
  Dim AllHoards As New CTreasureHoards
  
  Set AllHoards = RandomNPCPartyInventory(CLng(txtCR.Text))
  
  lstTreasure.Clear
  
  If AllHoards.Count > 0 Then
    If AllHoards.lngTotalCP > 0 Then lstTreasure.AddItem AllHoards.lngTotalCP & " cp"
    If AllHoards.lngTotalSP > 0 Then lstTreasure.AddItem AllHoards.lngTotalSP & " sp"
    If AllHoards.lngTotalGP > 0 Then lstTreasure.AddItem AllHoards.lngTotalGP & " gp"
    If AllHoards.lngTotalPP > 0 Then lstTreasure.AddItem AllHoards.lngTotalPP & " pp"
    
    ' loop through gems and art
    For lngHoardID = 1 To AllHoards.Count
      If AllHoards(lngHoardID).lngGems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objGems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objGems(i).strGemType & " worth " & AllHoards(lngHoardID).objGems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngArt > 0 Then
        For i = 1 To AllHoards(lngHoardID).objGems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objGems(i).strGemType & " worth " & AllHoards(lngHoardID).objGems(i).lngValue & " gp"
        Next i
      End If
    Next lngHoardID
      
    ' loop through magic items
    ' loop through magic items
    For lngHoardID = 1 To AllHoards.Count
      If AllHoards(lngHoardID).lngMundaneItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngMinorItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngMediumItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngMajorItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngEpicItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
    Next lngHoardID
    
    'If AllHoards.lngTotalMundaneItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalMundaneItems & " mundane items"
    'If AllHoards.lngTotalMinorItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalMinorItems & " minor items"
    'If AllHoards.lngTotalMediumItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalMediumItems & " medium items"
    'If AllHoards.lngTotalMajorItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalMajorItems & " major items"
    'If AllHoards.lngTotalEpicItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalEpicItems & " epic items"
    
    'If AllHoards.lngTotalGems > 0 Then lstTreasure.AddItem AllHoards.lngTotalGems & " gems worth a total of " & CStr(AllHoards.lngTotalGemValue) & " gp"
    'If AllHoards.lngTotalArtObjects > 0 Then lstTreasure.AddItem AllHoards.lngTotalArtObjects & " pieces of art worth a total of " & CStr(AllHoards.lngTotalArtValue) & " gp"
    lstTreasure.AddItem "Total hoard value = " & CStr(AllHoards.lngTotalCoinValue + AllHoards.lngTotalArtValue + AllHoards.lngTotalGemValue + AllHoards.lngTotalItemValue) & " gp"

  End If
End Sub


Private Sub cmdRandNPC_Click()
  Dim lngHoardID As Long
  Dim i As Long
  Dim AllHoards As New CTreasureHoards
  
  Set AllHoards = RandomNPCInventory(CLng(txtCR.Text))
  
  lstTreasure.Clear
  
  If AllHoards.Count > 0 Then
    If AllHoards.lngTotalCP > 0 Then lstTreasure.AddItem AllHoards.lngTotalCP & " cp"
    If AllHoards.lngTotalSP > 0 Then lstTreasure.AddItem AllHoards.lngTotalSP & " sp"
    If AllHoards.lngTotalGP > 0 Then lstTreasure.AddItem AllHoards.lngTotalGP & " gp"
    If AllHoards.lngTotalPP > 0 Then lstTreasure.AddItem AllHoards.lngTotalPP & " pp"
    
    ' loop through gems and art
    For lngHoardID = 1 To AllHoards.Count
      If AllHoards(lngHoardID).lngGems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objGems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objGems(i).strGemType & " worth " & AllHoards(lngHoardID).objGems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngArt > 0 Then
        For i = 1 To AllHoards(lngHoardID).objGems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objGems(i).strGemType & " worth " & AllHoards(lngHoardID).objGems(i).lngValue & " gp"
        Next i
      End If
    Next lngHoardID
      
    ' loop through magic items
    For lngHoardID = 1 To AllHoards.Count
      If AllHoards(lngHoardID).lngMundaneItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngMinorItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngMediumItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngMajorItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
      If AllHoards(lngHoardID).lngEpicItems > 0 Then
        For i = 1 To AllHoards(lngHoardID).objItems.Count
          lstTreasure.AddItem AllHoards(lngHoardID).objItems(i).strGemType & " worth " & AllHoards(lngHoardID).objItems(i).lngValue & " gp"
        Next i
      End If
    Next lngHoardID
    
    'If AllHoards.lngTotalMundaneItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalMundaneItems & " mundane items"
    'If AllHoards.lngTotalMinorItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalMinorItems & " minor items"
    'If AllHoards.lngTotalMediumItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalMediumItems & " medium items"
    'If AllHoards.lngTotalMajorItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalMajorItems & " major items"
    'If AllHoards.lngTotalEpicItems > 0 Then lstTreasure.AddItem AllHoards.lngTotalEpicItems & " epic items"
    
    'If AllHoards.lngTotalGems > 0 Then lstTreasure.AddItem AllHoards.lngTotalGems & " gems worth a total of " & CStr(AllHoards.lngTotalGemValue) & " gp"
    'If AllHoards.lngTotalArtObjects > 0 Then lstTreasure.AddItem AllHoards.lngTotalArtObjects & " pieces of art worth a total of " & CStr(AllHoards.lngTotalArtValue) & " gp"
    lstTreasure.AddItem "Total hoard value = " & CStr(AllHoards.lngTotalCoinValue + AllHoards.lngTotalArtValue + AllHoards.lngTotalGemValue + AllHoards.lngTotalItemValue) & " gp"

  End If
End Sub

Private Sub cmdRoll_Click()
  Dim hoard As New CTreasureHoard
  Dim i As Long
  
  hoard.sngCR = CSng(txtCR.Text)
  hoard.RollTreasure
  With lstTreasure
    .Clear
    If hoard.lngCP > 0 Then .AddItem hoard.lngCP & " cp"
    If hoard.lngSP > 0 Then .AddItem hoard.lngSP & " sp"
    If hoard.lngGP > 0 Then .AddItem hoard.lngGP & " gp"
    If hoard.lngPP > 0 Then .AddItem hoard.lngPP & " pp"
    
    If hoard.lngGems > 0 Then
      For i = 1 To hoard.objGems.Count
        .AddItem hoard.objGems(i).strGemType & " worth " & hoard.objGems(i).lngValue & " gp"
      Next i
    End If
    If hoard.lngArt > 0 Then
      For i = 1 To hoard.objGems.Count
        .AddItem hoard.objGems(i).strGemType & " worth " & hoard.objGems(i).lngValue & " gp"
      Next i
    End If
    
    If hoard.lngMundaneItems > 0 Then
      For i = 1 To hoard.objItems.Count
        .AddItem hoard.objItems(i).strGemType & " worth " & hoard.objItems(i).lngValue & " gp"
      Next i
    End If
    If hoard.lngMinorItems > 0 Then
      For i = 1 To hoard.objItems.Count
        .AddItem hoard.objItems(i).strGemType & " worth " & hoard.objItems(i).lngValue & " gp"
      Next i
    End If
    If hoard.lngMediumItems > 0 Then
      For i = 1 To hoard.objItems.Count
        .AddItem hoard.objItems(i).strGemType & " worth " & hoard.objItems(i).lngValue & " gp"
      Next i
    End If
    If hoard.lngMajorItems > 0 Then
      For i = 1 To hoard.objItems.Count
        .AddItem hoard.objItems(i).strGemType & " worth " & hoard.objItems(i).lngValue & " gp"
      Next i
    End If
    If hoard.lngEpicItems > 0 Then
      For i = 1 To hoard.objItems.Count
        .AddItem hoard.objItems(i).strGemType & " worth " & hoard.objItems(i).lngValue & " gp"
      Next i
    End If
    
    'If hoard.lngGems > 0 Then .AddItem hoard.lngGems & " gems worth a total of " & CStr(hoard.lngTotalGemValue) & " gp"
    'If hoard.lngArt > 0 Then .AddItem hoard.lngArt & " pieces of art worth a total of " & CStr(hoard.lngTotalGemValue) & " gp"
    .AddItem "Total hoard value = " & CStr(hoard.lngTotalHoardValue) & " gp"
    
  End With
  
End Sub

Private Sub Form_Load()
  Randomize Timer
End Sub
