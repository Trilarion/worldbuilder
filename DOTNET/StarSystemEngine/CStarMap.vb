Option Strict Off
Option Explicit On
Friend Class CStarMap
	
    Private StarMap(,,) As Byte
	
	Public Sub MakeStarMap(ByRef filename As String, ByRef MaxAppMag As Single, ByRef NStars As Integer, ByRef rows As Integer, ByRef cols As Integer)
		Dim stars As New CStars
		Dim j, i, k As Integer
		
		ReDim StarMap(rows, cols, 2)
		
		For i = 1 To NStars
			stars.Add()
			stars(i).RollStar()
			stars(i).AppMag = Rnd() * MaxAppMag
			stars(i).RightAscension = Rnd() * 360
			stars(i).DistanceLY = Rnd() * 6000
			stars(i).RightAscension = Rnd() * 360
			stars(i).DecDeg = Rnd() * 180 - 90
			stars(i).DistanceLY = 32.6 * System.Math.Exp(0.460517018599 * (stars(i).AppMag - stars(i).AbsMag))
			
			j = CInt(stars(i).DecDeg * rows / 90 + 90)
			k = CInt(stars(i).RightAscension * cols / 360)
			
			Select Case Left(stars(i).SpectralType, 1)
				Case "O"
					StarMap(j, k, 0) = 173
					StarMap(j, k, 1) = 173
					StarMap(j, k, 2) = 255
				Case "B"
					StarMap(j, k, 0) = 230
					StarMap(j, k, 1) = 230
					StarMap(j, k, 2) = 255
				Case "A"
					StarMap(j, k, 0) = 255
					StarMap(j, k, 1) = 255
					StarMap(j, k, 2) = 255
				Case "F"
					StarMap(j, k, 0) = 220
					StarMap(j, k, 1) = 255
					StarMap(j, k, 2) = 255
				Case "G"
					StarMap(j, k, 0) = 255
					StarMap(j, k, 1) = 255
					StarMap(j, k, 2) = 128
				Case "K"
					StarMap(j, k, 0) = 255
					StarMap(j, k, 1) = 175
					StarMap(j, k, 2) = 0
				Case "M"
					StarMap(j, k, 0) = 255
					StarMap(j, k, 1) = 128
					StarMap(j, k, 2) = 128
				Case Else
					StarMap(j, k, 0) = 255
					StarMap(j, k, 0) = 255
					StarMap(j, k, 0) = 255
			End Select
			
			StarMap(j, k, 0) = StarMap(j, k, 0) / 10 ^ stars(i).AppMag
			StarMap(j, k, 1) = StarMap(j, k, 1) / 10 ^ stars(i).AppMag
			StarMap(j, k, 2) = StarMap(j, k, 2) / 10 ^ stars(i).AppMag
			
		Next i
		
		' next save the bitmap
		
	End Sub
End Class