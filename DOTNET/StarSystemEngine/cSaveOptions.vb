Option Strict Off
Option Explicit On
Friend Class cSaveOptions
	
	Private m_lJpgQuality As Integer
	Private m_lJpgTransformation As Integer
	Private m_lTiffCompression As Integer
	Private m_lTiffBitDepth As Integer
	
	Public Property JpgQuality() As Integer
		Get
			JpgQuality = m_lJpgQuality
		End Get
		Set(ByVal Value As Integer)
			m_lJpgQuality = Value
		End Set
	End Property
	
	Public Property JpgTransformation() As Integer
		Get
			JpgTransformation = m_lJpgTransformation
		End Get
		Set(ByVal Value As Integer)
			m_lJpgTransformation = Value
		End Set
	End Property
	
	Public Property TiffCompression() As Integer
		Get
			TiffCompression = m_lTiffCompression
		End Get
		Set(ByVal Value As Integer)
			m_lTiffCompression = Value
		End Set
	End Property
	
	Public Property TiffBitDepth() As Integer
		Get
			TiffBitDepth = m_lTiffBitDepth
		End Get
		Set(ByVal Value As Integer)
			m_lTiffBitDepth = Value
		End Set
	End Property
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		m_lJpgQuality = 50
		m_lJpgTransformation = 0
		m_lTiffCompression = 6
		m_lTiffBitDepth = 24
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
End Class