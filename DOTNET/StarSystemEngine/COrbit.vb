Option Strict Off
Option Explicit On
Friend Class COrbit
	
	Private mvarlN As Integer ' quantum number
	Private mvarlK As Integer ' quantum number
	Private mvarEpoch As Single 'local copy
	Private mvark As Single 'local copy
	Private mvariDeg As Single 'local copy
	Private mvarMeanMotion As Single 'local copy
	Private mvarAscendingNode As Single 'local copy
	Private mvarTimeOfPericenter As Single 'local copy
	Private mvarPeriodYr As Single 'local copy
	Private mvarPericenterDistance As Single 'local copy
	Private mvarArgumentOfPericenter As Single 'local copy
	Private mvarEccentricity As Single 'local copy
	Private mvarSemiMajorAxisAU As Double 'local copy
	Private mvarMeanAnomaly As Single 'local copy
	Private mvarLongOfPericenter As Single 'local copy
	
	
	
	Public Property LongOfPericenter() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.LongOfPericenter
			LongOfPericenter = mvarLongOfPericenter
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.LongOfPericenter = 5
			mvarLongOfPericenter = Value
		End Set
	End Property
	
	
	
	
	
	
	
	Public Property MeanAnomaly() As Single
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.MeanAnomaly
			MeanAnomaly = mvarMeanAnomaly
		End Get
		Set(ByVal Value As Single)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.MeanAnomaly = 5
			mvarMeanAnomaly = Value
		End Set
	End Property
	
	
	
	
	
	
	Public Property lN() As Integer
		Get
			lN = mvarlN
		End Get
		Set(ByVal Value As Integer)
			mvarlN = Value
			If mvarlN < 1 Then mvarlN = 1
			If mvarlK >= mvarlN Then mvarlK = mvarlN - 1
		End Set
	End Property
	
	
	
	Public Property lK() As Integer
		Get
			lK = mvarlK
		End Get
		Set(ByVal Value As Integer)
			mvarlK = Value
			If mvarlK < 0 Then mvarlK = 0
			If mvarlK >= mvarlN Then mvarlN = mvarlK + 1
			
		End Set
	End Property
	
	
	
	Public Property SemiMajorAxisAU() As Double
		Get
			SemiMajorAxisAU = mvarSemiMajorAxisAU
		End Get
		Set(ByVal Value As Double)
			mvarSemiMajorAxisAU = Value
		End Set
	End Property
	
	
	
	Public Property eccentricity() As Single
		Get
			eccentricity = mvarEccentricity
		End Get
		Set(ByVal Value As Single)
			mvarEccentricity = Value
		End Set
	End Property
	
	
	
	Public Property ArgumentOfPericenter() As Single
		Get
			ArgumentOfPericenter = mvarArgumentOfPericenter
		End Get
		Set(ByVal Value As Single)
			mvarArgumentOfPericenter = Value
		End Set
	End Property
	
	
	
	Public Property PericenterDistance() As Single
		Get
			PericenterDistance = mvarPericenterDistance
		End Get
		Set(ByVal Value As Single)
			mvarPericenterDistance = Value
		End Set
	End Property
	
	
	
	Public Property PeriodYr() As Single
		Get
			PeriodYr = mvarPeriodYr
		End Get
		Set(ByVal Value As Single)
			mvarPeriodYr = Value
		End Set
	End Property
	
	
	
	Public Property TimeOfPericenter() As Single
		Get
			TimeOfPericenter = mvarTimeOfPericenter
		End Get
		Set(ByVal Value As Single)
			mvarTimeOfPericenter = Value
		End Set
	End Property
	
	
	
	
	Public Property AscendingNode() As Single
		Get
			AscendingNode = mvarAscendingNode
		End Get
		Set(ByVal Value As Single)
			mvarAscendingNode = Value
		End Set
	End Property
	
	
	
	
	Public Property MeanMotion() As Single
		Get
			MeanMotion = mvarMeanMotion
		End Get
		Set(ByVal Value As Single)
			mvarMeanMotion = Value
		End Set
	End Property
	
	
	
	
	Public Property iDeg() As Single
		Get
			iDeg = mvariDeg
		End Get
		Set(ByVal Value As Single)
			mvariDeg = Value
		End Set
	End Property
	
	
	
	
	Public Property k() As Single
		Get
			k = mvark
		End Get
		Set(ByVal Value As Single)
			mvark = Value
		End Set
	End Property
	
	
	
	
	Public Property epoch() As Single
		Get
			epoch = mvarEpoch
		End Get
		Set(ByVal Value As Single)
			mvarEpoch = Value
		End Set
	End Property
	
	
	Public Function EllipsePerimeterNK(ByRef n As Integer, ByRef k As Integer) As Double
		' Calculates the perimeter of an ellipse where e = k/n
		' k and n are planetary quantum numbers
		
		Dim sqrn2k2 As Double
		
		If n > 0 And k >= 0 And k < n Then
			sqrn2k2 = System.Math.Sqrt(n ^ 2 - k ^ 2)
			EllipsePerimeterNK = (3 * PI * (sqrn2k2 - n) ^ 2) / ((System.Math.Sqrt(14 * sqrn2k2 * n - k ^ 2 + 2 * n ^ 2) + 10 * (sqrn2k2 + n)) * n) + PI * (sqrn2k2 + n) / n
		End If
		
	End Function
	
	Public Function EllipsePerimeterE(ByRef e As Single) As Double
		' Calculates the perimeter of an ellipse where e = k/n
		' k and n are planetary quantum numbers
		
		Dim sqrn2k2 As Double
		
		If e >= 0 And e < 1 Then
			sqrn2k2 = System.Math.Sqrt(1 - e ^ 2)
			EllipsePerimeterE = (3 * PI * (sqrn2k2 - 1) ^ 2) / ((System.Math.Sqrt(14 * sqrn2k2 - e ^ 2 + 2) + 10 * (sqrn2k2 + 1))) + PI * (sqrn2k2 + 1)
		End If
		
	End Function
	
	
	Public Sub OrbitDataNK(ByRef PrimaryMassMSol As Single)
		
		If mvarlN = 0 Then
			mvarlN = 1
			mvarlK = 0
		End If
		
		mvarEccentricity = mvarlK / mvarlN
		mvarPeriodYr = PrimaryMassMSol * (mvarlN * EllipsePerimeterNK(mvarlN, mvarlK) / w0) ^ 3
		mvarSemiMajorAxisAU = (PrimaryMassMSol * mvarPeriodYr ^ 2) ^ (1 / 3)
		
	End Sub
	
	Public Sub OrbitDataE(ByRef PrimaryMassMSol As Single)
		
		If mvarlN = 0 Then
			mvarlN = 1
			mvarlK = 0
		End If
		
		mvarPeriodYr = PrimaryMassMSol * (mvarlN * EllipsePerimeterE(mvarEccentricity) / w0) ^ 3
		mvarSemiMajorAxisAU = (PrimaryMassMSol * mvarPeriodYr ^ 2) ^ (1 / 3)
		
	End Sub
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		mvarlN = 1
		mvarMeanAnomaly = Rnd() * 360
		mvarEpoch = 2450800.5
		mvarLongOfPericenter = Rnd() * 360
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
End Class