Option Strict Off
Option Explicit On
Module modStarSystemGlobals
	
	
	
	' solar system constants
	Public Const MSol As Double = 1.98892E+30 ' kg
	Public Const RSol As Integer = 695500000 ' m
	Public Const TSol As Integer = 5780 ' Kelvin
	Public Const MvSol As Single = 4.83 ' Absolute Magnitude
	Public Const MAppSol As Single = -26.77 ' Apparent Magnitude of Sol from Earth
	Public Const MEarth As Double = 5.9742E+24 ' kg
	Public Const REarth As Double = 6378.1 ' km
	Public Const MJupiter As Double = 1.8987E+27 ' kg
	
	' Atomic constants
	Public Const MElectron As Double = 9.10938188E-31 'kg
	Public Const MHydrogen As Double = 1.67353403897E-27 'kg
	
	' Physical and Mathematical constants
	Public Const w0 As Double = 30.376077073 ' AU/yr: fundamental and universal speed of planetary accretion disk
	Public Const PI As Double = 3.14159265359
	Public Const G As Double = 6.67259E-11 ' m�/(s��kg)
	Public Const HPlanck As Double = 6.6260755E-34 'J�s
	Public Const HBar As Double = 1.0545727E-34 'J�s
	Public Const UniversalGasConstantR As Double = 8.31 'K/mol�K
	Public Const AvogadroConstantNA As Double = 6.02E+23 'atoms/mol
	Public Const StefanBoltzmannConstant As Double = 0.0000000567 'W/(m��K4)
	Public Const SpeedOfLight As Double = 299792458 ' m/s; exact
	Public Const kB As Double = 1.380658E-23 ' J/K
	
	' Conversion ratios
	Public Const DiamMi2RadKm As Single = 0.8045 ' converts planetary diameter in miles to radius in km
	Public Const AU2km As Double = 149597900 ' Converts astronomical units to km
	Public Const mi2km As Single = 1.609344 ' miles per km
	
	' Density Constants
	Public Const HDensityKgM3 As Double = 0.09
	Public Const AerogelDensityKgM3 As Double = 3
	Public Const IceDensityKgM3 As Double = 900
	Public Const H2ODensityKgM3 As Double = 997
	Public Const NDensityKgM3 As Double = 1250
	Public Const ODensityKgM3 As Double = 1429
	Public Const MgDensityKgM3 As Double = 1738
	Public Const SiDensityKgM3 As Double = 2330
	Public Const CarbonaceousMatrixRichCometDensityKgM3 As Short = 2600
	Public Const CarbonaceousMetalRichCometDensityKgM3 As Short = 3300
	Public Const ChondriteCometDensityKgM3 As Short = 3750
	Public Const CDensityKgM3 As Double = 3510
	Public Const FayaliteDensityKgM3 As Short = 4312 ' Magnesium Silicate
	Public Const FeDensityKgM3 As Double = 7874
	Public Const NiDensityKgM3 As Double = 8908
	Public Const PbDensityKgM3 As Double = 11340
	Public Const UDensityKgM3 As Double = 19050
	Public Const WhiteDwarfStarCoreDensityKgM3 As Double = 10000000000#
	Public Const UraniumNucleusDensityKgM3 As Double = 3E+17
	Public Const NeutronStarCoreDensityKgM3 As Double = 1E+17 'to 1e18 kg/m�
	Public Const NeutronStarMeanDensityKgM3 As Double = 1E+15
	Public Const BlackHoleDensityKgM3 As Double = 1E+19
	
	Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Integer)
End Module