Option Strict Off
Option Explicit On
Friend Class CAtmosphere
	
	'local variable(s) to hold property value(s)
	Private mvarHeight As Integer 'local copy
    Private mvarLower As CRGBColor 'local copy
	Private mvarUpper As CRGBColor 'local copy
	Private mvarSky As CRGBColor 'local copy
	Private mvarCloudMap As String 'local copy
	Private mvarCloudHeight As Integer 'local copy
	Private mvarCloudSpeed As Integer 'local copy
	Private mvarSunset As CRGBColor 'local copy
	
	
	
	Public Property Sunset() As CRGBColor
		Get
			Sunset = mvarSunset
		End Get
		Set(ByVal Value As CRGBColor)
			mvarSunset = Value
		End Set
	End Property
	
	
	
	
	
	Public Property CloudSpeed() As Integer
		Get
			CloudSpeed = mvarCloudSpeed
		End Get
		Set(ByVal Value As Integer)
			mvarCloudSpeed = Value
		End Set
	End Property
	
	
	
	
	
	Public Property CloudHeight() As Integer
		Get
			CloudHeight = mvarCloudHeight
		End Get
		Set(ByVal Value As Integer)
			mvarCloudHeight = Value
		End Set
	End Property
	
	
	
	
	
	Public Property CloudMap() As String
		Get
			CloudMap = mvarCloudMap
		End Get
		Set(ByVal Value As String)
			mvarCloudMap = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Sky() As CRGBColor
		Get
			Sky = mvarSky
		End Get
		Set(ByVal Value As CRGBColor)
			mvarSky = Value
		End Set
	End Property
	
	
	
	
	
	
	Public Property Upper() As CRGBColor
		Get
			Upper = mvarUpper
		End Get
		Set(ByVal Value As CRGBColor)
			mvarUpper = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Lower() As CRGBColor
		Get
			Lower = mvarLower
		End Get
		Set(ByVal Value As CRGBColor)
			mvarLower = Value
		End Set
	End Property
	
	
	
	
	
	Public Property Height() As Integer
		Get
			Height = mvarHeight
		End Get
		Set(ByVal Value As Integer)
			mvarHeight = Value
		End Set
	End Property
	
	
	Public Sub Generate(ByRef StarType As String, ByRef SpectralType As String, ByRef RadiusM As Double)
		
		If SpectralType Like "EoGaian" Or SpectralType Like "Europan" Or SpectralType Like "LithicGelidian" Or SpectralType Like "Panthalassic" Or SpectralType Like "Gaian" Or SpectralType Like "MesoGaian" Or SpectralType Like "Cytherian" Or SpectralType Like "Moist Greenhouse" Or StarType Like "Jovian" Or StarType Like "Brown Dwarf" Then
			
			If SpectralType Like "EoGaian" Or SpectralType Like "Europan" Or SpectralType Like "LithicGelidian" Or SpectralType Like "Panthalassic" Or SpectralType Like "Gaian" Then
				
				mvarHeight = (0.004957 + randn * 0.000268552) * RadiusM
				
				
				mvarLower.Red = 0.43
				mvarLower.Green = 0.52
				mvarLower.Blue = 0.65
				
				mvarUpper.Red = 0.26
				mvarUpper.Green = 0.47
				mvarUpper.Blue = 0.84
				
				mvarSky.Red = 0.4
				mvarSky.Green = 0.6
				mvarSky.Blue = 1#
				
				mvarSunset.Red = 1#
				mvarSunset.Green = 0.6
				mvarSunset.Blue = 0.2
				
				mvarCloudHeight = 7
				mvarCloudSpeed = 65
				mvarCloudMap = "earth-clouds.*"
				
			ElseIf SpectralType Like "MesoGaian" Then 
				mvarHeight = (0.004957 + randn * 0.000268552) * RadiusM
				
				mvarLower.Red = 0.43
				mvarLower.Green = 0.52
				mvarLower.Blue = 0.65
				
				mvarUpper.Red = 0.26
				mvarUpper.Green = 0.47
				mvarUpper.Blue = 0.84
				
				mvarSky.Red = 0.4
				mvarSky.Green = 0.6
				mvarSky.Blue = 1#
				
				mvarSunset.Red = 1#
				mvarSunset.Green = 0.6
				mvarSunset.Blue = 0.2
				
				mvarCloudHeight = 7
				mvarCloudSpeed = 65
				mvarCloudMap = "venus.jpg"
				
			ElseIf SpectralType Like "Cytherian" Or SpectralType Like "Moist Greenhouse" Then 
				mvarHeight = (0.004957 + randn * 0.000268552) * RadiusM
				mvarLower.Red = 0.8
				mvarLower.Green = 0.8
				mvarLower.Blue = 0.5
				
				mvarUpper.Red = 0.6
				mvarUpper.Green = 0.6
				mvarUpper.Blue = 0.6
				
				mvarSky.Red = 0.8
				mvarSky.Green = 0.8
				mvarSky.Blue = 0.5
				
				mvarCloudMap = "venus.jpg"
				mvarCloudHeight = 50
				mvarCloudSpeed = 90
				
				
			ElseIf SpectralType Like "Arean" Then 
				mvarHeight = (0.004957 + randn * 0.000268552) * RadiusM
				
				mvarLower.Red = 0.8
				mvarLower.Green = 0.6
				mvarLower.Blue = 0.6
				
				mvarUpper.Red = 0.7
				mvarUpper.Green = 0.3
				mvarUpper.Blue = 0.3
				
				mvarSky.Red = 0.83
				mvarSky.Green = 0.75
				mvarSky.Blue = 0.65
				
				mvarSunset.Red = 0.7
				mvarSunset.Green = 0.7
				mvarSunset.Blue = 0.8
			End If
		End If
		
		If StarType Like "Jovian" Or StarType Like "Brown Dwarf" Then
			
			mvarHeight = (0.002442188 + randn * 0.000233374) * RadiusM
			
			mvarLower.Red = 0.8
			mvarLower.Green = 0.75
			mvarLower.Blue = 0.65
			
			mvarUpper.Red = 0.6
			mvarUpper.Green = 0.55
			mvarUpper.Blue = 0.45
			
			mvarSky.Red = 0.8
			mvarSky.Green = 0.8
			mvarSky.Blue = 0.5
			
		End If
		
		If StarType Like "Star" Then
			mvarHeight = RadiusM / 2
			
			If SpectralType Like "O*" Then
				mvarLower.Red = 0.8
				mvarLower.Green = 0.8
				mvarLower.Blue = 0.96
				
			ElseIf SpectralType Like "B*" Then 
				mvarLower.Red = 0.91
				mvarLower.Green = 0.99
				mvarLower.Blue = 1
				
			ElseIf SpectralType Like "A*" Then 
				mvarLower.Red = 1
				mvarLower.Green = 1
				mvarLower.Blue = 1
				
			ElseIf SpectralType Like "F*" Then 
				mvarLower.Red = 0.99
				mvarLower.Green = 0.99
				mvarLower.Blue = 0.8
				
			ElseIf SpectralType Like "G*" Then 
				mvarLower.Red = 1
				mvarLower.Green = 0.96
				mvarLower.Blue = 0.59
				
			ElseIf SpectralType Like "K*" Then 
				mvarLower.Red = 0.98
				mvarLower.Green = 0.84
				mvarLower.Blue = 0.59
				
			ElseIf SpectralType Like "M*" Then 
				mvarLower.Red = 0.89
				mvarLower.Green = 0.44
				mvarLower.Blue = 0.45
				
			End If
			
			If StarType Like "White Dwarf" Then
				mvarLower.Red = 1
				mvarLower.Green = 1
				mvarLower.Blue = 1
			End If
			
			mvarUpper.Red = 0
			mvarUpper.Green = 0
			mvarUpper.Blue = 0
			
			mvarSky.Red = mvarLower.Red
			mvarSky.Green = mvarLower.Green
			mvarSky.Blue = mvarLower.Blue
		End If
	End Sub
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		mvarLower = New CRGBColor
		mvarUpper = New CRGBColor
		mvarSky = New CRGBColor
		mvarSunset = New CRGBColor
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		'UPGRADE_NOTE: Object mvarLower may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarLower = Nothing
		'UPGRADE_NOTE: Object mvarUpper may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarUpper = Nothing
		'UPGRADE_NOTE: Object mvarSky may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarSky = Nothing
		'UPGRADE_NOTE: Object mvarSunset may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		mvarSunset = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class