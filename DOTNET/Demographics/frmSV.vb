Option Strict Off
Option Explicit On
Friend Class frmSV
	Inherits System.Windows.Forms.Form
	
	Public Sub CalculateSV()
		lstBusinesses.Items.Clear()
		AddListItem("Shoemakers          ", 150)
		AddListItem("Furriers            ", 250)
		AddListItem("Maidservants        ", 250)
		AddListItem("Tailors             ", 250)
		AddListItem("Barbers             ", 350)
		AddListItem("Unlicensed doctors  ", 350)
		AddListItem("Jewelers            ", 400)
		AddListItem("Taverns/Restaurants ", 400)
		AddListItem("Old-Clothes         ", 400)
		AddListItem("Pastrycooks         ", 500)
		AddListItem("Masons              ", 500)
		AddListItem("Carpenters          ", 550)
		AddListItem("Mercers             ", 700)
		AddListItem("Coopers             ", 700)
		AddListItem("Bakers              ", 800)
		AddListItem("Watercarriers       ", 850)
		AddListItem("Scabbardmakers      ", 850)
		AddListItem("Wine-Sellers        ", 900)
		AddListItem("Hatmakers           ", 950)
		AddListItem("Saddlers            ", 1000)
		AddListItem("Chicken Butchers    ", 1000)
		AddListItem("Pursemakers         ", 1100)
		AddListItem("Woodsellers         ", 2400)
		AddListItem("Arcane Supply Seller", 2800)
		AddListItem("Bookbinders         ", 3000)
		AddListItem("Butchers            ", 1200)
		AddListItem("Fishmongers         ", 1200)
		AddListItem("Beer-Sellers        ", 1400)
		AddListItem("Buckle Makers       ", 1400)
		AddListItem("Plasterers          ", 1400)
		AddListItem("Spice Merchants     ", 1400)
		AddListItem("Blacksmiths         ", 1500)
		AddListItem("Painters            ", 1500)
		AddListItem("Licensed Doctors    ", 1700)
		AddListItem("Roofers             ", 1800)
		AddListItem("Locksmiths          ", 1900)
		AddListItem("Bathers             ", 1900)
		AddListItem("Ropemakers          ", 1900)
		AddListItem("Inns                ", 2000)
		AddListItem("Tanners             ", 2000)
		AddListItem("Copyists            ", 2000)
		AddListItem("Sculptors           ", 2000)
		AddListItem("Rugmakers           ", 2000)
		AddListItem("Harness-Makers      ", 2000)
		AddListItem("Bleachers           ", 2100)
		AddListItem("Hay Merchants       ", 2300)
		AddListItem("Glovemakers         ", 2400)
		AddListItem("Woodcarvers         ", 2400)
		AddListItem("Booksellers         ", 6300)
		AddListItem("Illuminators        ", 3900)
		AddListItem("Nobles              ", 200)
		AddListItem("Lawyer (Advocate)   ", 650)
		AddListItem("Clergymen           ", 40)
		AddListItem("Priest              ", 25 * 40)
		AddListItem("Lawmen              ", 150)
		lstBusinesses.Items.Add("Avian Livestock     " & Chr(9) & VB6.Format(CInt(lblPopulation.Text) * 2.2 * 0.68, "#,##0"))
		lstBusinesses.Items.Add("Mammalian Livestock " & Chr(9) & VB6.Format(CInt(lblPopulation.Text) * 2.2 * 0.32, "#,##0"))
		lblArea.Text = VB6.Format(CInt(lblPopulation.Text) / 38850, "#,##0.0")
		
		
		' Power Center
		Select Case Int(Rnd() * 20) + 1
			Case 1
				lblPowerCenter.Text = "Monstrous"
			Case 2 To 13
				lblPowerCenter.Text = "Conventional"
			Case 14 To 18
				lblPowerCenter.Text = "Nonstandard"
			Case 19 To 20
				lblPowerCenter.Text = "Magical"
		End Select
		
		Select Case Int(Rnd() * 100) + 1
			Case 1 To 35
				lblAlignment.Text = "Lawful Good"
			Case 36 To 39
				lblAlignment.Text = "Neutral Good"
			Case 40 To 41
				lblAlignment.Text = "Chaotic Good"
			Case 42 To 61
				lblAlignment.Text = "Lawful Neutral"
			Case 62 To 63
				lblAlignment.Text = "Neutral"
			Case 64
				lblAlignment.Text = "Chaotic Neutral"
			Case 65 To 90
				lblAlignment.Text = "Lawful Evil"
			Case 91 To 98
				lblAlignment.Text = "Neutral Evil"
			Case 99 To 100
				lblAlignment.Text = "Chaotic Evil"
				
		End Select
		
	End Sub
	
	
	Private Sub AddListItem(ByRef strCaption As String, ByRef sv As Integer)
		Dim pop As Integer
		Dim sngval As Single
		pop = CInt(lblPopulation.Text)
		
		With lstBusinesses
			If pop >= sv Then
				.Items.Add(strCaption & Chr(9) & VB6.Format(pop / sv, "0"))
			Else
				sngval = Rnd()
				If sngval < CSng(pop) / CSng(sv) Then
					.Items.Add(strCaption & Chr(9) & "1 poor quality")
				Else
					'.AddItem strCaption & Chr(9) & "0"
				End If
				'.AddItem strCaption & Chr(9) & Format(CSng(pop) / CSng(sv), "0%") & " chance"
			End If
		End With
		
	End Sub
	
	
	Private Sub frmSV_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        'If gcblnOGL = True Then
        Label18.Visible = False
        Label21.Visible = False
        lblPowerCenter.Visible = False
        lblAlignment.Visible = False
        lstBusinesses.Top = VB6.TwipsToPixelsY(420)
        lstBusinesses.Height = VB6.TwipsToPixelsY(5940)
        'End If
	End Sub
End Class