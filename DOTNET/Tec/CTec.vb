Option Strict Off
Option Explicit On
Friend Class CTec
	
	Const MAXSPLINTER As Short = 100
	Const MAXFRAG As Short = 1000
	Const MAXCHANGE As Short = 10
	Const REALSCALE As Short = 100
	
	Const RIFTMARK As Short = -1
	
    Dim finalDEM(,) As Short
	
	Dim MR As Object
	
	Dim MAXLIFE As Integer
	Dim change(1) As Integer ' needed for fileio
	
	' The following arrays are global and most are used by functions in both _
	'source files.  The two main ones are m and t.  Each is set up to be two _
	'2-d arrays, where each array is the size of the whole world.  M is the _
	'map; elements in m are indices of plates, showing which squares are _
	'covered by which plate.  T is the topography; elements in t are altitudes.
	
    Dim m(,,) As Short
    Dim t(,,) As Short
	
	
	' Several arrays are used by the binary blob segmenter, segment() in tec2.c. _
	'These include r, which is used to store fragment indices; many fragments _
	'make up one region during a segmentation.  Kid is a lookup table; fragment _
	'k belongs to region kid(k) after a segmentation is finished.  Karea(k) _
	'is the area of fragment k.
	
    Dim r(,) As Integer
	Dim kid(MAXFRAG) As Integer
	Dim karea(MAXFRAG) As Integer
	
	' The merge routine gets information from the move routine; when the move _
	'routine puts a square of one plate on top of another plate, that information _
	'is recorded in the merge matrix mm.
	
	Dim mm(MAXPLATE, MAXPLATE) As Integer
	
	' The erosion routine needs an array to store delta information; during an _
	'erosion, the increases or decreases in elevation are summed in e and then _
	'applied all at once to the topography.
	
    Dim e(,) As Integer
	
	' Several routines need temporary storage for areas and plate identifiers.
	
	Dim tarea(MAXPLATE) As Integer
	Dim ids(MAXPLATE) As Integer
	
	
	'UPGRADE_NOTE: step was upgraded to step_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Dim phead, pavail, step_Renamed As Integer
	
	
	' The plates in use are stored in this data structure.  Dx,dy are the _
	'values to move by THIS STEP ONLY; odx,ody are the permanent move _
	'values; rx,ry are the remainder x and y values used by newdxy() to _
	'determine dx,dy; age is the age of the plate, in steps; area is the _
	'area of the plate, in squares; id is the value in the m array which _
	'corresponds to this plate; next is a polonger to the next occupied _
	'element of the plate array.
	
	Private p As CPlates
	'local variable(s) to hold property value(s)
	Private mvarXSize As Integer
	Private mvarYSize As Integer
	Private mvarMoveRate As Integer
	Private mvarMaxStep As Integer
	Private mvarMaxBump As Integer
	Private mvarBumpTol As Integer
	Private mvarDrawEvery As Integer
	Private mvarPrintMode As modTecGlobals.PrintMode_E
	Private mvarHydroPct As Integer
	Private mvarZInit As Integer
	Private mvarZSubsume As Integer
	Private mvarZCoast As Integer
	Private mvarZShelf As Integer
	Private mvarZMountain As Integer
	Private mvarZErode As Integer
	Private mvarRiftPct As Integer
	Private mvarDoErode As Integer
	Private mvarErodeRnd As Integer
	Private mvarMaxCtrTry As Integer
	Private mvarRiftDist As Integer
	Private mvarBendEvery As Integer
	Private mvarBendBy As Integer
	Private mvarSpeedRng As Integer
	Private mvarSpeedBase As Integer
	Private mvarUnderScan As Integer
	Private mvarExportBitmap As Boolean
	Private mvarZOffset As Integer
	
	Public Property ZOffset() As Integer
		Get
			ZOffset = mvarZOffset
		End Get
		Set(ByVal Value As Integer)
			mvarZOffset = Value
		End Set
	End Property
	
	
	
	Public Property ExportBitmap() As Boolean
		Get
			ExportBitmap = mvarExportBitmap
		End Get
		Set(ByVal Value As Boolean)
			mvarExportBitmap = Value
		End Set
	End Property
	
	Public Property UnderScan() As Integer
		Get
			UnderScan = mvarUnderScan
		End Get
		Set(ByVal Value As Integer)
			mvarUnderScan = Value
		End Set
	End Property
	
	
	
	Public Property SpeedBase() As Integer
		Get
			SpeedBase = mvarSpeedBase
		End Get
		Set(ByVal Value As Integer)
			mvarSpeedBase = Value
		End Set
	End Property
	
	
	
	Public Property SpeedRng() As Integer
		Get
			SpeedRng = mvarSpeedRng
		End Get
		Set(ByVal Value As Integer)
			mvarSpeedRng = Value
		End Set
	End Property
	
	
	
	Public Property BendBy() As Integer
		Get
			BendBy = mvarBendBy
		End Get
		Set(ByVal Value As Integer)
			mvarBendBy = Value
		End Set
	End Property
	
	
	
	Public Property BendEvery() As Integer
		Get
			BendEvery = mvarBendEvery
		End Get
		Set(ByVal Value As Integer)
			mvarBendEvery = Value
		End Set
	End Property
	
	
	
	Public Property RiftDist() As Integer
		Get
			RiftDist = mvarRiftDist
		End Get
		Set(ByVal Value As Integer)
			mvarRiftDist = Value
		End Set
	End Property
	
	
	
	Public Property MaxCtrTry() As Integer
		Get
			MaxCtrTry = mvarMaxCtrTry
		End Get
		Set(ByVal Value As Integer)
			mvarMaxCtrTry = Value
		End Set
	End Property
	
	
	
	Public Property ErodeRnd() As Integer
		Get
			ErodeRnd = mvarErodeRnd
		End Get
		Set(ByVal Value As Integer)
			mvarErodeRnd = Value
		End Set
	End Property
	
	
	
	Public Property DoErode() As Integer
		Get
			DoErode = mvarDoErode
		End Get
		Set(ByVal Value As Integer)
			mvarDoErode = Value
		End Set
	End Property
	
	
	
	Public Property RiftPct() As Integer
		Get
			RiftPct = mvarRiftPct
		End Get
		Set(ByVal Value As Integer)
			mvarRiftPct = Value
		End Set
	End Property
	
	
	
	Public Property ZErode() As Integer
		Get
			ZErode = mvarZErode
		End Get
		Set(ByVal Value As Integer)
			mvarZErode = Value
		End Set
	End Property
	
	
	
	Public Property ZShelf() As Integer
		Get
			ZShelf = mvarZShelf
		End Get
		Set(ByVal Value As Integer)
			mvarZShelf = Value
		End Set
	End Property
	
	
	
	Public Property ZCoast() As Integer
		Get
			ZCoast = mvarZCoast
		End Get
		Set(ByVal Value As Integer)
			mvarZCoast = Value
		End Set
	End Property
	
	
	
	Public Property ZSubsume() As Integer
		Get
			ZSubsume = mvarZSubsume
		End Get
		Set(ByVal Value As Integer)
			mvarZSubsume = Value
		End Set
	End Property
	
	
	
	Public Property ZInit() As Integer
		Get
			ZInit = mvarZInit
		End Get
		Set(ByVal Value As Integer)
			mvarZInit = Value
		End Set
	End Property
	
	
	
	Public Property HydroPct() As Integer
		Get
			HydroPct = mvarHydroPct
		End Get
		Set(ByVal Value As Integer)
			mvarHydroPct = Value
		End Set
	End Property
	
	
	
	Public Property PrintMode() As modTecGlobals.PrintMode_E
		Get
			PrintMode = mvarPrintMode
		End Get
		Set(ByVal Value As modTecGlobals.PrintMode_E)
			mvarPrintMode = Value
		End Set
	End Property
	
	
	
	Public Property DrawEvery() As Integer
		Get
			DrawEvery = mvarDrawEvery
		End Get
		Set(ByVal Value As Integer)
			mvarDrawEvery = Value
		End Set
	End Property
	
	
	
	Public Property BumpTol() As Integer
		Get
			BumpTol = mvarBumpTol
		End Get
		Set(ByVal Value As Integer)
			mvarBumpTol = Value
		End Set
	End Property
	
	
	
	Public Property MaxBump() As Integer
		Get
			MaxBump = mvarMaxBump
		End Get
		Set(ByVal Value As Integer)
			mvarMaxBump = Value
		End Set
	End Property
	
	
	
	Public Property MaxStep() As Integer
		Get
			MaxStep = mvarMaxStep
		End Get
		Set(ByVal Value As Integer)
			mvarMaxStep = Value
		End Set
	End Property
	
	Public Property YSize() As Integer
		Get
			YSize = mvarYSize
		End Get
		Set(ByVal Value As Integer)
			If Value > MAXY Then Value = MAXY
			
			mvarYSize = Value
			ReDim m(0, 0, 0)
			ReDim t(0, 0, 0)
            ReDim r(0, 0)
            ReDim e(0, 0)
			ReDim m(2, mvarXSize, mvarYSize)
			ReDim t(2, mvarXSize, mvarYSize)
			ReDim r(mvarXSize, mvarYSize)
			ReDim e(mvarXSize, mvarYSize)
		End Set
	End Property
	
	
	
	Public Property XSize() As Integer
		Get
			XSize = mvarXSize
		End Get
		Set(ByVal Value As Integer)
			If Value > MAXX Then Value = MAXX
			mvarXSize = Value
			ReDim m(0, 0, 0)
			ReDim t(0, 0, 0)
            ReDim r(0, 0)
            ReDim e(0, 0)
			
			
			ReDim m(2, mvarXSize, mvarYSize)
			ReDim t(2, mvarXSize, mvarYSize)
			ReDim r(mvarXSize, mvarYSize)
			ReDim e(mvarXSize, mvarYSize)
			
		End Set
	End Property
	
	
	
	
	Public Property Plates() As CPlates
		Get
            Plates = p
		End Get
		Set(ByVal Value As CPlates)
            p = Value
		End Set
	End Property
	
	
    Public Function GetHeightMap() As Short(,)
        ' need to get just the dest
        GetHeightMap = VB6.CopyArray(finalDEM)
    End Function
	
	
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Initialize_Renamed()
		mvarXSize = MAXX
		mvarYSize = MAXY
		mvarMaxStep = 25
		mvarMaxBump = 809 '50
		mvarBumpTol = 142 '50
		mvarUnderScan = 0
		mvarHydroPct = 75
		mvarDrawEvery = 1
		mvarPrintMode = modTecGlobals.PrintMode_E.PRINTMODE_NONE
		mvarZInit = 22
		mvarZSubsume = 16
		mvarZCoast = 16
        mvarZShelf = 8
		mvarZErode = 1
        mvarRiftPct = 80
		mvarDoErode = 1
		mvarErodeRnd = 4
        mvarMaxCtrTry = 200000
        mvarRiftDist = 7
		mvarBendEvery = 6
		mvarBendBy = 100
		mvarSpeedRng = 853
		mvarSpeedBase = 569
		mvarExportBitmap = False
		
		ReDim m(0, 0, 0)
		ReDim t(0, 0, 0)
		ReDim r(0, 0)
		ReDim e(0, 0)
		
		
		ReDim m(2, mvarXSize, mvarYSize)
		ReDim t(2, mvarXSize, mvarYSize)
		ReDim r(mvarXSize, mvarYSize)
		ReDim e(mvarXSize, mvarYSize)
		
		
        MR = New Object() {1.0#, 1.0#, 1.0#, 0.9, 0.85, 0.75, 0.7, 0.6, 0.4, 0.25, 0.2, 0.15, 0.1, -0.2, -0.5, -0.8, -1.0#}
		MAXLIFE = UBound(MR) ' Length of MR vector
		
		
		
		p = New CPlates
		
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	'UPGRADE_NOTE: Class_Terminate was upgraded to Class_Terminate_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
	Private Sub Class_Terminate_Renamed()
		'UPGRADE_NOTE: Object p may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6E35BFF6-CD74-4B09-9689-3E1A43DF8969"'
		p = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
	
	Public Function RunModel(ByRef numsteps As Integer) As Integer
		'UPGRADE_NOTE: step was upgraded to step_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
		Dim step_Renamed As Integer
		Dim src, dest As Integer
		Dim checkstatus As Integer
		
		'Dim Tect As New LibTec.Tectonics
		Dim i, j As Integer
		
		
		checkstatus = 0
		
		assign_colors(mvarZShelf, mvarZCoast)
		init()
		
		frmTec.ProgressBar1.Maximum = 100
		frmTec.ProgressBar1.Value = frmTec.ProgressBar1.Minimum
		For step_Renamed = 0 To numsteps
			'   This is the sequencing routine called by main once per step.
			'   It just calls the important subfunctions in order:
			'   - trysplit   finds a plate to break up, and computes new velocities
			'   - newdxy     computes the deltas to move each plate this step
			'   - move       moves the plates
			'   - merge      determines results when plates rub together
			'   - erode      erodes the terrain, adding or subtracting altitude
			'   - draw       draw the resulting array once every DRAWEVERY steps
			'   The m and t arrays are double-buffered in the sense that operations go
			'   from m(0) to m(1) or vice-versa; src and dest determine which is which.
			
			src = step_Renamed Mod 2
			dest = 1 - src
			If random(100) < mvarRiftPct Then
				checkstatus = trysplit(src)
				If checkstatus = -1 Then
					RunModel = -1
					Exit Function
				End If
			End If
			
			newdxy()
			move(src, dest)
			merge(dest)
			If mvarDoErode <> 0 Then
				erode(dest)
			End If
			
			If mvarDrawEvery <> 0 Then
				If step_Renamed <> 0 And (step_Renamed Mod mvarDrawEvery) = 0 Then
                    'draw_(modTecGlobals.DrawType_E.DRAW_TEC, modTecGlobals.LineType_E.LINE_NONE, t, dest)
					'draw_ DRAW_PLATE, LINE_NONE, m, dest
					System.Windows.Forms.Application.DoEvents()
					tecst(dest)
				End If
			End If
			
			If (mvarDrawEvery = 0) And (step_Renamed = (mvarMaxStep - 1)) Then
				tecst(dest)
                'draw_(modTecGlobals.DrawType_E.DRAW_TEC, modTecGlobals.LineType_E.LINE_NONE, t, dest)
				'draw_ DRAW_PLATE, LINE_NONE, m, dest
			End If
			
			frmTec.lblStep.Text = CStr((numsteps - step_Renamed) * 10) & " million years ago"
			frmTec.ProgressBar1.Value = 100 * step_Renamed / numsteps
			System.Windows.Forms.Application.DoEvents()
		Next step_Renamed
		
		For i = 0 To UBound(t, 2)
			For j = 0 To UBound(t, 3)
				t(dest, i, j) = t(dest, i, j) + mvarZOffset
				If t(dest, i, j) >= UBound(tecols) Then t(dest, i, j) = UBound(tecols) - 1
				If t(dest, i, j) < 0 Then t(dest, i, j) = 0
			Next j
		Next i
		'  With Tect
		'    .BendBy = mvarBendBy
		'    .BendEvery = mvarBendEvery
		'    .BumpTol = mvarBumpTol
		'    .DoErode = mvarDoErode
		'    .DrawEvery = mvarDrawEvery
		'    .ErodeRnd = mvarErodeRnd
		'    .HydoPct = mvarHydroPct
		'    .MaxAge = 10
		'    .MaxBump = mvarMaxBump
		'    .MaxCtrTry = mvarMaxCtrTry
		'    .MaxStep = mvarMaxStep
		'    .PrintMode = mvarPrintMode
		'    .RiftDist = mvarRiftDist
		'    .RiftPct = mvarRiftPct
		'    .SpeedBase = mvarSpeedBase
		'    .SpeedRng = mvarSpeedRng
		'    .UnderScan = mvarUnderScan
		'    .XSize = mvarXSize
		'    .YSize = mvarYSize
		'    .ZCoast = mvarZCoast
		'    .ZErode = mvarZErode
		'    .ZInit = mvarZInit
		'    .ZMountain = mvarZMountain
		'    .ZShelf = mvarZShelf
		'    .ZSubsume = mvarZSubsume
		'
		'  End With
		'
		'  Tect.Run
		'  Tect.GetDEM t
        'draw_(modTecGlobals.DrawType_E.DRAW_TEC, modTecGlobals.LineType_E.LINE_NONE, t, dest)
		
		' copy it into the final map
		ReDim finalDEM(UBound(t, 2), UBound(t, 3))
		For i = 0 To UBound(t, 2)
			For j = 0 To UBound(t, 3)
				finalDEM(i, j) = t(dest, i, j)
			Next j
		Next i
		Spherify()
        'draw2d_(modTecGlobals.DrawType_E.DRAW_TEC, modTecGlobals.LineType_E.LINE_NONE, finalDEM)
		
		
		' export if flag is set
		Dim strTargetFile As String
		If mvarExportBitmap = True Then
			
			strTargetFile = My.Application.Info.DirectoryPath & "\generated-" & VB6.Format(Now, "yyyy-mm-dd-hh-mm-ss")
			
			' copy m_intImageArray to the byte array
			Dim bytSingleBitArray((UBound(t, 2) + 1) * (UBound(t, 3) + 1)) As Byte
			
			For i = 0 To UBound(t, 2)
				For j = 0 To UBound(t, 3)
					bytSingleBitArray(i + j * UBound(t, 2)) = CByte(t(dest, i, UBound(t, 3) - j))
				Next j
			Next i
			
			'Writes a regular bitmap file
            'createBMPImage(strTargetFile & " bump.bmp", UBound(t, 2), UBound(t, 3), bytSingleBitArray)
			
			' Free the memory containing the image
			ReDim bytSingleBitArray(0)
		End If
		
		frmTec.lblStep.Text = "Present day"
		
		
		'Set Tect = Nothing
		RunModel = 0
	End Function
	
	
	
	Private Function palloc() As Integer
		'  Allocate a plate from the array and return its index.  All the fields
		'   of the plate are initialized to 0, except `next'.  That field is used to
		'   link together the plate structures in use.
		
		Dim X As Integer
		
		If pavail = 0 Then
			panic("No more objects")
			palloc = -1
			Exit Function
		End If
		X = pavail
		pavail = p(X).NextPlate
		p(X).NextPlate = phead
		phead = X
		p(X).area = 0
		p(X).age = 0
		p(X).rx = 0
		p(X).ry = 0
		p(X).odx = 0
		p(X).ody = 0
		p(X).dx = 0
		p(X).dy = 0
		palloc = X
	End Function
	
	Private Function pfree(ByRef n As Integer) As Object
		'   Return a plate array element to the pool of available elements.
		'   To check for infinite loops, the variable guard is incremented
		'   at each operation; if the number of operations exceeds the maximum
		'   possible number, the program panics.
		
		Dim i, guard As Integer
		guard = 0
		
		If phead = n Then
			phead = p(n).NextPlate
		Else
			i = phead
			Do 
				guard = guard + 1
				If guard > MAXPLATE Then
                    pfree = -1
                    panic("Infinite loop in pfree")

                    're-initialize the plates using the current plate

					Exit Function
				End If
				i = p(i).NextPlate
				'DoEvents
			Loop While p(i).NextPlate <> n
			p(i).NextPlate = p(n).NextPlate
		End If
		
		p(n).NextPlate = pavail
        pavail = n
        pfree = 0
	End Function
	
	Private Function tecst(ByRef src As Integer) As Integer
		'   This function is called whenever map output is called for.  It looks
		'   at the parameter `printmode' to decide between long text, simple text,
		'   and PostScript output formats.  Note that the default for this
		'   function is no output at all, corresponding to PRINTMODE_NONE.  If only
		'   one output map is desired, then move the coastline up or down to meet the
		'   desired hydrographic percentage.
		
		Dim j, i, lngzcoast As Integer
		Dim hist(256) As Integer
		Dim goal As Integer
        Dim sk(,) As Integer
		ReDim sk(mvarXSize, mvarYSize)
		
		
		If mvarPrintMode = 0 Then
			tecst = 0
			Exit Function
		End If
		
		If mvarDrawEvery = 0 Then
			' Create a histogram of the output array
			For i = 0 To 256
				hist(i) = 0
			Next i
			
			For i = 0 To mvarXSize
				For j = 0 To mvarYSize
					hist(t(src, i, j)) = hist(t(src, i, j)) + 1
				Next j
			Next i
			
			' Starting from the highest altitude, move down until number of
			' squares above water is slightly greater than the exact goal
			goal = mvarXSize * mvarYSize
			goal = (goal * (100 - mvarHydroPct)) / 100
			i = 0
			For lngzcoast = 255 To 0 Step -1
				i = i + hist(lngzcoast)
				If i > goal Then
					Exit For
				End If
			Next lngzcoast
			
			' If the new coast level is zero, then there wasn't enough land
			' to meet the goal, even going right down to the ocean floor.  The
			' only possible result is to panic since the goal can't be met.
			If lngzcoast = 0 Then
				panic("Scaled till oceans dried up")
				tecst = -1
				Exit Function
			End If
			
			mvarZCoast = lngzcoast
		End If
		
		
		tecst = 0
	End Function
	
	Private Function greyscale(ByRef X As Integer) As Double
		' Called by the PostScript print routine, this function simply computes
		' the intensity from 0-1 corresponding to the altitude 0-255
		If (X < mvarZCoast) Then
			greyscale = 0#
		End If
		
		If CShort(1#) - CShort(X > 128) Then
			greyscale = 128 / 128#
		Else
			greyscale = X / 128#
		End If
	End Function
	
	Private Function init() As Integer
		'   This is the catchall function that initializes everything.  First,
		'   it calls getparams() in fileio.c to allow the user to set parameters.  Next,
		'   it links together the plates onto the free list and starts the used list
		'   at empty.  The first plate is created by a fractal technique and then
		'   improved.  Finally, the fractal is copied to the data array and drawn.
		'   There are two kinds of improvement done here.  First, islands are
		'   eliminated by segmenting the blob and erasing all the regions except
		'   for the biggest.  Second, oceans inside the blob (holes) are eliminated
		'   by segmenting the _ocean_ and filling in all regions except the biggest.
		
		Dim besti, X As Integer
		Dim i, j As Integer
        'Dim tmpHydroPct As Integer
		
		For i = 1 To MAXPLATE
			p(i).NextPlate = i + 1
		Next i
		
		p(MAXPLATE - 1).NextPlate = 0
		pavail = 1
		phead = 0
		
		' Allocate a plate structure for the first plate and make a blob
		X = palloc()
		
		Dim blnTest As Boolean
		
		blnTest = True
		
		
		Do 
			ReDim t(2, mvarXSize, mvarYSize)
			ReDim m(2, mvarXSize, mvarYSize)
			
			makefrac(0, X)
			
			' Segment m(0) looking for x, set besti to the largest region,
			' and zero out all the other regions.  This eliminates islands.
			besti = singlefy(0, X)
			If besti > 0 Then
				For i = 1 To mvarXSize
					For j = 1 To mvarYSize
						If kid(r(i, j)) <> besti Then
							m(0, i, j) = 0
						End If
					Next j
				Next i
			End If
			
			' Segment m(0) looking for 0 (ocean), set besti to the largest region,
			' and fill in all the other regions.  This eliminates holes in the blob.
			besti = singlefy(0, 0)
			If besti > 0 Then
				For i = 1 To mvarXSize
					For j = 1 To mvarYSize
						If kid(r(i, j)) <> besti Then
							m(0, i, j) = X
						End If
					Next j
				Next i
			End If
			
			' Fill the topo structure with the blob shape while finding its area
			For i = 0 To mvarXSize
				For j = 0 To mvarYSize
					If m(0, i, j) <> 0 Then
						t(0, i, j) = mvarZInit
						p(X).area = p(X).area + 1
					End If
				Next j
			Next i
			
			' Draw the blob
            'If mvarDrawEvery <> 0 Then
            '	draw_(modTecGlobals.DrawType_E.DRAW_TEC, modTecGlobals.LineType_E.LINE_NONE, t, 0)
            'End If
			
			blnTest = checkSuper(0)
			
			System.Windows.Forms.Application.DoEvents()
		Loop While blnTest = False ' Loop until the corners are ocean
		
	End Function
	
	Private Function trysplit(ByRef src As Integer) As Integer
		'   Trysplit is called at most once per step in only 40% of the steps.
		'   It first draws a rift on one of the plates, then it segments the result
		'   into some number of new plates and some splinters.  If exactly two new
		'   non-splinter plates are found, new plate structures are allocated, new
		'   dx and dy values are computed, and the old plate is freed.  If anything
		'   goes wrong, the rift is erased from the array, returning the array to its
		'   previous state.  The functions newrift, segment and newplates do most
		'   of the work.
		Dim checkstatus As Integer
		
		Dim frag, Count, j, i, a, old, reg As Integer
        Dim tempsum As Integer
		
		checkstatus = 0
		
		If newrift(src, old) <> 0 Then
			If segment(src, old, frag, reg) <> 0 Then
                If reg >= 1 Then

                    ' Set tarea(i) to areas of the final segmented regions
                    For i = 0 To MAXPLATE
                        tarea(i) = 0
                    Next i

                    For i = 1 To frag + 1
                        tarea(kid(i)) = tarea(kid(i)) + karea(i)
                    Next i

                    ' Give up unless exactly two regions are large enough
                    Count = 0
                    For i = 1 To reg + 1
                        If tarea(i) > MAXSPLINTER Then
                            Count = Count + 1
                        End If
                    Next i

                    If Count = 2 Then

                        ' Compute new dx,dy; update m with the ids of the new plates
                        newplates(src, old)
                        Count = 0

                        tempsum = 0

                        For i = 0 To mvarXSize
                            For j = 0 To mvarYSize
                                tempsum = tempsum + kid(a)

                                a = r(i, j)
                                If a <> 0 Then
                                    m(src, i, j) = ids(kid(a))
                                End If

                                If m(src, i, j) = RIFTMARK Then
                                    m(src, i, j) = 0
                                    t(src, i, j) = 0
                                End If
                            Next j
                        Next i

                        checkstatus = pfree(old)
                        If checkstatus = -1 Then
                            trysplit = -1
                            Exit Function
                        End If

                        trysplit = 0
                        Exit Function
                    End If
                End If
			End If
		End If
		
		' If execution reaches here, the split operation failed; remove rift
        If old <> 0 Then
            For i = 0 To mvarXSize
                For j = 0 To mvarYSize
                    If m(src, i, j) = RIFTMARK Then
                        m(src, i, j) = old
                        p(old).area = p(old).area + 1
                    End If
                Next j
            Next i
        End If
		trysplit = 0
	End Function
	
	Private Function newrift(ByRef src As Integer, ByRef old As Integer) As Integer
		'   This function randomly picks a center for a new rift, and draws in
		'   a curving line until the line hits either the coast or another plate.
		'   If another plate is hit, the rift is invalid and the function returns 0.
		'   To find a center, the function generates random x,y values until it
		'   finds one that is at least RIFTDIST squares from any ocean square.  If a
		'   center is found, a random angle is generated; the rift will pass through
		'   the center at that angle.  Next, halfrift() is called twice.  Each call
		'   generates the rift leaving the center in one direction.  If everything
		'   works out, the function returns the id of the plate the rift is on.
		
		Dim tries, i, ty, lx, X, Y, rx, by, j, which As Integer
		Dim tt As Double
		
		tries = 0
		
		' Generate a random x, y value
getctr: 
		If tries > mvarMaxCtrTry Then
			old = 0
			newrift = 0
			Exit Function
		End If
		
		X = random(mvarXSize)
		Y = random(mvarYSize)
		
		' If the location is ocean, try again
		If m(src, X, Y) = 0 Then
			tries = tries + 1
			GoTo getctr
		End If
		
		' Set lx,rx,ty,by to the coordinate values of a box 2*RIFTDIST on a side
		' centered on the center.  Clip the values to make sure they are on the
		' array.  Loop through the box; if a point is ocean, try another center.
		If X < mvarRiftDist Then
			lx = 0
		Else
			lx = X - mvarRiftDist
		End If
		
		If X > mvarXSize - mvarRiftDist - 1 Then
			rx = mvarXSize - 1
		Else
			rx = X + mvarRiftDist
		End If
		
		If Y < mvarRiftDist Then
			ty = 0
		Else
			ty = Y - mvarRiftDist
		End If
		
		If Y > mvarYSize - mvarRiftDist - 1 Then
			by = mvarYSize - 1
		Else
			by = Y + mvarRiftDist
		End If
		
		For i = lx To rx
			For j = ty To by
				If m(src, i, j) = 0 Then
					tries = tries + 1
					GoTo getctr
				End If
			Next j
		Next i
		
		' Found a good center, on plate `which'.  Put a rift indicator in the
		' center.  Generate a random angle t, which is really an integer in the
		' range 0-499 multiplied by 2 PI / 1000.  Call halfrift once for each
		' half of the rift; t is the initial angle for the first call, and
		' t + PI is the initial angle for the second call.  If halfrift()
		' returns zero, abort and return 0; otherwise, return the plate id.
		which = m(src, X, Y)
		m(src, X, Y) = RIFTMARK
		tt = random(500) * TWOMILLIPI
		If halfrift(src, X, Y, which, tt) = 0 Then
			old = which
			newrift = 0
			Exit Function
		End If
		
		tt = tt + PI
		If tt > TWOPI Then tt = tt - TWOPI
		
		If halfrift(src, X, Y, which, tt) = 0 Then
			old = which
			newrift = 0
			Exit Function
		End If
		
		old = which
		newrift = 1
	End Function
	
	Private Function halfrift(ByRef src As Integer, ByRef cx As Integer, ByRef cy As Integer, ByRef which As Integer, ByRef tt As Double) As Integer
		'   Draw a rift from cx,cy on plate `which' at angle t.  At the beginning,
		'   digitize the angle using Bresenham's algorithm; once in a while thereafter,
		'   modify the angle randomly and digitize it again.  For each square travelled,
		'   call stoprift() to see if the rift has left the plate.
		
		Dim i, rdy, ddy, ddx, rdx, DoDraw, a As Integer
		Dim adx, dx, dy, ady As Double
		
		'checkmouse
		
		' For-loop against SIZE to guard against infinite loops
		For i = 0 To mvarXSize
			
			' If first square or 1/6 chance at each step, digitize
			If (i = 0) Or (random(mvarBendEvery) = 0) Then
				
				' If not first step, modify angle a little
				If i <> 0 Then
					tt = tt + (random(mvarBendBy * 2) * TWOMILLIPI) - (mvarBendBy * TWOMILLIPI)
				End If
				If (tt > TWOPI) Then
					tt = tt - TWOPI
				End If
				If (tt < 0) Then
					tt = tt + TWOPI
				End If
				
				' Compute dx and dy, scaled so that larger is exactly +1.0 or -1.0
				dy = System.Math.Sin(tt)
				dx = System.Math.Cos(tt)
				adx = System.Math.Abs(dx)
				ady = System.Math.Abs(dy)
				If adx > ady Then
					dy = dy / adx
					If dx < 0 Then
						dx = -1#
					Else
						dx = 1#
					End If
				Else
					dx = dx / ady
					If dy < 0 Then
						dy = -1#
					Else
						dy = 1#
					End If
				End If
				
				' Convert to integer value and initialize remainder
				' for each coordinate to half value
				ddx = REALSCALE * dx
				ddy = REALSCALE * dy
				rdx = ddx / 2
				rdy = ddy / 2
			End If
			
			' Main part of loop, draws one square along line.  The basic idea
			' of Bresenham's algorithm is that if the slope of the line is less
			' than 45 degrees, each time you step one square in X and maybe step
			' one square in Y.  If the slope is greater than 45, step one square
			' in Y and maybe one square in X.  Here, if the slope is less than 45
			' then ddx == REALSCALE (or -REALSCALE) and the first call to
			' stoprift() is guaranteed.  If stoprift returns <0, all is ok;
			' if zero, the rift ran into the ocean, so stop now; if positive, the
			' rift ran into another plate, which is a perverse condition and the
			' rift must be abandoned.
			rdx = rdx + ddx
			rdy = rdy + ddy
			
			If rdx >= REALSCALE Then
				cx = cx + 1
				rdx = rdx - REALSCALE
				DoDraw = 1
			End If
			
			If rdx <= -REALSCALE Then
				cx = cx - 1
				rdx = rdx + REALSCALE
				DoDraw = 1
			End If
			
			If DoDraw = 1 Then
				If cx > 0 And cy > 0 And cx < mvarXSize And cy < mvarYSize Then
					a = stoprift(src, cx, cy, which)
				End If
				If a >= 0 Then
					halfrift = Not a
					Exit Function
				End If
			End If
			
			If rdy >= REALSCALE Then
				cy = cy + 1
				rdy = rdy - REALSCALE
				DoDraw = 2
			End If
			
			If rdy <= -REALSCALE Then
				cy = cy - 1
				rdy = rdy + REALSCALE
				DoDraw = 2
			End If
			
			If DoDraw = 2 Then
				If cx > 0 And cy > 0 And cx < mvarXSize And cy < mvarYSize Then
					a = stoprift(src, cx, cy, which)
				End If
				If a >= 0 Then
					halfrift = Not a
					Exit Function
				End If
			End If
		Next i
		halfrift = 1
	End Function
	
	Private Function stoprift(ByRef src As Integer, ByRef X As Integer, ByRef Y As Integer, ByRef which As Integer) As Integer
		' This function is called once for each square the rift enters.  It
		'   puts a rift marker into m(src) and decides whether the rift can go on.
		'   It looks at all four adjacent squares.  If one of them contains ocean
		'   or another plate, return immediately so that the rift stops (if ocean)
		'   or aborts (if another plate).  If none of them do, then check to make
		'   sure the point is within underscan boundaries.  If so, return ok.
		
		Dim w, a As Integer
		
		w = which
		p(w).area = p(w).area - 1
		m(src, X, Y) = RIFTMARK
		a = m(src, X, Y + 1)
		If (a <> w) And (a <> RIFTMARK) Then
			If a <> 0 Then
				stoprift = 1
			Else
				stoprift = 0
			End If
			Exit Function
		End If
		
		If Y > 0 Then
			a = m(src, X, Y - 1)
			If (a <> w) And (a <> RIFTMARK) Then
				If a <> 0 Then
					stoprift = 1
				Else
					stoprift = 0
				End If
				Exit Function
			End If
		End If
		
		If X < UBound(m, 2) Then
			a = m(src, X + 1, Y)
			If (a <> w) And (a <> RIFTMARK) Then
				If a <> 0 Then
					stoprift = 1
				Else
					stoprift = 0
				End If
				Exit Function
			End If
		End If
		
		If X > 0 Then
			a = m(src, X - 1, Y)
			If (a <> w) And (a <> RIFTMARK) Then
				If a <> 0 Then
					stoprift = 1
				Else
					stoprift = 0
				End If
				Exit Function
			End If
		End If
		
		If (X < mvarUnderScan) Or (X > mvarXSize - mvarUnderScan) Then
			stoprift = 1
			Exit Function
		End If
		
		If (Y < mvarUnderScan) Or (Y > mvarYSize - mvarUnderScan) Then
			stoprift = 1
			Exit Function
		End If
		
		stoprift = -1
	End Function
	
	Private Function segment(ByRef src As Integer, ByRef match As Integer, ByRef frag As Integer, ByRef reg As Integer) As Integer
		'   This routine implements a standard binary-blob segmentation.  It looks
		'   at the array m(src); match is the value of the blob, and everything else
		'   is background.  The result is placed into array r and vectors kid and karea.
		'   One 8-connected region can be made up of many fragments; each fragment is
		'   assigned a unique index.  Array r contains the frag indices k, while kid(k)
		'   is the region frag k belongs to and karea(k) is the area of frag k.
		'   Variables frag and reg are set on output to the number of fragments and
		'   regions found during the segmentation.  The private vector kk provides one
		'   level of indirection for merging fragments; fragment k is merged with
		'   fragment kk(k) where kk(k) is the smallest frag index in the region.
		
		Dim k3, k1, j, i, k, k2, L As Integer
		Dim kk(MAXFRAG) As Integer
		
		' Initialize all frag areas to zero and every frag to merge with itself
		For k = 0 To MAXFRAG
			kk(k) = k
			karea(k) = 0
		Next k
		'checkmouse ();
		
		' Look at every point in the array
		k = 0
		For i = 0 To mvarXSize
			For j = 0 To mvarYSize
				
				' If too many fragments, give up
				If k = MAXFRAG Then
					segment = 0
					Exit Function
				End If
				
				' If this square isn't part of the blob, try the next square
				If m(src, i, j) <> match Then
					r(i, j) = 0
					
				Else
					
					' It is part of the blob.  Set k1 to the frag id of the square to
					' its left, and set k2 to the frag id of the square above it.  Note
					' that because of the for-loop direction, both of these squares have
					' already been processed.
					If i <> 0 Then
						k1 = kk(r(i - 1, j))
					Else
						k1 = 0
					End If
					
					If j <> 0 Then
						k2 = kk(r(i, j - 1))
					Else
						k2 = 0
					End If
					
					' If k1 and k2 are both background, start a new fragment
					If (k1 = 0 And k2 = 0) Then
						k = k + 1
						r(i, j) = k
						karea(k) = karea(k) + 1
					Else
						
						' If k1 and k2 are part of the same frag, add this square to it
						If (k1 <> 0) And (k1 = k2) Then
							r(i, j) = k1
							karea(k1) = karea(k1) + 1
						Else
							
							' If k1 and k2 belong to different frags, merge them by finding
							' all the frags merged with max(k1,k2) and merging them instead
							' with min(k1,k2).  Add k to that fragment as well.
							If (k1 <> 0) And (k2 <> 0) Then
								If (k2 < k1) Then
									k3 = k1
									k1 = k2
									k2 = k3
								End If
								For L = 1 To k + 1
									If kk(L) = k2 Then
										kk(L) = k1
									End If
								Next L
								r(i, j) = k1
								karea(k1) = karea(k1) + 1
							Else
								
								' Default case is that one of k1,k2 is a fragment and the other is
								' background.  Add k to the fragment.
								If (k1 <> 0) Then
									k3 = k1
								Else
									k3 = k2
								End If
								r(i, j) = k3
								karea(k3) = karea(k3) + 1
							End If
						End If
					End If
				End If
				
			Next j
		Next i
		
		' Set up vector kid to map from fragments to regions by using i to count
		' unique groups of fragments.  A unique group of fragments is
		' characterized by kk(k) == k; otherwise, frag k is merged with some
		' other fragment.
		i = 0
		For j = 1 To k + 1
			If j = kk(j) Then
				i = i + 1
				kid(j) = i
			Else
				kid(j) = kid(kk(j))
			End If
		Next j
		
		' Make sure the id of the background is zero; set up return values
		kid(0) = 0
		frag = k
		reg = i
		segment = 1
	End Function
	
	
	Private Sub newplates(ByRef src As Integer, ByRef old As Integer)
		' Compute new dx and dy values for plates right after fragmentation.  This
		'   function looks at the rift markers in m(src); variable old is the index of
		'   the plate from which the new plates were created.  For each plate adjacent
		'   to the rift, this function subtracts the number of plate squares to the left
		'   of the rift from the number to the right; this gives some indication of
		'   whether the plate should move left or right, and how fast.  The same is done
		'   for squares above and below the rift.  The results are put into dx() and
		'   dy().  At this point some unscaled movement vector is available for both of
		'   the new plates.  The vectors are then scaled by the relative sizes of the
		'   plates.  The idea is that if one plate is much larger than the other, the
		'   small one should move faster.  New plate structures are allocated for the
		'   new plates, and the computed dx and dy values are put in them.
		
		Dim dx(MAXPLATE) As Integer
		Dim dy(MAXPLATE) As Integer
		Dim totarea, j, i, a, maxmag As Integer
		Dim dblScale, b As Double
		
		totarea = 0
		maxmag = 0
		
		For i = 1 To MAXPLATE
			dx(i) = 0
			dy(i) = 0
			ids(i) = 0
		Next i
		
		'checkmouse ();
		
		' For every point in the array, set a to the region id (kid is the
		' lookup table and r contains frag indices); if a is nonzero and
		' the rift is adjacent, adjust counters appropriately
		For i = 0 To mvarXSize
			For j = 0 To mvarYSize
				a = kid(r(i, j))
				If a <> 0 Then
					If (i - 1 > -1) Then
						If m(src, i - 1, j) = RIFTMARK Then
							dx(a) = dx(a) + 1
						End If
					End If
					
					If (i + 1 < mvarXSize) Then
						If m(src, i + 1, j) = RIFTMARK Then
							dx(a) = dx(a) - 1
						End If
					End If
					
					If (j - 1 > -1) Then
						If m(src, i, j - 1) = RIFTMARK Then
							dy(a) = dy(a) + 1
						End If
					End If
					
					If (j + 1 < mvarYSize) Then
						If m(src, i, j + 1) = RIFTMARK Then
							dy(a) = dy(a) - 1
						End If
					End If
				End If
			Next j
		Next i
		
		' For those regions larger than splinters (tarea is set up in trysplit),
		' allocate a new plate structure and initialize its area; compute the
		' magnitude of the dx dy vector and remember the maximum magnitude; also
		' record the total area of new regions
		For i = 1 To MAXPLATE
			If tarea(i) > MAXSPLINTER Then
				ids(i) = palloc()
				p(ids(i)).area = tarea(i)
				totarea = totarea + tarea(i)
				a = System.Math.Sqrt(CDbl((dx(i) * dx(i)) + (dy(i) * dy(i))))
				If a > maxmag Then maxmag = a
			End If
		Next i
		
		' Generate a random speed and predivide so that all speeds computed
		' below are less than the random speed.
		If maxmag <> 0 Then
			dblScale = CDbl(random(mvarSpeedRng) + mvarSpeedBase) / (maxmag * totarea)
		End If
		
		' Compute the dx and dy for each new plate; note that the speed the
		' plate was moving at before splitting is given by p(old).odx,ody
		' but those must be multiplied by MR to get the actual values
		For i = 1 To MAXPLATE
			If ids(i) <> 0 Then
				b = dblScale * (totarea - tarea(i))
                p(ids(i)).odx = p(old).odx * MR(p(old).age) + dx(i) * b
                p(ids(i)).ody = p(old).ody * MR(p(old).age) + dy(i) * b
			End If
		Next i
	End Sub
	
	
	Private Sub makefrac(ByRef src As Integer, ByRef match As Integer)
		' This function uses a very simple fractal technique to draw a blob.  Four
		'   one-dimensional fractals are created and stored in array x, then the
		'   fractals are superimposed on a square array, summed and thresholded to
		'   produce a binary blob.  Squares in the blob are set to the value in `match'.
		'   A one-dimensional fractal of length n is computed like this.  First,
		'   set x (n/2) to some height and set the endpoints (x(0) and x(n)) to 0.
		'   Then do log-n iterations.  The first iteration computes 2 more values:
		'   x(n/4) = average of x(0) and x(n/2), plus some random number, and
		'   x(3n/4) = average of x(n/2) and x(n), plus some random number.  The second
		'   iteration computes 4 more values (x(n/8), x(3n/8), ...) and so on.  The
		'   random number gets smaller by a factor of two each time also.
		'
		'   Anyway, you wind up with a number sequence that looks like the cross-section
		'   of a mountain.  If you sum two fractals, one horizontal and one vertical,
		'   you get a 3-d mountain; but it looks too symmetric.  If you sum four,
		'   including the two 45 degree diagonals, you get much better results.
		Dim FractalSize As Integer
		
		Dim j, inc, dist, xy, n, i, k As Integer
        Dim X(,) As Integer
		Dim ymax, ymin, yofs, xofs, xmin, xmax, bloblevel As Integer
		Dim hist() As Integer
		Dim logval As Single
		
		
		'ReDim hist(256) As Long
		
		If mvarXSize < mvarYSize Then
			logval = System.Math.Log(mvarXSize) / System.Math.Log(2)
			If Fix(logval) = logval Then
				FractalSize = mvarXSize
			Else
				FractalSize = 2 ^ Fix(System.Math.Log(mvarXSize) / System.Math.Log(2))
			End If
		Else
			logval = System.Math.Log(mvarYSize) / System.Math.Log(2)
			If Fix(logval) = logval Then
				FractalSize = mvarYSize
			Else
				FractalSize = 2 ^ Fix(System.Math.Log(mvarYSize) / System.Math.Log(2))
			End If
		End If
		
		ReDim X(4, FractalSize + 1)
		
		ReDim hist(FractalSize)
		
		' Compute offsets to put center of blob in center of the world, and
		' compute array limits to clip blob to world size
		xofs = (mvarXSize - FractalSize) / 2
		yofs = (mvarYSize - FractalSize) / 2
		
		
		If (xofs < 0) Then
			xmin = -xofs
			xmax = FractalSize + xofs
		Else
			xmin = 0
			xmax = FractalSize
		End If
		
		If (yofs < 0) Then
			ymin = -yofs
			ymax = FractalSize + yofs
		Else
			ymin = 0
			ymax = FractalSize
		End If
		
		For xy = 0 To 4
			' Initialize loop values and fractal endpoints
			X(xy, 0) = 0
			X(xy, FractalSize) = 0
			dist = (FractalSize / 2)
			X(xy, FractalSize / 2) = (FractalSize * 3 / 8) + random(FractalSize / 4)
			n = 2
			inc = (FractalSize / 4)
			
			' Loop log-n times, each time halving distance and doubling iterations
			For i = 0 To System.Math.Log(FractalSize) / System.Math.Log(2) - 3
				dist = dist / 2
				n = n * 2
				inc = inc / 2
				
				k = 0
				For j = 0 To n
					k = k + dist
					If k + dist < UBound(X, 2) Then
						X(xy, k + inc) = ((X(xy, k) + X(xy, k + dist)) / 2) + random(dist) - inc
					End If
				Next j
			Next i
		Next xy
		
		' Superimpose fractals into the output array.  x(0) is horizontal, x(1)
		' vertical, x(2) diagonal from top left to bottom right, x(3) diagonal
		' from TR to BL.  While superimposing, create a histogram of the values.
		For i = 0 To FractalSize '256
			hist(i) = 0
		Next i
		
		For i = xmin To xmax
			For j = ymin To ymax
				k = X(0, i) + X(1, j) + X(2, (i - j + FractalSize) / 2) + X(3, (i + j) / 2)
				If k < 0 Then k = 0
				If k > FractalSize Then k = FractalSize ' 255
				hist(k) = hist(k) + 1
				m(src, i + xofs, j + yofs) = k
			Next j
		Next i
		
		' Pick a threshhold to get as close to the goal number of squares as
		' possible, then go back through the array and adjust it
		bloblevel = mvarXSize * mvarYSize * (100 - mvarHydroPct) / 100
		i = 0
		For k = FractalSize To 0 Step -1
			i = i + hist(k)
			If i > bloblevel Then
				Exit For
			End If
		Next k
		
		For i = xmin To xmax
			For j = ymin To ymax
				If m(src, i + xofs, j + yofs) > k Then
					m(src, i + xofs, j + yofs) = match
				Else
					m(src, i + xofs, j + yofs) = 0
				End If
			Next j
		Next i
	End Sub
	
	Private Function singlefy(ByRef src As Integer, ByRef match As Integer) As Integer
		' This is a subfunction of init() which is called twice to improve the
		'   fractal blob.  It calls segment() and then interprets the result.  If
		'   only one region was found, no improvement is needed; otherwise, the
		'   area of each region must be computed by summing the areas of all its
		'   fragments, and the index of the largest region is returned.
		
		Dim besti, reg, i, frag, besta As Integer
		
		segment(src, match, frag, reg)
		If reg = 1 Then
			singlefy = -1
			Exit Function ' No improvement needed
		End If
		
		' Initialize the areas to zero, then sum frag areas
		For i = 1 To reg + 1
			tarea(i) = 0
		Next i
		
		For i = 1 To frag + 1
			tarea(kid(i)) = tarea(kid(i)) + karea(i)
		Next i
		
		' Pick largest area of all regions and return it
		besta = 0
		besti = 0
		For i = 1 To reg + 1
			If (besta < tarea(i)) Then
				besti = i
				besta = tarea(i)
			End If
		Next i
		singlefy = besti
	End Function
	
	Private Sub newdxy()
		' For each plate, compute how many squares it should move this step.
		'   Multiply the plate's basic movement vector odx,ody by the age modifier
		'   MR(), then add the remainders rx,ry from the last move to get some large
		'   integers.  Then turn the large integers into small ones by dividing by
		'   REALSCALE and putting the remainders back into rx,ry.  This function also
		'   increases the age of each plate, but doesn't let the age of any plate
		'   go above MAXLIFE.  This is done to make sure that MR() does not need to
		'   be a long vector.
		
		Dim i, a As Integer
		
		' If there is only a single supercontinent, anchor it
		i = phead
		a = 0
		Do 
			i = p(i).NextPlate
			a = a + 1
			'DoEvents
		Loop While i <> 0
		
		If a = 1 Then
			If p(phead).odx <> 0 Or p(phead).ody <> 0 Then
				p(phead).odx = 0
				p(phead).ody = 0
			End If
		End If
		
		
		i = phead
		Do 
            a = (p(i).odx * MR(p(i).age)) + p(i).rx
			p(i).dx = (a / REALSCALE) * (mvarXSize / 175)
			p(i).rx = (a Mod REALSCALE) * (mvarXSize / 175)
			
            a = (p(i).ody * MR(p(i).age)) + p(i).ry
			p(i).dy = ((a / REALSCALE) / 2) * (mvarXSize / 175)
			p(i).ry = ((a Mod REALSCALE) / 2) * (mvarXSize / 175)
			
			If p(i).age < MAXLIFE - 1 Then
				p(i).age = p(i).age + 1
			End If
			
			i = p(i).NextPlate
			'DoEvents
		Loop While i <> 0
	End Sub
	
	
	Private Sub move(ByRef src As Integer, ByRef dest As Integer)
		' This function moves all the plates that are drifting.  The amount to
		'   move by is determined in newdxy().  The function simply steps through
		'   every square in the array; if there's a plate in a square, its new location
		'   is found and the topography is moved there.  Overlaps between plates are
		'   detected and recorded so that merge() can resolve the collision; mountain
		'   growing is performed.  If two land squares wind up on top of each other,
		'   folded mountains are produced.  If a land square winds up where ocean was
		'   previously, that square is the leading edge of a continent and grows a
		'   mountain by subsuming the ocean basin.
		
		Dim X, b, j, i, a, c, Y As Integer
		
		' Clear out the merge matrix and the destination arrays
		For i = 1 To MAXPLATE
			For j = 1 To MAXPLATE
				mm(i, j) = 0
			Next j
		Next i
		
		For i = 0 To mvarXSize
			For j = 0 To mvarYSize
				m(dest, i, j) = 0
				t(dest, i, j) = 0
			Next j
		Next i
		
		'checkmouse ();
		
		' Look at every square which belongs to a plate
		For i = 0 To mvarXSize
			For j = 0 To mvarYSize
				a = m(src, i, j)
				If a <> 0 Then
					
					' Add the plate's dx,dy to the position to get the square's new
					' location; if it is off the map, throw it away
                    X = (p(a).dx + i) Mod mvarXSize
                    Y = (p(a).dy + j) Mod mvarYSize
					
					'        If y > mvarYSize Then
					'          y = mvarYSize - y
					'          x = mvarXSize - x
					'        End If
					
					' wrap around
					If X < 0 Then
						X = mvarXSize + X
					End If
					
					If Y < 0 Then
						Y = mvarYSize + Y
						'x = mvarXSize - x
					End If
					
					If (X >= mvarXSize) Or (X < 0) Or (Y >= mvarYSize) Or (Y < 0) Then
						p(a).area = p(a).area - 1
					Else ' It IS on the map
						
						' If the destination is occupied, remove the other guy but
						' remember that the two plates overlapped; set the new height
						' to the larger height plus half the smaller.
						c = m(dest, X, Y)
						If c <> 0 Then
							mm(a, c) = mm(a, c) + 1
							p(c).area = p(c).area - 1
							b = t(src, i, j)
							c = t(dest, X, Y)
							If (b > c) Then
								t(dest, X, Y) = b + (c / 2)
							Else
								t(dest, X, Y) = c + (b / 2)
							End If
							If t(dest, X, Y) > MAXMOUNTAINHEIGHT Then t(dest, X, Y) = MAXMOUNTAINHEIGHT
							
							' The destination isn't occupied.  Just copy the height.
						Else
							t(dest, X, Y) = t(src, i, j)
						End If
						
						' If this square is over ocean, increase its height.
						If t(src, X, Y) < mvarZCoast Then
							t(dest, X, Y) = t(dest, X, Y) + mvarZSubsume
							If t(dest, X, Y) > MAXMOUNTAINHEIGHT Then t(dest, X, Y) = MAXMOUNTAINHEIGHT
						End If
						
						' Plate A now owns this square
						m(dest, X, Y) = a
					End If
				End If
			Next j
		Next i
	End Sub
	
	
	Private Sub merge(ByRef dest As Integer)
		' Since move has set up the merge matrix, most of the work is done.  This
		'   function calls bump once for each pair of plates which are rubbing; note
		'   that a and b below loop through the lower diagonal part of the matrix.
		'   One subtle feature is that a plate can bump with several other plates in
		'   a step; suppose that the plate is merged with the first plate it bumped.
		'   The loop will try to bump the vanished plate with the other plates, which
		'   would be wrong.  To avoid this, the lookup table lut is used to provide
		'   a level of indirection.  When a plate is merged with another, its lut
		'   entry is changed to indicate that future merges with the vanished plate
		'   should be applied to the plate it has just been merged with.
		
		Dim lut(MAXPLATE) As Integer
		Dim bb, aa, a, b, i As Integer
		
		For a = 1 To MAXPLATE
			lut(a) = a
		Next a
		
		For a = 2 To MAXPLATE
			For b = 1 To a
				If (mm(a, b) <> 0) Or (mm(b, a) <> 0) Then
					aa = lut(a)
					bb = lut(b)
					If (aa <> bb) Then
						If bump(dest, aa, bb) <> 0 Then
							lut(aa) = bb
							For i = 1 To MAXPLATE
								If lut(i) = aa Then
									lut(i) = bb
								End If
							Next i
						End If
					End If
				End If
			Next b
		Next a
		
	End Sub
	
	
	Private Function bump(ByRef dest As Integer, ByRef a As Integer, ByRef b As Integer) As Integer
		'   Plates a and b have been moved on top of each other by some amount;
		'   alter their movement rates for a slow collision, possibly merging them.
		'   The collision "strength" is a ratio of the area overlap (provided by
		'   move ()) to the total area of the plates involved.  A fraction of each
		'   plate's current movement vector is subtracted from the movement vector
		'   of the other plate.  If the two vectors are now within some tolerance
		'   of each other, they are almost at rest so merge them with each other.
		
		Dim rat, ta, maa, mab, tb, area As Double
		Dim j, i, X As Integer
		
		'checkmouse();
		
		' Find a ratio describing how strong the collision is
		X = mm(a, b) + mm(b, a)
		area = p(a).area + p(b).area
		rat = X / (mvarMaxBump + (area / 20))
		If rat > 1# Then rat = 1#
		
		' Do some math to update the move vectors.  This looks complicated
		' because a plate's actual movement vector must be multiplied by
		' MR(age), and because I have rewritten the equations to maximize
		' use of common factors.  Trust me, it's just inelastic collision.
        maa = p(a).area * MR(p(a).age)
        mab = p(b).area * MR(p(b).age)
        ta = MR(p(a).age) * area
		p(a).odx = (p(a).odx * maa + p(b).odx * mab * rat) / ta
		p(a).ody = (p(a).ody * maa + p(b).ody * mab * rat) / ta
        tb = MR(p(b).age) * area
		p(b).odx = (p(b).odx * mab + p(a).odx * maa * rat) / tb
		p(b).ody = (p(b).ody * mab + p(a).ody * maa * rat) / tb
		
		' For each axis, compute the remaining relative velocity.  If it is
		' too large, return without merging the plates
        If System.Math.Abs(p(a).odx * MR(p(a).age) - p(b).odx * MR(p(b).age)) > mvarBumpTol Then
            bump = 0
            Exit Function
        End If
		
        If System.Math.Abs(p(a).ody * MR(p(a).age) - p(b).ody * MR(p(b).age)) > mvarBumpTol Then
            bump = 0
            Exit Function
        End If
		
		' The relative velocity is small enough, so merge the plates.  Replace
		' all references to a with b, free a, and tell merge() a was freed.
		For i = 0 To mvarXSize
			For j = 0 To mvarYSize
				If m(dest, i, j) = a Then
					m(dest, i, j) = b
				End If
			Next j
		Next i
		
		p(b).area = p(b).area + p(a).area
		pfree(a)
		bump = a
	End Function
	
	Private Sub erode(ByRef dest As Integer)
		' This function takes the topography in t(dest) and smooths it, lowering
		'   mountains and raising lowlands and continental margins.  It does this by
		'   stepping across the entire array and doing a computation once for each
		'   pair of 8-connected pixels.  The computation is done by onerode(), below.
		'   The computation result for a pair is a small delta for each square, which
		'   is summed in the array e.  When the computation is finished, the delta
		'   is applied; if this pushes an ocean square high enough, it is added to
		'   an adjacent plate if one can be found.  Also, if a land square is eroded
		'   low enough, it is turned into ocean and removed from its plate.
		
		Dim Z, j, i, X, xx As Integer
		
		' Zero out the array for the deltas first
		For i = 0 To mvarXSize
			For j = 0 To mvarYSize
				e(i, j) = 0
			Next j
		Next i
		
		'checkmouse();
		
		' Step across the entire array; each pixel is adjacent to 8 others, and
		' it turns out that if four pairs are considered for each pixel, each
		' pair is considered exactly once.  This is important for even erosion
		For i = 1 To mvarXSize
			For j = 1 To mvarYSize
				onerode(dest, i, j, 0, -1)
				onerode(dest, i, j, -1, -1)
				onerode(dest, i, j, -1, 0)
				If i < mvarXSize - 1 Then
					onerode(dest, i, j, -1, 1)
				End If
			Next j
		Next i
		
		' Now go back across the array, applying the delta values from e(,)
		For i = 0 To mvarXSize
			For j = 0 To mvarYSize
				Z = t(dest, i, j) + e(i, j)
				If Z < 0 Then Z = 0
				If Z > 255 Then Z = 255
				
				' If the square just rose above shelf level, look at the four
				' adjacent squares.  If one is a plate, add the square to that plate
				If (Z >= mvarZShelf) And (t(dest, i, j) < mvarZShelf) Then
					xx = 0
					If i >= 1 Then
						If X = m(dest, i - 1, j) Then xx = X
					End If
					If i < mvarXSize - 1 Then
						If X = m(dest, i + 1, j) Then xx = X
					End If
					If j > 1 Then
						If X = m(dest, i, j - 1) Then xx = X
					End If
					If j < mvarYSize - 1 Then
						If X = m(dest, i, j + 1) Then xx = X
					End If
					If (xx) Then
						p(xx).area = p(xx).area + 1
						m(dest, i, j) = xx
					End If
				End If
				
				' Add the increment to the old value; if the square is lower than
				' shelf level but belongs to a plate, remove it from the plate
				t(dest, i, j) = Z
				If (Z < mvarZShelf) Then
					X = m(dest, i, j)
					If X <> 0 Then
						p(X).area = p(X).area - 1
						m(dest, i, j) = 0
					End If
				End If
			Next j
		Next i
	End Sub
	
	
	Private Sub onerode(ByRef dest As Integer, ByRef i As Integer, ByRef j As Integer, ByRef di As Integer, ByRef dj As Integer)
		'   This function is called once per pair of squares in the array.  The
		'   amount each square loses to an adjacent but lower square in each step is
		'   one-eighth the difference in altitude.  This is coded as a shift right 3
		'   bits, but since -1 >> 3 is still -1, the code must be repeated to avoid
		'   shifting negative numbers.  Also, since erosion is reduced below the
		'   waterline, if an altitude is lower than ZERODE, ZERODE is used instead.
		
		Dim t1, Z, t2 As Integer
		
		t1 = t(dest, i, j)
		If i + di < UBound(t, 2) And j + dj < UBound(t, 3) Then
			t2 = t(dest, i + di, j + dj)
		End If
		Z = 0
		If (t1 > t2) And (t1 > mvarZErode) Then
			If t2 < mvarZErode Then t2 = mvarZErode
			Z = ((t1 - t2 + mvarErodeRnd) / 8)
		ElseIf (t2 > t1) And (t2 > mvarZErode) Then 
			If (t1 < mvarZErode) Then t1 = mvarZErode
			Z = -((t2 - t1 + mvarErodeRnd) / 8)
		End If
		If Z <> 0 Then
			e(i, j) = e(i, j) - Z
			e(i + di, j + dj) = e(i + di, j + dj) + Z
		End If
		
	End Sub
	
	Public Sub DrawTopographicReliefMap()
        'draw2d_(modTecGlobals.DrawType_E.DRAW_GREY, modTecGlobals.LineType_E.LINE_NONE, finalDEM)
	End Sub
	
	Public Sub platedraw()
        'draw_(modTecGlobals.DrawType_E.DRAW_PLATE, modTecGlobals.LineType_E.LINE_NONE, m, 0)
	End Sub
	
	'UPGRADE_NOTE: ctype was upgraded to ctype_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'Private Sub draw_(ByRef ctype_Renamed As modTecGlobals.DrawType_E, ByRef ltype As modTecGlobals.LineType_E, ByRef cra() As Short, ByRef buffer As Integer)

    '	Dim k, i, j, X As Integer
    '	Dim lut() As Integer

    '	assign_colors(mvarZShelf, mvarZCoast)
    '	Select Case ctype_Renamed
    '		Case modTecGlobals.DrawType_E.DRAW_GREY
    '			lut = VB6.CopyArray(greycols)
    '		Case modTecGlobals.DrawType_E.DRAW_LAND
    '			lut = VB6.CopyArray(landcols)
    '		Case modTecGlobals.DrawType_E.DRAW_CLIM
    '			lut = VB6.CopyArray(climcols)
    '		Case modTecGlobals.DrawType_E.DRAW_TEC
    '			lut = VB6.CopyArray(tecols)
    '		Case modTecGlobals.DrawType_E.DRAW_PLATE
    '			lut = VB6.CopyArray(platecols)
    '		Case modTecGlobals.DrawType_E.DRAW_JET
    '			lut = VB6.CopyArray(heatcols)
    '	End Select

    '	'lut = greycols

    '	'frmTec.Picture1.Cls
    '	'frmTec.Picture1.Line (0, 0)-(frmTec.Picture1.ScaleWidth, frmTec.Picture1.ScaleHeight), RGB(0, 0, 255), BF

    '	For j = 0 To mvarYSize
    '		For i = 0 To mvarXSize - 1

    '			X = lut(cra(buffer, i, j))
    '			k = i + 1

    '			Do While ((lut(cra(buffer, k, j)) = X) And (k < mvarXSize - 1))
    '				k = k + 1
    '			Loop 

    '			'UPGRADE_ISSUE: PictureBox method Picture1.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '			frmTec.Picture1.Line (i * SIZE * VB6.PixelsToTwipsX(frmTec.Picture1.ClientRectangle.Width) / mvarXSize, j * SIZE * VB6.PixelsToTwipsY(frmTec.Picture1.ClientRectangle.Height) / mvarYSize) - ((k - i) * SIZE * VB6.PixelsToTwipsX(frmTec.Picture1.ClientRectangle.Width) / mvarXSize, SIZE * VB6.PixelsToTwipsY(frmTec.Picture1.ClientRectangle.Height) / mvarYSize), X, BF

    '			i = k - 1

    '		Next i
    '	Next j

    '	'  Select Case ltype
    '	'    Case LINE_DIAG
    '	'      diagonal lra
    '	'    Case LINE_CORN
    '	'      corner lra
    '	'    Case LINE_NONE
    '	'  End Select
    '	'DrawGrid
    '	frmTec.DrawColorBar(lut)
    'End Sub
	
	'UPGRADE_NOTE: ctype was upgraded to ctype_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'Private Sub draw2d_(ByRef ctype_Renamed As modTecGlobals.DrawType_E, ByRef ltype As modTecGlobals.LineType_E, ByRef cra(,) As Short)

    '    Dim k, i, j, X As Integer
    '    Dim lut() As Integer

    '    assign_colors(mvarZShelf, mvarZCoast)
    '    Select Case ctype_Renamed
    '        Case modTecGlobals.DrawType_E.DRAW_GREY
    '            lut = VB6.CopyArray(greycols)
    '        Case modTecGlobals.DrawType_E.DRAW_LAND
    '            lut = VB6.CopyArray(landcols)
    '        Case modTecGlobals.DrawType_E.DRAW_CLIM
    '            lut = VB6.CopyArray(climcols)
    '        Case modTecGlobals.DrawType_E.DRAW_TEC
    '            lut = VB6.CopyArray(tecols)
    '        Case modTecGlobals.DrawType_E.DRAW_PLATE
    '            lut = VB6.CopyArray(platecols)
    '        Case modTecGlobals.DrawType_E.DRAW_JET
    '            lut = VB6.CopyArray(heatcols)
    '    End Select

    '    'lut = greycols

    '    'frmTec.Picture1.Cls
    '    'frmTec.Picture1.Line (0, 0)-(frmTec.Picture1.ScaleWidth, frmTec.Picture1.ScaleHeight), RGB(0, 0, 255), BF

    '    For j = 0 To mvarYSize
    '        For i = 0 To mvarXSize - 1

    '            If cra(i, j) < UBound(lut) Then
    '                X = lut(cra(i, j))

    '                k = i + 1
    '                '        Do While ((lut(cra(k, j)) = x) And (k < mvarXSize - 1))
    '                '          k = k + 1
    '                '          If cra(k, j) > UBound(lut) Then Exit Do
    '                '        Loop

    '            Else
    '                X = lut(UBound(lut))
    '                k = i + 1
    '            End If

    '            'UPGRADE_ISSUE: PictureBox method Picture1.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    'frmTec.Picture1.Line (i * SIZE * VB6.PixelsToTwipsX(frmTec.Picture1.ClientRectangle.Width) / mvarXSize, j * SIZE * VB6.PixelsToTwipsY(frmTec.Picture1.ClientRectangle.Height) / mvarYSize) - ((k - i) * SIZE * VB6.PixelsToTwipsX(frmTec.Picture1.ClientRectangle.Width) / mvarXSize, SIZE * VB6.PixelsToTwipsY(frmTec.Picture1.ClientRectangle.Height) / mvarYSize), X, BF

    '            i = k - 1

    '        Next i
    '    Next j

    '    '  Select Case ltype
    '    '    Case LINE_DIAG
    '    '      diagonal lra
    '    '    Case LINE_CORN
    '    '      corner lra
    '    '    Case LINE_NONE
    '    '  End Select
    '    'DrawGrid
    '    frmTec.DrawColorBar(lut)
    'End Sub
	
	
	
	'UPGRADE_NOTE: ctype was upgraded to ctype_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'Private Sub drawByte_(ByRef ctype_Renamed As modTecGlobals.DrawType_E, ByRef ltype As modTecGlobals.LineType_E, ByRef cra() As Byte, ByRef buffer As Integer)

    '	Dim k, i, j, X As Integer
    '	Dim lut() As Integer

    '	assign_colors(mvarZShelf, mvarZCoast)
    '	Select Case ctype_Renamed
    '		Case modTecGlobals.DrawType_E.DRAW_GREY
    '			lut = VB6.CopyArray(greycols)
    '		Case modTecGlobals.DrawType_E.DRAW_LAND
    '			lut = VB6.CopyArray(landcols)
    '		Case modTecGlobals.DrawType_E.DRAW_CLIM
    '			lut = VB6.CopyArray(climcols)
    '		Case modTecGlobals.DrawType_E.DRAW_TEC
    '			lut = VB6.CopyArray(tecols)
    '		Case modTecGlobals.DrawType_E.DRAW_PLATE
    '			lut = VB6.CopyArray(platecols)
    '		Case modTecGlobals.DrawType_E.DRAW_JET
    '			lut = VB6.CopyArray(heatcols)
    '	End Select

    '	'lut = greycols

    '	'frmTec.Picture1.Cls
    '	'frmTec.Picture1.Line (0, 0)-(frmTec.Picture1.ScaleWidth, frmTec.Picture1.ScaleHeight), RGB(0, 0, 255), BF

    '	For j = 0 To mvarYSize
    '		For i = 0 To mvarXSize - 1

    '			X = lut(cra(buffer, i, j))
    '			k = i + 1

    '			Do While ((lut(cra(buffer, k, j)) = X) And (k < mvarXSize - 1))
    '				k = k + 1
    '			Loop 

    '			'UPGRADE_ISSUE: PictureBox method Picture1.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '			frmTec.Picture1.Line (i * SIZE * VB6.PixelsToTwipsX(frmTec.Picture1.ClientRectangle.Width) / mvarXSize, j * SIZE * VB6.PixelsToTwipsY(frmTec.Picture1.ClientRectangle.Height) / mvarYSize) - ((k - i) * SIZE * VB6.PixelsToTwipsX(frmTec.Picture1.ClientRectangle.Width) / mvarXSize, SIZE * VB6.PixelsToTwipsY(frmTec.Picture1.ClientRectangle.Height) / mvarYSize), X, BF

    '			i = k - 1

    '		Next i
    '	Next j

    '	'  Select Case ltype
    '	'    Case LINE_DIAG
    '	'      diagonal lra
    '	'    Case LINE_CORN
    '	'      corner lra
    '	'    Case LINE_NONE
    '	'  End Select
    '	'DrawGrid
    '	frmTec.DrawColorBar(lut)
    'End Sub
	
	
    'Public Sub diagonal(ByRef ra() As Integer)
    '	Dim i, j As Integer

    '	For j = 0 To mvarYSize
    '		For i = 0 To mvarXSize
    '			Select Case (ra(i, j))
    '				Case 0
    '				Case North
    '					LEFT_Renamed(i, j, 0)
    '				Case South
    '					LEFT_Renamed(i, j, 1)
    '				Case East
    '					ABOVE(i, j, 1)
    '				Case West
    '					ABOVE(i, j, 0)
    '				Case Northeast
    '					UPRIGHT(i, j, 0)
    '				Case Northwest
    '					UPLEFT(i, j, 0)
    '				Case Southeast
    '					UPLEFT(i, j, 1)
    '				Case Southwest
    '					UPRIGHT(i, j, 1)
    '			End Select
    '		Next i
    '	Next j
    'End Sub
	
	
    'Sub corner(ByRef ra() As Integer)

    '	Dim j, i, X As Integer
    '	For j = 0 To mvarYSize
    '		For i = 0 To mvarXSize
    '			X = ra(i, j)
    '			If (X And LINE_0V) Then
    '				LEFT_Renamed(i, j, 0)
    '			ElseIf (X And LINE_1V) Then 
    '				LEFT_Renamed(i, j, 1)
    '			End If

    '			If (X And LINE_0H) Then
    '				ABOVE(i, j, 0)
    '			ElseIf (X And LINE_1H) Then 
    '				ABOVE(i, j, 1)
    '			End If
    '		Next i
    '	Next j
    'End Sub
	
	
	
	
	
	
	Public Function checkSuper(ByRef buf As Integer) As Boolean
		' Returns FALSE if the continent is bad
		
		If (t(buf, 1, 1) > 0) Or (t(buf, 1, UBound(t, 3) - 1) > 0) Or (t(buf, UBound(t, 2) - 1, 1) > 0) Or (t(buf, UBound(t, 2) - 1, UBound(t, 3) - 1) > 0) Then
			checkSuper = False
		Else
			checkSuper = True
		End If
		
	End Function
	
	
	
	
	'Private Sub assign_colors()
	'  Dim i As Long
	'
	'  FillX11ColorTable
	'
	'  color_values(0) = &H0 'black
	'  color_values(1) = &HFFFFFF 'white
	'  color_values(2) = &HBFBFBF 'grey75
	'  color_values(3) = &H7F7F7F 'grey50
	'  color_values(4) = &HAB82FF 'MediumPurple1
	'  color_values(5) = &H551A8B 'purple4
	'  color_values(6) = &H7D26CD 'purple3
	'  color_values(7) = &H912CEE 'purple2
	'  color_values(8) = &H9B30FF 'purple1
	'  color_values(9) = RGB(0, 0, &H8B) 'blue4
	'  color_values(10) = RGB(0, 0, &HCD) 'blue3
	'  color_values(11) = RGB(0, 0, &HEE) 'blue2
	'  color_values(12) = RGB(0, 0, &HFF)     'blue1
	'  color_values(13) = RGB(0, &H64, 0) 'DarkGreen
	'  color_values(14) = RGB(0, &H8B, 0) 'green4
	'  color_values(15) = RGB(0, &HCD, 0) 'green3
	'  color_values(16) = RGB(0, &HEE, 0) 'green2
	'  color_values(17) = RGB(0, &HFF, 0) 'green1
	'  color_values(18) = &H8B6508 'DarkGoldenrod4
	'  color_values(19) = &H8B8B00 'yellow4
	'  color_values(20) = &HCDCD00 'yellow3
	'  color_values(21) = &HEEEE00 'yellow2
	'  color_values(22) = &HFFFF00 'yellow1
	'  color_values(23) = &H8B5A00 'orange4
	'  color_values(24) = &HCD8500 'orange3
	'  color_values(25) = &HEE9A00 'orange2
	'  color_values(26) = &HFFA500 'orange1
	'  color_values(27) = &H8B2323 'brown4
	'  color_values(28) = &H8B0000 'red4
	'  color_values(29) = &HCD0000 'red3
	'  color_values(30) = &HEE0000 'red2
	'  color_values(31) = &HFF0000 'red1
	'
	'  climcols(0) = color_values(9)
	'  climcols(1) = color_values(20)
	'  climcols(2) = color_values(18)
	'  climcols(3) = color_values(2)
	'  climcols(4) = color_values(3)
	'  climcols(5) = color_values(18)
	'  climcols(6) = color_values(22)
	'  climcols(7) = color_values(26)
	'  climcols(8) = color_values(14)
	'  climcols(9) = color_values(17)
	'  climcols(10) = color_values(12)
	'
	'  landcols(0) = color_values(11)
	'  landcols(1) = color_values(28)
	'  landcols(2) = color_values(3)
	'
	'
	'  For i = 0 To 255
	'    'greycols(i) = color_values(Int(4# + CDbl(i) * 27# / 254#))
	'    greycols(i) = RGB(i, i, i)
	'  Next i
	'
	'  For i = 1 To MAXPLATE
	'    platecols(i) = RGB(random(128) + 127, random(128) + 127, random(128) + 127)
	'  Next i
	'
	'  ReDim tecols(MAXMOUNTAINHEIGHT) As Long
	'
	'  ' deep ocean
	'  For i = 0 To mvarZShelf
	'    tecols(i) = color_values(9) ' dark blue
	'  Next i
	'
	'  ' continental shelf
	'  For i = mvarZShelf To mvarZCoast
	'    tecols(i) = RGB(128, 255, 255) ' light blue
	'  Next i
	'
	'  ' Greens for land
	'  For i = mvarZCoast To mvarZCoast + 5000 / (MAXMOUNTAINHEIGHT * 100# / 255)
	'    'tecols(i) = color_values((Int(4# + CDbl(i - 16) * 27# / 62#)))
	'    If i >= UBound(tecols) Then Exit For
	'    tecols(i) = RGB(0, i * 2 + 95, 0)
	'  Next i
	'
	'  ' Brown for above tree line
	'  For i = mvarZCoast + 5000 / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 10000 / (MAXMOUNTAINHEIGHT * 100# / 255)
	'    If i >= UBound(tecols) Then Exit For
	'    tecols(i) = RGB(128, i, i)
	'  Next i
	'
	'  ' White for above snow line
	'  For i = mvarZCoast + 10000 / (MAXMOUNTAINHEIGHT * 100# / 255) To mvarZCoast + 200
	'    If i >= UBound(tecols) Then Exit For
	'    'tecols(i) = color_values(1)
	'    tecols(i) = RGB(150 + i / 2, 150 + i / 2, 150 + i / 2)
	'  Next i
	'
	'  For i = mvarZCoast + 200 To UBound(tecols)
	'    If i >= UBound(tecols) Then Exit For
	'    'tecols(i) = color_values(1)
	'    tecols(i) = RGB(255, 255, 255)
	'  Next i
	'End Sub
	
	
	
	'UPGRADE_NOTE: LEFT was upgraded to LEFT_Renamed. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
    'Sub LEFT_Renamed(ByRef i As Object, ByRef j As Object, ByRef X As Object, Optional ByVal col As Object = 0)
    '	setXYWinScale()
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_LOW Then col = 0
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_HIGH Then col = RGB(255, 255, 255)
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_HEQ Then col = 0
    '	'UPGRADE_WARNING: Couldn't resolve default property of object j. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	'UPGRADE_WARNING: Couldn't resolve default property of object i. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	'UPGRADE_ISSUE: PictureBox method Picture1.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '	frmTec.Picture1.Line (i * SIZE * XWinScale, j * SIZE * YWinScale) - (i * SIZE * XWinScale, (j + 1) * SIZE * YWinScale), col
    'End Sub

    'Sub ABOVE(ByRef i As Object, ByRef j As Object, ByRef X As Object, Optional ByVal col As Object = 0)
    '	setXYWinScale()
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_LOW Then col = 0
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_HIGH Then col = RGB(255, 255, 255)
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_HEQ Then col = 0
    '	'UPGRADE_WARNING: Couldn't resolve default property of object j. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	'UPGRADE_WARNING: Couldn't resolve default property of object i. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	'UPGRADE_ISSUE: PictureBox method Picture1.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '	frmTec.Picture1.Line (i * SIZE * XWinScale, j * SIZE * YWinScale) - ((i + 1) * SIZE * XWinScale, j * SIZE * YWinScale), col
    'End Sub

    'Sub UPLEFT(ByRef i As Object, ByRef j As Object, ByRef X As Object, Optional ByVal col As Object = 0)
    '	setXYWinScale()
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_LOW Then col = 0
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_HIGH Then col = RGB(255, 255, 255)
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_HEQ Then col = 0
    '	'UPGRADE_WARNING: Couldn't resolve default property of object j. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	'UPGRADE_WARNING: Couldn't resolve default property of object i. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	'UPGRADE_ISSUE: PictureBox method Picture1.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '	frmTec.Picture1.Line (i * SIZE * XWinScale, j * SIZE * YWinScale) - ((i + 1) * SIZE * XWinScale, (j + 1) * SIZE * YWinScale), col
    'End Sub

    'Sub UPRIGHT(ByRef i As Object, ByRef j As Object, ByRef X As Object, Optional ByVal col As Object = 0)
    '	setXYWinScale()
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_LOW Then col = 0
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_HIGH Then col = RGB(255, 255, 255)
    '	'UPGRADE_WARNING: Couldn't resolve default property of object col. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	If col = modTecGlobals.Pressure_E.PR_HEQ Then col = 0
    '	'UPGRADE_WARNING: Couldn't resolve default property of object j. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	'UPGRADE_WARNING: Couldn't resolve default property of object i. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
    '	'UPGRADE_ISSUE: PictureBox method Picture1.Line was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
    '	frmTec.Picture1.Line ((i + 1) * SIZE * XWinScale, j * SIZE * YWinScale) - (i * SIZE * XWinScale, (j + 1) * SIZE * YWinScale), col
    'End Sub
	
    'Private Sub setXYWinScale()
    '	XWinScale = VB6.PixelsToTwipsX(frmTec.Picture1.ClientRectangle.Width) / mvarXSize
    '	YWinScale = VB6.PixelsToTwipsY(frmTec.Picture1.ClientRectangle.Height) / mvarYSize
    'End Sub
	
	
	
	Public Sub Spherify()
		Dim i As Integer
		Dim j As Integer
		Dim k As Integer
        Dim st(,) As Short
		Dim sngNumPixels As Single
		Dim lngPixelAvgSize As Integer
		Dim lngHalfHeight As Integer
		Dim lat As Single
		Dim salt As Integer
		
		ReDim st(UBound(finalDEM, 1), UBound(finalDEM, 2))
		
		lngHalfHeight = mvarYSize / 2
		
		For j = 0 To mvarYSize
			lat = PI / 2 - PI * CDbl(j) / CDbl(mvarYSize)
			
			sngNumPixels = CSng(mvarXSize * System.Math.Abs(System.Math.Cos(lat)))
			If sngNumPixels < 1 Then sngNumPixels = 1
			
			
			
			lngPixelAvgSize = mvarXSize / sngNumPixels
			For i = 0 To mvarXSize Step lngPixelAvgSize
				
				salt = 0
				For k = 0 To lngPixelAvgSize
					salt = salt + finalDEM((i + k) Mod mvarXSize, j)
				Next k
				salt = salt / lngPixelAvgSize
				
				For k = 0 To lngPixelAvgSize
					st((i + k) Mod mvarXSize, j) = salt
				Next k
			Next i
		Next j
		
		For i = 0 To UBound(st, 1)
			For j = 0 To UBound(st, 2)
				finalDEM(i, j) = st(i, j)
			Next j
		Next i
		
	End Sub
End Class