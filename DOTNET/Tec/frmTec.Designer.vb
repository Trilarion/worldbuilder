<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmTec
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Public WithEvents cboSize As System.Windows.Forms.ComboBox
    Public WithEvents _StatusBar1_Panel1 As System.Windows.Forms.ToolStripStatusLabel
    Public WithEvents StatusBar1 As System.Windows.Forms.StatusStrip
    Public WithEvents Command1 As System.Windows.Forms.Button
    Public WithEvents Label5 As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	Public WithEvents Label6 As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	Public WithEvents Label7 As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTec))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cboSize = New System.Windows.Forms.ComboBox
        Me.StatusBar1 = New System.Windows.Forms.StatusStrip
        Me._StatusBar1_Panel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.Command1 = New System.Windows.Forms.Button
        Me.Label5 = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(Me.components)
        Me.Label6 = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(Me.components)
        Me.Label7 = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(Me.components)
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.txtMaxMtnHt = New System.Windows.Forms.TextBox
        Me.fraAltitudes = New System.Windows.Forms.GroupBox
        Me.txtZSubsume = New System.Windows.Forms.TextBox
        Me.txtZCoast = New System.Windows.Forms.TextBox
        Me.txtZShelf = New System.Windows.Forms.TextBox
        Me.txtZInit = New System.Windows.Forms.TextBox
        Me.txtHydroPct = New System.Windows.Forms.TextBox
        Me.txtMaxSteps = New System.Windows.Forms.TextBox
        Me._Label1_2 = New System.Windows.Forms.Label
        Me._Label1_3 = New System.Windows.Forms.Label
        Me._Label1_4 = New System.Windows.Forms.Label
        Me._Label1_5 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me._Label1_0 = New System.Windows.Forms.Label
        Me._Label1_13 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me._Label1_14 = New System.Windows.Forms.Label
        Me._Label1_7 = New System.Windows.Forms.Label
        Me._Label1_8 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.LabelArray1 = New Microsoft.VisualBasic.Compatibility.VB6.LabelArray(Me.components)
        Me._Label1_1 = New System.Windows.Forms.Label
        Me._Label1_10 = New System.Windows.Forms.Label
        Me._Label1_9 = New System.Windows.Forms.Label
        Me._Label1_12 = New System.Windows.Forms.Label
        Me._Label1_6 = New System.Windows.Forms.Label
        Me._Label1_11 = New System.Windows.Forms.Label
        Me.txtRiftDist = New System.Windows.Forms.TextBox
        Me.txtBendEvery = New System.Windows.Forms.TextBox
        Me.txtBendBy = New System.Windows.Forms.TextBox
        Me.txtSpeedBase = New System.Windows.Forms.TextBox
        Me.txtSpeedRng = New System.Windows.Forms.TextBox
        Me.txtMaxBump = New System.Windows.Forms.TextBox
        Me.txtBumpTol = New System.Windows.Forms.TextBox
        Me.txtErodeRnd = New System.Windows.Forms.TextBox
        Me.txtRiftPct = New System.Windows.Forms.TextBox
        Me.ToolTip2 = New System.Windows.Forms.ToolTip(Me.components)
        Me.txtMaxCtrTry = New System.Windows.Forms.TextBox
        Me.txtZErode = New System.Windows.Forms.TextBox
        Me.chkDoErode = New System.Windows.Forms.CheckBox
        Me.fraRifts = New System.Windows.Forms.GroupBox
        Me.fraErosion = New System.Windows.Forms.GroupBox
        Me.ProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
        Me.lblStep = New System.Windows.Forms.ToolStripStatusLabel
        Me.StatusBar1.SuspendLayout()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fraAltitudes.SuspendLayout()
        CType(Me.LabelArray1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.fraRifts.SuspendLayout()
        Me.fraErosion.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboSize
        '
        Me.cboSize.BackColor = System.Drawing.SystemColors.Window
        Me.cboSize.Cursor = System.Windows.Forms.Cursors.Default
        Me.cboSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSize.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSize.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cboSize.Items.AddRange(New Object() {"512 (0�42' = 50 mi)", "1024 (0�21' = 25 mi)", "2048 (0�10' = 12 mi)", "4096 (0�5' = 6 mi)", "8192 (0�2' = 3 mi)"})
        Me.cboSize.Location = New System.Drawing.Point(10, 574)
        Me.cboSize.Name = "cboSize"
        Me.cboSize.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cboSize.Size = New System.Drawing.Size(161, 22)
        Me.cboSize.TabIndex = 8
        '
        'StatusBar1
        '
        Me.StatusBar1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StatusBar1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusBar1_Panel1, Me.lblStep, Me.ProgressBar1})
        Me.StatusBar1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow
        Me.StatusBar1.Location = New System.Drawing.Point(0, 603)
        Me.StatusBar1.Name = "StatusBar1"
        Me.StatusBar1.Size = New System.Drawing.Size(817, 22)
        Me.StatusBar1.TabIndex = 6
        '
        '_StatusBar1_Panel1
        '
        Me._StatusBar1_Panel1.AutoSize = False
        Me._StatusBar1_Panel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._StatusBar1_Panel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me._StatusBar1_Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me._StatusBar1_Panel1.Name = "_StatusBar1_Panel1"
        Me._StatusBar1_Panel1.Size = New System.Drawing.Size(200, 22)
        Me._StatusBar1_Panel1.Spring = True
        Me._StatusBar1_Panel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Command1
        '
        Me.Command1.BackColor = System.Drawing.SystemColors.Control
        Me.Command1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Command1.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Command1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Command1.Location = New System.Drawing.Point(653, 531)
        Me.Command1.Name = "Command1"
        Me.Command1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Command1.Size = New System.Drawing.Size(125, 65)
        Me.Command1.TabIndex = 2
        Me.Command1.Text = "Run Tectonics"
        Me.Command1.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(0, -3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(800, 400)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 27
        Me.PictureBox1.TabStop = False
        '
        'txtMaxMtnHt
        '
        Me.txtMaxMtnHt.AcceptsReturn = True
        Me.txtMaxMtnHt.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaxMtnHt.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMaxMtnHt.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxMtnHt.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMaxMtnHt.Location = New System.Drawing.Point(199, 136)
        Me.txtMaxMtnHt.MaxLength = 0
        Me.txtMaxMtnHt.Name = "txtMaxMtnHt"
        Me.txtMaxMtnHt.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMaxMtnHt.Size = New System.Drawing.Size(45, 20)
        Me.txtMaxMtnHt.TabIndex = 6
        Me.txtMaxMtnHt.Text = "34000"
        Me.txtMaxMtnHt.Visible = False
        '
        'fraAltitudes
        '
        Me.fraAltitudes.BackColor = System.Drawing.SystemColors.Control
        Me.fraAltitudes.Controls.Add(Me.txtZSubsume)
        Me.fraAltitudes.Controls.Add(Me.txtZCoast)
        Me.fraAltitudes.Controls.Add(Me.txtZShelf)
        Me.fraAltitudes.Controls.Add(Me.txtMaxMtnHt)
        Me.fraAltitudes.Controls.Add(Me.txtZInit)
        Me.fraAltitudes.Controls.Add(Me.txtHydroPct)
        Me.fraAltitudes.Controls.Add(Me.txtMaxSteps)
        Me.fraAltitudes.Controls.Add(Me._Label1_2)
        Me.fraAltitudes.Controls.Add(Me._Label1_3)
        Me.fraAltitudes.Controls.Add(Me._Label1_4)
        Me.fraAltitudes.Controls.Add(Me._Label1_5)
        Me.fraAltitudes.Controls.Add(Me.Label8)
        Me.fraAltitudes.Controls.Add(Me.Label9)
        Me.fraAltitudes.Controls.Add(Me.Label15)
        Me.fraAltitudes.Controls.Add(Me.Label16)
        Me.fraAltitudes.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraAltitudes.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraAltitudes.Location = New System.Drawing.Point(0, 403)
        Me.fraAltitudes.Name = "fraAltitudes"
        Me.fraAltitudes.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraAltitudes.Size = New System.Drawing.Size(289, 161)
        Me.fraAltitudes.TabIndex = 44
        Me.fraAltitudes.TabStop = False
        Me.fraAltitudes.Text = "Parameters affecting altitude:"
        '
        'txtZSubsume
        '
        Me.txtZSubsume.AcceptsReturn = True
        Me.txtZSubsume.BackColor = System.Drawing.SystemColors.Window
        Me.txtZSubsume.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtZSubsume.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZSubsume.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZSubsume.Location = New System.Drawing.Point(243, 60)
        Me.txtZSubsume.MaxLength = 0
        Me.txtZSubsume.Name = "txtZSubsume"
        Me.txtZSubsume.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtZSubsume.Size = New System.Drawing.Size(25, 20)
        Me.txtZSubsume.TabIndex = 9
        Me.txtZSubsume.Text = "16"
        Me.ToolTip2.SetToolTip(Me.txtZSubsume, "Default 16")
        '
        'txtZCoast
        '
        Me.txtZCoast.AcceptsReturn = True
        Me.txtZCoast.BackColor = System.Drawing.SystemColors.Window
        Me.txtZCoast.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtZCoast.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZCoast.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZCoast.Location = New System.Drawing.Point(243, 84)
        Me.txtZCoast.MaxLength = 0
        Me.txtZCoast.Name = "txtZCoast"
        Me.txtZCoast.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtZCoast.Size = New System.Drawing.Size(25, 20)
        Me.txtZCoast.TabIndex = 8
        Me.txtZCoast.Text = "16"
        Me.ToolTip2.SetToolTip(Me.txtZCoast, "Default 16")
        '
        'txtZShelf
        '
        Me.txtZShelf.AcceptsReturn = True
        Me.txtZShelf.BackColor = System.Drawing.SystemColors.Window
        Me.txtZShelf.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtZShelf.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZShelf.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZShelf.Location = New System.Drawing.Point(243, 109)
        Me.txtZShelf.MaxLength = 0
        Me.txtZShelf.Name = "txtZShelf"
        Me.txtZShelf.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtZShelf.Size = New System.Drawing.Size(25, 20)
        Me.txtZShelf.TabIndex = 7
        Me.txtZShelf.Text = "8"
        Me.ToolTip2.SetToolTip(Me.txtZShelf, "Default 8")
        '
        'txtZInit
        '
        Me.txtZInit.AcceptsReturn = True
        Me.txtZInit.BackColor = System.Drawing.SystemColors.Window
        Me.txtZInit.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtZInit.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZInit.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZInit.Location = New System.Drawing.Point(243, 36)
        Me.txtZInit.MaxLength = 0
        Me.txtZInit.Name = "txtZInit"
        Me.txtZInit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtZInit.Size = New System.Drawing.Size(25, 20)
        Me.txtZInit.TabIndex = 5
        Me.txtZInit.Text = "22"
        Me.ToolTip2.SetToolTip(Me.txtZInit, "Default 22")
        '
        'txtHydroPct
        '
        Me.txtHydroPct.AcceptsReturn = True
        Me.txtHydroPct.BackColor = System.Drawing.SystemColors.Window
        Me.txtHydroPct.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtHydroPct.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtHydroPct.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtHydroPct.Location = New System.Drawing.Point(147, 16)
        Me.txtHydroPct.MaxLength = 0
        Me.txtHydroPct.Name = "txtHydroPct"
        Me.txtHydroPct.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtHydroPct.Size = New System.Drawing.Size(25, 20)
        Me.txtHydroPct.TabIndex = 4
        Me.txtHydroPct.Text = "75"
        '
        'txtMaxSteps
        '
        Me.txtMaxSteps.AcceptsReturn = True
        Me.txtMaxSteps.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaxSteps.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMaxSteps.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxSteps.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMaxSteps.Location = New System.Drawing.Point(95, 16)
        Me.txtMaxSteps.MaxLength = 0
        Me.txtMaxSteps.Name = "txtMaxSteps"
        Me.txtMaxSteps.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMaxSteps.Size = New System.Drawing.Size(25, 20)
        Me.txtMaxSteps.TabIndex = 3
        Me.txtMaxSteps.Text = "25"
        '
        '_Label1_2
        '
        Me._Label1_2.AutoSize = True
        Me._Label1_2.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_2.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_2, CType(2, Short))
        Me._Label1_2.Location = New System.Drawing.Point(87, 39)
        Me._Label1_2.Name = "_Label1_2"
        Me._Label1_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_2.Size = New System.Drawing.Size(158, 14)
        Me._Label1_2.TabIndex = 17
        Me._Label1_2.Text = "Initial altitude of supercontinent:"
        '
        '_Label1_3
        '
        Me._Label1_3.AutoSize = True
        Me._Label1_3.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_3.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_3, CType(3, Short))
        Me._Label1_3.Location = New System.Drawing.Point(17, 63)
        Me._Label1_3.Name = "_Label1_3"
        Me._Label1_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_3.Size = New System.Drawing.Size(230, 14)
        Me._Label1_3.TabIndex = 16
        Me._Label1_3.Text = "Altitude added to leading edge of drifting plate:"
        '
        '_Label1_4
        '
        Me._Label1_4.AutoSize = True
        Me._Label1_4.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_4.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_4, CType(4, Short))
        Me._Label1_4.Location = New System.Drawing.Point(189, 87)
        Me._Label1_4.Name = "_Label1_4"
        Me._Label1_4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_4.Size = New System.Drawing.Size(54, 14)
        Me._Label1_4.TabIndex = 15
        Me._Label1_4.Text = "Sea level:"
        '
        '_Label1_5
        '
        Me._Label1_5.AutoSize = True
        Me._Label1_5.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_5.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_5.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_5.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_5, CType(5, Short))
        Me._Label1_5.Location = New System.Drawing.Point(63, 112)
        Me._Label1_5.Name = "_Label1_5"
        Me._Label1_5.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_5.Size = New System.Drawing.Size(182, 14)
        Me._Label1_5.TabIndex = 14
        Me._Label1_5.Text = "Minimum altitude of continental shelf:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.SystemColors.Control
        Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label8.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label8.Location = New System.Drawing.Point(111, 139)
        Me.Label8.Name = "Label8"
        Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label8.Size = New System.Drawing.Size(91, 14)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "Maximum altitude:"
        Me.Label8.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.SystemColors.Control
        Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label9.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label9.Location = New System.Drawing.Point(247, 139)
        Me.Label9.Name = "Label9"
        Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label9.Size = New System.Drawing.Size(26, 14)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "feet"
        Me.Label9.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.SystemColors.Control
        Me.Label15.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label15.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label15.Location = New System.Drawing.Point(175, 19)
        Me.Label15.Name = "Label15"
        Me.Label15.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label15.Size = New System.Drawing.Size(49, 14)
        Me.Label15.TabIndex = 11
        Me.Label15.Text = "% water"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.BackColor = System.Drawing.SystemColors.Control
        Me.Label16.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label16.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label16.Location = New System.Drawing.Point(7, 19)
        Me.Label16.Name = "Label16"
        Me.Label16.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label16.Size = New System.Drawing.Size(90, 14)
        Me.Label16.TabIndex = 10
        Me.Label16.Text = "Number of steps:"
        '
        '_Label1_0
        '
        Me._Label1_0.AutoSize = True
        Me._Label1_0.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_0.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_0, CType(0, Short))
        Me._Label1_0.Location = New System.Drawing.Point(6, 87)
        Me._Label1_0.Name = "_Label1_0"
        Me._Label1_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_0.Size = New System.Drawing.Size(47, 14)
        Me._Label1_0.TabIndex = 36
        Me._Label1_0.Text = "Bend by"
        '
        '_Label1_13
        '
        Me._Label1_13.AutoSize = True
        Me._Label1_13.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_13.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_13.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_13, CType(13, Short))
        Me._Label1_13.Location = New System.Drawing.Point(8, 115)
        Me._Label1_13.Name = "_Label1_13"
        Me._Label1_13.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_13.Size = New System.Drawing.Size(119, 14)
        Me._Label1_13.TabIndex = 35
        Me._Label1_13.Text = "New plate base speed:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.SystemColors.Control
        Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label3.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label3.Location = New System.Drawing.Point(163, 115)
        Me.Label3.Name = "Label3"
        Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label3.Size = New System.Drawing.Size(39, 14)
        Me.Label3.TabIndex = 34
        Me.Label3.Text = "+ rnd *"
        '
        '_Label1_14
        '
        Me._Label1_14.AutoSize = True
        Me._Label1_14.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_14.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_14.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_14.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_14, CType(14, Short))
        Me._Label1_14.Location = New System.Drawing.Point(247, 87)
        Me._Label1_14.Name = "_Label1_14"
        Me._Label1_14.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_14.Size = New System.Drawing.Size(50, 14)
        Me._Label1_14.TabIndex = 33
        Me._Label1_14.Text = "squares."
        '
        '_Label1_7
        '
        Me._Label1_7.AutoSize = True
        Me._Label1_7.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_7.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_7.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_7.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_7, CType(7, Short))
        Me._Label1_7.Location = New System.Drawing.Point(8, 139)
        Me._Label1_7.Name = "_Label1_7"
        Me._Label1_7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_7.Size = New System.Drawing.Size(116, 14)
        Me._Label1_7.TabIndex = 32
        Me._Label1_7.Text = "Merge plates if at least"
        '
        '_Label1_8
        '
        Me._Label1_8.AutoSize = True
        Me._Label1_8.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_8.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_8.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_8.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_8, CType(8, Short))
        Me._Label1_8.Location = New System.Drawing.Point(159, 139)
        Me._Label1_8.Name = "_Label1_8"
        Me._Label1_8.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_8.Size = New System.Drawing.Size(128, 14)
        Me._Label1_8.TabIndex = 31
        Me._Label1_8.Text = "squares overlap, and the"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.SystemColors.Control
        Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label1.Location = New System.Drawing.Point(8, 163)
        Me.Label1.Name = "Label1"
        Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label1.Size = New System.Drawing.Size(153, 14)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "plates are moving slower than"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.SystemColors.Control
        Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label4.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label4.Location = New System.Drawing.Point(194, 163)
        Me.Label4.Name = "Label4"
        Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label4.Size = New System.Drawing.Size(93, 14)
        Me.Label4.TabIndex = 29
        Me.Label4.Text = "squares per step."
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.SystemColors.Control
        Me.Label10.Cursor = System.Windows.Forms.Cursors.Default
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label10.Location = New System.Drawing.Point(79, 87)
        Me.Label10.Name = "Label10"
        Me.Label10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label10.Size = New System.Drawing.Size(53, 14)
        Me.Label10.TabIndex = 28
        Me.Label10.Text = "� p / 2000"
        '
        '_Label1_1
        '
        Me._Label1_1.AutoSize = True
        Me._Label1_1.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_1, CType(1, Short))
        Me._Label1_1.Location = New System.Drawing.Point(135, 87)
        Me._Label1_1.Name = "_Label1_1"
        Me._Label1_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_1.Size = New System.Drawing.Size(74, 14)
        Me._Label1_1.TabIndex = 37
        Me._Label1_1.Text = "radians every"
        '
        '_Label1_10
        '
        Me._Label1_10.AutoSize = True
        Me._Label1_10.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_10.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_10.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_10.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_10, CType(10, Short))
        Me._Label1_10.Location = New System.Drawing.Point(8, 39)
        Me._Label1_10.Name = "_Label1_10"
        Me._Label1_10.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_10.Size = New System.Drawing.Size(204, 14)
        Me._Label1_10.TabIndex = 39
        Me._Label1_10.Text = "Number of tries to find an acceptable rift:"
        '
        '_Label1_9
        '
        Me._Label1_9.AutoSize = True
        Me._Label1_9.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_9.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_9.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_9.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_9, CType(9, Short))
        Me._Label1_9.Location = New System.Drawing.Point(8, 15)
        Me._Label1_9.Name = "_Label1_9"
        Me._Label1_9.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_9.Size = New System.Drawing.Size(255, 14)
        Me._Label1_9.TabIndex = 40
        Me._Label1_9.Text = "Percent chance that a rift will occur in a given step:"
        '
        '_Label1_12
        '
        Me._Label1_12.AutoSize = True
        Me._Label1_12.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_12.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_12.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_12, CType(12, Short))
        Me._Label1_12.Location = New System.Drawing.Point(8, 43)
        Me._Label1_12.Name = "_Label1_12"
        Me._Label1_12.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_12.Size = New System.Drawing.Size(164, 14)
        Me._Label1_12.TabIndex = 46
        Me._Label1_12.Text = "Rounding factor used in erosion:"
        '
        '_Label1_6
        '
        Me._Label1_6.AutoSize = True
        Me._Label1_6.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_6.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_6.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_6.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_6, CType(6, Short))
        Me._Label1_6.Location = New System.Drawing.Point(8, 67)
        Me._Label1_6.Name = "_Label1_6"
        Me._Label1_6.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_6.Size = New System.Drawing.Size(164, 14)
        Me._Label1_6.TabIndex = 45
        Me._Label1_6.Text = "Minimum altitude used in erosion:"
        '
        '_Label1_11
        '
        Me._Label1_11.AutoSize = True
        Me._Label1_11.BackColor = System.Drawing.SystemColors.Control
        Me._Label1_11.Cursor = System.Windows.Forms.Cursors.Default
        Me._Label1_11.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._Label1_11.ForeColor = System.Drawing.SystemColors.ControlText
        Me.LabelArray1.SetIndex(Me._Label1_11, CType(11, Short))
        Me._Label1_11.Location = New System.Drawing.Point(6, 63)
        Me._Label1_11.Name = "_Label1_11"
        Me._Label1_11.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._Label1_11.Size = New System.Drawing.Size(241, 14)
        Me._Label1_11.TabIndex = 38
        Me._Label1_11.Text = "Minimum distance between rift center and coast:"
        '
        'txtRiftDist
        '
        Me.txtRiftDist.AcceptsReturn = True
        Me.txtRiftDist.BackColor = System.Drawing.SystemColors.Window
        Me.txtRiftDist.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRiftDist.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRiftDist.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRiftDist.Location = New System.Drawing.Point(256, 60)
        Me.txtRiftDist.MaxLength = 0
        Me.txtRiftDist.Name = "txtRiftDist"
        Me.txtRiftDist.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRiftDist.Size = New System.Drawing.Size(41, 20)
        Me.txtRiftDist.TabIndex = 25
        Me.txtRiftDist.Text = "7"
        Me.ToolTip2.SetToolTip(Me.txtRiftDist, "Default 28")
        '
        'txtBendEvery
        '
        Me.txtBendEvery.AcceptsReturn = True
        Me.txtBendEvery.BackColor = System.Drawing.SystemColors.Window
        Me.txtBendEvery.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBendEvery.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBendEvery.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBendEvery.Location = New System.Drawing.Point(215, 84)
        Me.txtBendEvery.MaxLength = 0
        Me.txtBendEvery.Name = "txtBendEvery"
        Me.txtBendEvery.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBendEvery.Size = New System.Drawing.Size(29, 20)
        Me.txtBendEvery.TabIndex = 24
        Me.txtBendEvery.Text = "6"
        Me.ToolTip2.SetToolTip(Me.txtBendEvery, "Default 6")
        '
        'txtBendBy
        '
        Me.txtBendBy.AcceptsReturn = True
        Me.txtBendBy.BackColor = System.Drawing.SystemColors.Window
        Me.txtBendBy.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBendBy.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBendBy.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBendBy.Location = New System.Drawing.Point(52, 84)
        Me.txtBendBy.MaxLength = 0
        Me.txtBendBy.Name = "txtBendBy"
        Me.txtBendBy.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBendBy.Size = New System.Drawing.Size(33, 20)
        Me.txtBendBy.TabIndex = 23
        Me.txtBendBy.Text = "100"
        Me.ToolTip2.SetToolTip(Me.txtBendBy, "Default is 100 * pi/2000")
        '
        'txtSpeedBase
        '
        Me.txtSpeedBase.AcceptsReturn = True
        Me.txtSpeedBase.BackColor = System.Drawing.SystemColors.Window
        Me.txtSpeedBase.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSpeedBase.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpeedBase.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSpeedBase.Location = New System.Drawing.Point(124, 112)
        Me.txtSpeedBase.MaxLength = 0
        Me.txtSpeedBase.Name = "txtSpeedBase"
        Me.txtSpeedBase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSpeedBase.Size = New System.Drawing.Size(33, 20)
        Me.txtSpeedBase.TabIndex = 22
        Me.txtSpeedBase.Text = "200"
        Me.ToolTip2.SetToolTip(Me.txtSpeedBase, "Default 200")
        '
        'txtSpeedRng
        '
        Me.txtSpeedRng.AcceptsReturn = True
        Me.txtSpeedRng.BackColor = System.Drawing.SystemColors.Window
        Me.txtSpeedRng.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSpeedRng.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSpeedRng.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSpeedRng.Location = New System.Drawing.Point(207, 112)
        Me.txtSpeedRng.MaxLength = 0
        Me.txtSpeedRng.Name = "txtSpeedRng"
        Me.txtSpeedRng.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSpeedRng.Size = New System.Drawing.Size(37, 20)
        Me.txtSpeedRng.TabIndex = 21
        Me.txtSpeedRng.Text = "300"
        Me.ToolTip2.SetToolTip(Me.txtSpeedRng, "Default 300")
        '
        'txtMaxBump
        '
        Me.txtMaxBump.AcceptsReturn = True
        Me.txtMaxBump.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaxBump.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMaxBump.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxBump.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMaxBump.Location = New System.Drawing.Point(124, 136)
        Me.txtMaxBump.MaxLength = 0
        Me.txtMaxBump.Name = "txtMaxBump"
        Me.txtMaxBump.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMaxBump.Size = New System.Drawing.Size(29, 20)
        Me.txtMaxBump.TabIndex = 20
        Me.txtMaxBump.Text = "50"
        Me.ToolTip2.SetToolTip(Me.txtMaxBump, "Default 50")
        '
        'txtBumpTol
        '
        Me.txtBumpTol.AcceptsReturn = True
        Me.txtBumpTol.BackColor = System.Drawing.SystemColors.Window
        Me.txtBumpTol.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtBumpTol.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBumpTol.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtBumpTol.Location = New System.Drawing.Point(162, 160)
        Me.txtBumpTol.MaxLength = 0
        Me.txtBumpTol.Name = "txtBumpTol"
        Me.txtBumpTol.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtBumpTol.Size = New System.Drawing.Size(29, 20)
        Me.txtBumpTol.TabIndex = 19
        Me.txtBumpTol.Text = "50"
        Me.ToolTip2.SetToolTip(Me.txtBumpTol, "Default 50")
        '
        'txtErodeRnd
        '
        Me.txtErodeRnd.AcceptsReturn = True
        Me.txtErodeRnd.BackColor = System.Drawing.SystemColors.Window
        Me.txtErodeRnd.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtErodeRnd.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtErodeRnd.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtErodeRnd.Location = New System.Drawing.Point(168, 40)
        Me.txtErodeRnd.MaxLength = 0
        Me.txtErodeRnd.Name = "txtErodeRnd"
        Me.txtErodeRnd.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtErodeRnd.Size = New System.Drawing.Size(25, 20)
        Me.txtErodeRnd.TabIndex = 44
        Me.txtErodeRnd.Text = "4"
        Me.ToolTip2.SetToolTip(Me.txtErodeRnd, "Default 4")
        '
        'txtRiftPct
        '
        Me.txtRiftPct.AcceptsReturn = True
        Me.txtRiftPct.BackColor = System.Drawing.SystemColors.Window
        Me.txtRiftPct.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtRiftPct.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRiftPct.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtRiftPct.Location = New System.Drawing.Point(256, 12)
        Me.txtRiftPct.MaxLength = 0
        Me.txtRiftPct.Name = "txtRiftPct"
        Me.txtRiftPct.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtRiftPct.Size = New System.Drawing.Size(41, 20)
        Me.txtRiftPct.TabIndex = 27
        Me.txtRiftPct.Text = "80"
        Me.ToolTip2.SetToolTip(Me.txtRiftPct, "Default 40%")
        '
        'txtMaxCtrTry
        '
        Me.txtMaxCtrTry.AcceptsReturn = True
        Me.txtMaxCtrTry.BackColor = System.Drawing.SystemColors.Window
        Me.txtMaxCtrTry.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtMaxCtrTry.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMaxCtrTry.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtMaxCtrTry.Location = New System.Drawing.Point(250, 36)
        Me.txtMaxCtrTry.MaxLength = 0
        Me.txtMaxCtrTry.Name = "txtMaxCtrTry"
        Me.txtMaxCtrTry.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtMaxCtrTry.Size = New System.Drawing.Size(47, 20)
        Me.txtMaxCtrTry.TabIndex = 26
        Me.txtMaxCtrTry.Text = "100000"
        Me.ToolTip2.SetToolTip(Me.txtMaxCtrTry, "Default 50")
        '
        'txtZErode
        '
        Me.txtZErode.AcceptsReturn = True
        Me.txtZErode.BackColor = System.Drawing.SystemColors.Window
        Me.txtZErode.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtZErode.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtZErode.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtZErode.Location = New System.Drawing.Point(168, 64)
        Me.txtZErode.MaxLength = 0
        Me.txtZErode.Name = "txtZErode"
        Me.txtZErode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtZErode.Size = New System.Drawing.Size(25, 20)
        Me.txtZErode.TabIndex = 43
        Me.txtZErode.Text = "16"
        Me.ToolTip2.SetToolTip(Me.txtZErode, "Default 16")
        '
        'chkDoErode
        '
        Me.chkDoErode.BackColor = System.Drawing.SystemColors.Control
        Me.chkDoErode.Checked = True
        Me.chkDoErode.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkDoErode.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkDoErode.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkDoErode.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkDoErode.Location = New System.Drawing.Point(8, 20)
        Me.chkDoErode.Name = "chkDoErode"
        Me.chkDoErode.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkDoErode.Size = New System.Drawing.Size(86, 20)
        Me.chkDoErode.TabIndex = 42
        Me.chkDoErode.Text = "Do erosion"
        Me.chkDoErode.UseVisualStyleBackColor = False
        '
        'fraRifts
        '
        Me.fraRifts.BackColor = System.Drawing.SystemColors.Control
        Me.fraRifts.Controls.Add(Me.txtRiftPct)
        Me.fraRifts.Controls.Add(Me.txtMaxCtrTry)
        Me.fraRifts.Controls.Add(Me.txtRiftDist)
        Me.fraRifts.Controls.Add(Me.txtBendEvery)
        Me.fraRifts.Controls.Add(Me.txtBendBy)
        Me.fraRifts.Controls.Add(Me.txtSpeedBase)
        Me.fraRifts.Controls.Add(Me.txtSpeedRng)
        Me.fraRifts.Controls.Add(Me.txtMaxBump)
        Me.fraRifts.Controls.Add(Me.txtBumpTol)
        Me.fraRifts.Controls.Add(Me._Label1_9)
        Me.fraRifts.Controls.Add(Me._Label1_10)
        Me.fraRifts.Controls.Add(Me._Label1_11)
        Me.fraRifts.Controls.Add(Me._Label1_1)
        Me.fraRifts.Controls.Add(Me._Label1_0)
        Me.fraRifts.Controls.Add(Me._Label1_13)
        Me.fraRifts.Controls.Add(Me.Label3)
        Me.fraRifts.Controls.Add(Me._Label1_14)
        Me.fraRifts.Controls.Add(Me._Label1_7)
        Me.fraRifts.Controls.Add(Me._Label1_8)
        Me.fraRifts.Controls.Add(Me.Label1)
        Me.fraRifts.Controls.Add(Me.Label4)
        Me.fraRifts.Controls.Add(Me.Label10)
        Me.fraRifts.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraRifts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraRifts.Location = New System.Drawing.Point(295, 403)
        Me.fraRifts.Name = "fraRifts"
        Me.fraRifts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraRifts.Size = New System.Drawing.Size(305, 193)
        Me.fraRifts.TabIndex = 45
        Me.fraRifts.TabStop = False
        Me.fraRifts.Text = "Parameters affecting rifts:"
        '
        'fraErosion
        '
        Me.fraErosion.BackColor = System.Drawing.SystemColors.Control
        Me.fraErosion.Controls.Add(Me.txtErodeRnd)
        Me.fraErosion.Controls.Add(Me.txtZErode)
        Me.fraErosion.Controls.Add(Me.chkDoErode)
        Me.fraErosion.Controls.Add(Me._Label1_12)
        Me.fraErosion.Controls.Add(Me._Label1_6)
        Me.fraErosion.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fraErosion.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraErosion.Location = New System.Drawing.Point(606, 403)
        Me.fraErosion.Name = "fraErosion"
        Me.fraErosion.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraErosion.Size = New System.Drawing.Size(203, 93)
        Me.fraErosion.TabIndex = 46
        Me.fraErosion.TabStop = False
        Me.fraErosion.Text = "Parameters affecting erosion:"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(300, 16)
        Me.ProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'lblStep
        '
        Me.lblStep.Name = "lblStep"
        Me.lblStep.Size = New System.Drawing.Size(13, 17)
        Me.lblStep.Text = "0"
        '
        'frmTec
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(817, 625)
        Me.Controls.Add(Me.fraAltitudes)
        Me.Controls.Add(Me.fraRifts)
        Me.Controls.Add(Me.fraErosion)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.cboSize)
        Me.Controls.Add(Me.StatusBar1)
        Me.Controls.Add(Me.Command1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(3, 29)
        Me.MaximizeBox = False
        Me.Name = "frmTec"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Plate Tectonics Simulator (48 mi. resolution)"
        Me.StatusBar1.ResumeLayout(False)
        Me.StatusBar1.PerformLayout()
        CType(Me.Label5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Label7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fraAltitudes.ResumeLayout(False)
        Me.fraAltitudes.PerformLayout()
        CType(Me.LabelArray1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.fraRifts.ResumeLayout(False)
        Me.fraRifts.PerformLayout()
        Me.fraErosion.ResumeLayout(False)
        Me.fraErosion.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Public WithEvents txtMaxMtnHt As System.Windows.Forms.TextBox
    Public WithEvents fraAltitudes As System.Windows.Forms.GroupBox
    Public WithEvents txtZSubsume As System.Windows.Forms.TextBox
    Public WithEvents ToolTip2 As System.Windows.Forms.ToolTip
    Public WithEvents txtZCoast As System.Windows.Forms.TextBox
    Public WithEvents txtZShelf As System.Windows.Forms.TextBox
    Public WithEvents txtZInit As System.Windows.Forms.TextBox
    Public WithEvents txtHydroPct As System.Windows.Forms.TextBox
    Public WithEvents txtMaxSteps As System.Windows.Forms.TextBox
    Public WithEvents _Label1_2 As System.Windows.Forms.Label
    Public WithEvents LabelArray1 As Microsoft.VisualBasic.Compatibility.VB6.LabelArray
    Public WithEvents _Label1_3 As System.Windows.Forms.Label
    Public WithEvents _Label1_4 As System.Windows.Forms.Label
    Public WithEvents _Label1_5 As System.Windows.Forms.Label
    Public WithEvents Label8 As System.Windows.Forms.Label
    Public WithEvents Label9 As System.Windows.Forms.Label
    Public WithEvents Label15 As System.Windows.Forms.Label
    Public WithEvents Label16 As System.Windows.Forms.Label
    Public WithEvents _Label1_0 As System.Windows.Forms.Label
    Public WithEvents _Label1_13 As System.Windows.Forms.Label
    Public WithEvents Label3 As System.Windows.Forms.Label
    Public WithEvents _Label1_14 As System.Windows.Forms.Label
    Public WithEvents _Label1_7 As System.Windows.Forms.Label
    Public WithEvents _Label1_8 As System.Windows.Forms.Label
    Public WithEvents Label1 As System.Windows.Forms.Label
    Public WithEvents Label4 As System.Windows.Forms.Label
    Public WithEvents Label10 As System.Windows.Forms.Label
    Public WithEvents _Label1_1 As System.Windows.Forms.Label
    Public WithEvents _Label1_10 As System.Windows.Forms.Label
    Public WithEvents _Label1_9 As System.Windows.Forms.Label
    Public WithEvents _Label1_12 As System.Windows.Forms.Label
    Public WithEvents _Label1_6 As System.Windows.Forms.Label
    Public WithEvents _Label1_11 As System.Windows.Forms.Label
    Public WithEvents txtRiftDist As System.Windows.Forms.TextBox
    Public WithEvents txtBendEvery As System.Windows.Forms.TextBox
    Public WithEvents txtBendBy As System.Windows.Forms.TextBox
    Public WithEvents txtSpeedBase As System.Windows.Forms.TextBox
    Public WithEvents txtSpeedRng As System.Windows.Forms.TextBox
    Public WithEvents txtMaxBump As System.Windows.Forms.TextBox
    Public WithEvents txtBumpTol As System.Windows.Forms.TextBox
    Public WithEvents txtErodeRnd As System.Windows.Forms.TextBox
    Public WithEvents txtRiftPct As System.Windows.Forms.TextBox
    Public WithEvents txtMaxCtrTry As System.Windows.Forms.TextBox
    Public WithEvents txtZErode As System.Windows.Forms.TextBox
    Public WithEvents chkDoErode As System.Windows.Forms.CheckBox
    Public WithEvents fraRifts As System.Windows.Forms.GroupBox
    Public WithEvents fraErosion As System.Windows.Forms.GroupBox
    Friend WithEvents lblStep As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ToolStripProgressBar
#End Region 
End Class