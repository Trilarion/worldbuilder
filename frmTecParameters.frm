VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmTecParameters 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Parameters"
   ClientHeight    =   8130
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5355
   Icon            =   "frmTecParameters.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8130
   ScaleWidth      =   5355
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   315
      Left            =   3180
      TabIndex        =   2
      Top             =   7740
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Height          =   315
      Left            =   1020
      TabIndex        =   1
      Top             =   7740
      Width           =   1275
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   7575
      Left            =   60
      TabIndex        =   0
      Top             =   60
      Width           =   5235
      _ExtentX        =   9234
      _ExtentY        =   13361
      _Version        =   393216
      Tabs            =   4
      TabsPerRow      =   4
      TabHeight       =   520
      TabCaption(0)   =   "Elevation"
      TabPicture(0)   =   "frmTecParameters.frx":1BB2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "fraErosion"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "fraRifts"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "fraAltitudes"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Temperature"
      TabPicture(1)   =   "frmTecParameters.frx":1BCE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "fraTemperature"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      TabCaption(2)   =   "Pressure"
      TabPicture(2)   =   "frmTecParameters.frx":1BEA
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "fraPressure"
      Tab(2).Control(0).Enabled=   0   'False
      Tab(2).ControlCount=   1
      TabCaption(3)   =   "Rainfall"
      TabPicture(3)   =   "frmTecParameters.frx":1C06
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame1"
      Tab(3).Control(0).Enabled=   0   'False
      Tab(3).ControlCount=   1
      Begin VB.Frame Frame1 
         Caption         =   "Parameters affecting precipitation:"
         Height          =   4455
         Left            =   -74880
         TabIndex        =   100
         Top             =   480
         Width           =   4995
         Begin VB.TextBox txtNrFDel 
            Height          =   285
            Left            =   1500
            TabIndex        =   128
            Text            =   "-3"
            Top             =   3720
            Width           =   495
         End
         Begin VB.TextBox txtFlankDel 
            Height          =   285
            Left            =   1500
            TabIndex        =   124
            Text            =   "-24"
            Top             =   3060
            Width           =   495
         End
         Begin VB.TextBox txtNrHEqDel 
            Height          =   285
            Left            =   1500
            TabIndex        =   121
            Text            =   "24"
            Top             =   2760
            Width           =   495
         End
         Begin VB.TextBox txtHEqDel 
            Height          =   285
            Left            =   1500
            TabIndex        =   118
            Text            =   "32"
            Top             =   2400
            Width           =   495
         End
         Begin VB.TextBox txtFetchDel 
            Height          =   285
            Left            =   1500
            TabIndex        =   115
            Text            =   "4"
            Top             =   2040
            Width           =   495
         End
         Begin VB.TextBox txtMountDel 
            Height          =   285
            Left            =   1500
            TabIndex        =   111
            Text            =   "32"
            Top             =   1320
            Width           =   495
         End
         Begin VB.TextBox txtLandDel 
            Height          =   285
            Left            =   1500
            TabIndex        =   108
            Text            =   "-10"
            Top             =   1020
            Width           =   495
         End
         Begin VB.TextBox txtRainConst 
            Height          =   285
            Left            =   2160
            TabIndex        =   105
            Text            =   "32"
            Top             =   660
            Width           =   495
         End
         Begin VB.TextBox txtMaxFetch 
            Height          =   285
            Left            =   2520
            TabIndex        =   101
            Text            =   "11"
            Top             =   300
            Width           =   495
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "on a flank."
            Height          =   195
            Index           =   20
            Left            =   120
            TabIndex        =   130
            Top             =   4080
            Width           =   750
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "cm for each adjacent square that is"
            Height          =   195
            Index           =   19
            Left            =   2040
            TabIndex        =   129
            Top             =   3765
            Width           =   2490
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Increase rainfall by"
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   127
            Top             =   3765
            Width           =   1320
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "circular wind pattern.  This happens when the wind blows south."
            Height          =   195
            Index           =   18
            Left            =   120
            TabIndex        =   126
            Top             =   3420
            Width           =   4530
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "cm if the square is on the flank of a "
            Height          =   195
            Index           =   17
            Left            =   2040
            TabIndex        =   125
            Top             =   3105
            Width           =   2520
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Increase rainfall by"
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   123
            Top             =   3105
            Width           =   1320
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "cm if the square is near the heat equator."
            Height          =   195
            Index           =   16
            Left            =   2040
            TabIndex        =   122
            Top             =   2805
            Width           =   2895
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Increase rainfall by"
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   120
            Top             =   2805
            Width           =   1320
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "cm if the square is on the heat equator."
            Height          =   195
            Index           =   15
            Left            =   2040
            TabIndex        =   119
            Top             =   2445
            Width           =   2760
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Increase rainfall by"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   117
            Top             =   2445
            Width           =   1320
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "cm for each unit of fetch in the square."
            Height          =   195
            Index           =   14
            Left            =   2040
            TabIndex        =   116
            Top             =   2085
            Width           =   2730
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Increase rainfall by"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   114
            Top             =   2085
            Width           =   1320
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "stopped by a mountain."
            Height          =   195
            Index           =   13
            Left            =   120
            TabIndex        =   113
            Top             =   1680
            Width           =   1650
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "cm for each unit of fetch that is"
            Height          =   195
            Index           =   12
            Left            =   2040
            TabIndex        =   112
            Top             =   1365
            Width           =   2190
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Increase rainfall by"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   110
            Top             =   1365
            Width           =   1320
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "cm in every land or mountain square."
            Height          =   195
            Index           =   11
            Left            =   2040
            TabIndex        =   109
            Top             =   1065
            Width           =   2595
         End
         Begin VB.Label Label14 
            AutoSize        =   -1  'True
            Caption         =   "Increase rainfall by"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   107
            Top             =   1065
            Width           =   1320
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "cm"
            Height          =   195
            Index           =   10
            Left            =   2700
            TabIndex        =   106
            Top             =   705
            Width           =   210
         End
         Begin VB.Label Label13 
            AutoSize        =   -1  'True
            Caption         =   "Base rainfall in each square:"
            Height          =   195
            Left            =   120
            TabIndex        =   104
            Top             =   705
            Width           =   1995
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "squares"
            Height          =   195
            Index           =   9
            Left            =   3060
            TabIndex        =   103
            Top             =   345
            Width           =   555
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Maximum fetch (wind moistness):"
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   102
            Top             =   345
            Width           =   2310
         End
      End
      Begin VB.Frame fraPressure 
         Caption         =   "Parameters affecting pressure:"
         Height          =   6195
         Left            =   -74580
         TabIndex        =   65
         Top             =   540
         Width           =   4395
         Begin VB.TextBox txtBarSep 
            Height          =   285
            Left            =   1920
            TabIndex        =   99
            Text            =   "16"
            Top             =   5715
            Width           =   495
         End
         Begin VB.TextBox txtLOThresh 
            Height          =   285
            Left            =   780
            TabIndex        =   87
            Text            =   "6"
            Top             =   3240
            Width           =   375
         End
         Begin VB.TextBox txtLLThresh 
            Height          =   285
            Left            =   2940
            TabIndex        =   86
            Text            =   "15"
            Top             =   3540
            Width           =   495
         End
         Begin VB.TextBox txtLLMax 
            Height          =   285
            Left            =   3780
            TabIndex        =   85
            Text            =   "255"
            Top             =   4500
            Width           =   495
         End
         Begin VB.TextBox txtLLMin 
            Height          =   285
            Left            =   3780
            TabIndex        =   84
            Text            =   "220"
            Top             =   4155
            Width           =   495
         End
         Begin VB.TextBox txtLHMax 
            Height          =   285
            Left            =   3780
            TabIndex        =   83
            Text            =   "20"
            Top             =   5205
            Width           =   495
         End
         Begin VB.TextBox txtLHMin 
            Height          =   285
            Left            =   3780
            TabIndex        =   82
            Text            =   "0"
            Top             =   4860
            Width           =   495
         End
         Begin VB.TextBox txtOLThresh 
            Height          =   285
            Left            =   780
            TabIndex        =   71
            Text            =   "2"
            Top             =   480
            Width           =   375
         End
         Begin VB.TextBox txtOOThresh 
            Height          =   285
            Left            =   2940
            TabIndex        =   70
            Text            =   "11"
            Top             =   780
            Width           =   495
         End
         Begin VB.TextBox txtOLMax 
            Height          =   285
            Left            =   3780
            TabIndex        =   69
            Text            =   "65"
            Top             =   1740
            Width           =   495
         End
         Begin VB.TextBox txtOLMin 
            Height          =   285
            Left            =   3780
            TabIndex        =   68
            Text            =   "40"
            Top             =   1395
            Width           =   495
         End
         Begin VB.TextBox txtOHMax 
            Height          =   285
            Left            =   3780
            TabIndex        =   67
            Text            =   "180"
            Top             =   2445
            Width           =   495
         End
         Begin VB.TextBox txtOHMin 
            Height          =   285
            Left            =   3780
            TabIndex        =   66
            Text            =   "130"
            Top             =   2100
            Width           =   495
         End
         Begin VB.Label Label12 
            AutoSize        =   -1  'True
            Caption         =   "Isobar separation value:"
            Height          =   195
            Left            =   120
            TabIndex        =   98
            Top             =   5760
            Width           =   1695
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Land pressure zones ignore bodies or water with radius"
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   97
            Top             =   3000
            Width           =   3870
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "less than"
            Height          =   195
            Index           =   14
            Left            =   120
            TabIndex        =   96
            Top             =   3285
            Width           =   630
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "squares."
            Height          =   195
            Index           =   13
            Left            =   1260
            TabIndex        =   95
            Top             =   3285
            Width           =   600
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Land pressure zones must be at least"
            Height          =   195
            Index           =   8
            Left            =   120
            TabIndex        =   94
            Top             =   3600
            Width           =   2625
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "squares"
            Height          =   195
            Index           =   12
            Left            =   3480
            TabIndex        =   93
            Top             =   3600
            Width           =   555
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "from the nearest (non-ignored) bodies of water."
            Height          =   195
            Index           =   7
            Left            =   120
            TabIndex        =   92
            Top             =   3900
            Width           =   3285
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Land low-pressure maximum scaled temperature:"
            Height          =   195
            Index           =   7
            Left            =   120
            TabIndex        =   91
            Top             =   4545
            Width           =   3420
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Land low-pressure minimum scaled temperature:"
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   90
            Top             =   4200
            Width           =   3375
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Land high-pressure maximum scaled temperature:"
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   89
            Top             =   5250
            Width           =   3480
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Land high-pressure minimum scaled temperature:"
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   88
            Top             =   4905
            Width           =   3435
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Ocean pressure zones ignore land masses with radius"
            Height          =   195
            Index           =   4
            Left            =   120
            TabIndex        =   81
            Top             =   240
            Width           =   3780
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "less than"
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   80
            Top             =   525
            Width           =   630
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "squares."
            Height          =   195
            Index           =   10
            Left            =   1260
            TabIndex        =   79
            Top             =   525
            Width           =   600
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Ocean pressure zones must be at least"
            Height          =   195
            Index           =   5
            Left            =   120
            TabIndex        =   78
            Top             =   840
            Width           =   2745
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "squares"
            Height          =   195
            Index           =   11
            Left            =   3480
            TabIndex        =   77
            Top             =   840
            Width           =   555
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "from the nearest (non-ignored) landmass."
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   76
            Top             =   1140
            Width           =   2865
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Ocean low-pressure maximum scaled temperature:"
            Height          =   195
            Index           =   1
            Left            =   120
            TabIndex        =   75
            Top             =   1785
            Width           =   3540
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Ocean low-pressure minimum scaled temperature:"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   74
            Top             =   1440
            Width           =   3495
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Ocean high-pressure maximum scaled temperature:"
            Height          =   195
            Index           =   2
            Left            =   120
            TabIndex        =   73
            Top             =   2490
            Width           =   3600
         End
         Begin VB.Label Label11 
            AutoSize        =   -1  'True
            Caption         =   "Ocean high-pressure minimum scaled temperature:"
            Height          =   195
            Index           =   3
            Left            =   120
            TabIndex        =   72
            Top             =   2145
            Width           =   3555
         End
      End
      Begin VB.Frame fraTemperature 
         Caption         =   "Parameters affecting temperature:"
         Height          =   3075
         Left            =   -74400
         TabIndex        =   39
         Top             =   660
         Width           =   3915
         Begin VB.TextBox txtEccPhase 
            Height          =   285
            Left            =   2220
            TabIndex        =   53
            Text            =   "1.0"
            Top             =   960
            Width           =   555
         End
         Begin VB.TextBox txtEccentricity 
            Height          =   285
            Left            =   2220
            TabIndex        =   51
            Text            =   "0.0167"
            Top             =   600
            Width           =   555
         End
         Begin VB.TextBox txtTilt 
            Height          =   285
            Left            =   2220
            TabIndex        =   49
            Text            =   "23.0"
            Top             =   300
            Width           =   555
         End
         Begin VB.TextBox txtOCos 
            Height          =   285
            Left            =   2220
            TabIndex        =   47
            Text            =   "50.0"
            Top             =   2400
            Width           =   615
         End
         Begin VB.TextBox txtOConst 
            Height          =   285
            Left            =   2220
            TabIndex        =   45
            Text            =   "285.0"
            Top             =   2040
            Width           =   615
         End
         Begin VB.TextBox txtLCos 
            Height          =   285
            Left            =   2220
            TabIndex        =   43
            Text            =   "84.0"
            Top             =   1680
            Width           =   675
         End
         Begin VB.TextBox txtLConst 
            Height          =   285
            Left            =   2220
            TabIndex        =   41
            Text            =   "275.0"
            Top             =   1320
            Width           =   675
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "radians"
            Height          =   195
            Index           =   8
            Left            =   3060
            TabIndex        =   63
            Top             =   1020
            Width           =   510
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Kelvin"
            Height          =   195
            Index           =   7
            Left            =   2880
            TabIndex        =   62
            Top             =   2460
            Width           =   435
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Kelvin"
            Height          =   195
            Index           =   6
            Left            =   2880
            TabIndex        =   61
            Top             =   2100
            Width           =   435
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Kelvin"
            Height          =   195
            Index           =   5
            Left            =   2940
            TabIndex        =   60
            Top             =   1740
            Width           =   435
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Kelvin"
            Height          =   195
            Index           =   4
            Left            =   2940
            TabIndex        =   59
            Top             =   1380
            Width           =   435
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "degrees"
            Height          =   195
            Index           =   3
            Left            =   2820
            TabIndex        =   58
            Top             =   360
            Width           =   570
         End
         Begin VB.Label Label7 
            AutoSize        =   -1  'True
            Caption         =   "� p"
            BeginProperty Font 
               Name            =   "Symbol"
               Size            =   8.25
               Charset         =   2
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   2820
            TabIndex        =   54
            Top             =   1020
            Width           =   210
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Eccentricity phase:"
            Height          =   195
            Index           =   2
            Left            =   795
            TabIndex        =   52
            Top             =   1005
            Width           =   1350
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Orbital Eccentricity:"
            Height          =   195
            Index           =   1
            Left            =   780
            TabIndex        =   50
            Top             =   645
            Width           =   1365
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Axial tilt:"
            Height          =   195
            Index           =   0
            Left            =   1575
            TabIndex        =   48
            Top             =   345
            Width           =   570
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Ocean temperature range:"
            Height          =   195
            Index           =   3
            Left            =   285
            TabIndex        =   46
            Top             =   2460
            Width           =   1860
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Ocean mean temperature:"
            Height          =   195
            Index           =   2
            Left            =   300
            TabIndex        =   44
            Top             =   2100
            Width           =   1845
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Land temperature range:"
            Height          =   195
            Index           =   1
            Left            =   405
            TabIndex        =   42
            Top             =   1680
            Width           =   1740
         End
         Begin VB.Label Label6 
            AutoSize        =   -1  'True
            Caption         =   "Land mean temperature:"
            Height          =   195
            Index           =   0
            Left            =   420
            TabIndex        =   40
            Top             =   1320
            Width           =   1725
         End
      End
      Begin VB.Frame fraAltitudes 
         Caption         =   "Parameters affecting altitude:"
         Height          =   2415
         Left            =   480
         TabIndex        =   31
         Top             =   480
         Width           =   4215
         Begin VB.TextBox txtMaxSteps 
            Height          =   285
            Left            =   1800
            TabIndex        =   135
            Text            =   "25"
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox txtHydroPct 
            Height          =   285
            Left            =   2640
            TabIndex        =   132
            Text            =   "75"
            Top             =   240
            Width           =   375
         End
         Begin VB.TextBox txtZInit 
            Height          =   285
            Left            =   3600
            TabIndex        =   131
            Text            =   "22"
            ToolTipText     =   "Default 22"
            Top             =   540
            Width           =   375
         End
         Begin VB.TextBox txtMaxMtnHt 
            Height          =   285
            Left            =   2940
            TabIndex        =   56
            Text            =   "34000"
            Top             =   2040
            Visible         =   0   'False
            Width           =   675
         End
         Begin VB.TextBox txtZShelf 
            Height          =   285
            Left            =   3600
            TabIndex        =   38
            Text            =   "8"
            ToolTipText     =   "Default 8"
            Top             =   1635
            Width           =   375
         End
         Begin VB.TextBox txtZCoast 
            Height          =   285
            Left            =   3600
            TabIndex        =   36
            Text            =   "16"
            ToolTipText     =   "Default 16"
            Top             =   1260
            Width           =   375
         End
         Begin VB.TextBox txtZSubsume 
            Height          =   285
            Left            =   3600
            TabIndex        =   34
            Text            =   "16"
            ToolTipText     =   "Default 16"
            Top             =   900
            Width           =   375
         End
         Begin VB.Label Label16 
            AutoSize        =   -1  'True
            Caption         =   "Number of steps:"
            Height          =   195
            Left            =   540
            TabIndex        =   134
            Top             =   285
            Width           =   1200
         End
         Begin VB.Label Label15 
            AutoSize        =   -1  'True
            Caption         =   "% water"
            Height          =   195
            Left            =   3060
            TabIndex        =   133
            Top             =   285
            Width           =   555
         End
         Begin VB.Label Label9 
            AutoSize        =   -1  'True
            Caption         =   "feet"
            Height          =   195
            Left            =   3660
            TabIndex        =   57
            Top             =   2085
            Visible         =   0   'False
            Width           =   270
         End
         Begin VB.Label Label8 
            AutoSize        =   -1  'True
            Caption         =   "Maximum altitude:"
            Height          =   195
            Left            =   1620
            TabIndex        =   55
            Top             =   2085
            Visible         =   0   'False
            Width           =   1260
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Minimum altitude of continental shelf:"
            Height          =   195
            Index           =   5
            Left            =   900
            TabIndex        =   37
            Top             =   1680
            Width           =   2595
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Sea level:"
            Height          =   195
            Index           =   4
            Left            =   2790
            TabIndex        =   35
            Top             =   1305
            Width           =   705
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Altitude added to leading edge of drifting plate:"
            Height          =   195
            Index           =   3
            Left            =   210
            TabIndex        =   33
            Top             =   945
            Width           =   3285
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Initial altitude of supercontinent:"
            Height          =   195
            Index           =   2
            Left            =   1260
            TabIndex        =   32
            Top             =   585
            Width           =   2235
         End
      End
      Begin VB.Frame fraRifts 
         Caption         =   "Parameters affecting rifts:"
         Height          =   2895
         Left            =   480
         TabIndex        =   8
         Top             =   2940
         Width           =   4575
         Begin VB.TextBox txtBumpTol 
            Height          =   285
            Left            =   2280
            TabIndex        =   27
            Text            =   "50"
            ToolTipText     =   "Default 50"
            Top             =   2400
            Width           =   435
         End
         Begin VB.TextBox txtMaxBump 
            Height          =   285
            Left            =   1800
            TabIndex        =   25
            Text            =   "50"
            ToolTipText     =   "Default 50"
            Top             =   2040
            Width           =   435
         End
         Begin VB.TextBox txtSpeedRng 
            Height          =   285
            Left            =   2880
            TabIndex        =   22
            Text            =   "300"
            ToolTipText     =   "Default 300"
            Top             =   1680
            Width           =   555
         End
         Begin VB.TextBox txtSpeedBase 
            Height          =   285
            Left            =   1860
            TabIndex        =   20
            Text            =   "200"
            ToolTipText     =   "Default 200"
            Top             =   1680
            Width           =   495
         End
         Begin VB.TextBox txtBendBy 
            Height          =   285
            Left            =   780
            TabIndex        =   16
            Text            =   "100"
            ToolTipText     =   "Default is 100 * pi/2000"
            Top             =   1260
            Width           =   495
         End
         Begin VB.TextBox txtBendEvery 
            Height          =   285
            Left            =   3000
            TabIndex        =   15
            Text            =   "6"
            ToolTipText     =   "Default 6"
            Top             =   1260
            Width           =   435
         End
         Begin VB.TextBox txtRiftDist 
            Height          =   285
            Left            =   3780
            TabIndex        =   14
            Text            =   "28"
            ToolTipText     =   "Default 28"
            Top             =   900
            Width           =   675
         End
         Begin VB.TextBox txtMaxCtrTry 
            Height          =   285
            Left            =   3480
            TabIndex        =   12
            Text            =   "50"
            ToolTipText     =   "Default 50"
            Top             =   540
            Width           =   675
         End
         Begin VB.TextBox txtRiftPct 
            Height          =   285
            Left            =   3780
            TabIndex        =   10
            Text            =   "40"
            ToolTipText     =   "Default 40%"
            Top             =   180
            Width           =   675
         End
         Begin VB.Label Label10 
            AutoSize        =   -1  'True
            Caption         =   "� p / 2000"
            BeginProperty Font 
               Name            =   "Symbol"
               Size            =   8.25
               Charset         =   2
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   1320
            TabIndex        =   64
            Top             =   1305
            Width           =   615
         End
         Begin VB.Label Label4 
            AutoSize        =   -1  'True
            Caption         =   "squares per step."
            Height          =   195
            Left            =   2760
            TabIndex        =   30
            Top             =   2445
            Width           =   1215
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "plates are moving slower than"
            Height          =   195
            Left            =   120
            TabIndex        =   28
            Top             =   2445
            Width           =   2100
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "squares overlap, and the"
            Height          =   195
            Index           =   8
            Left            =   2280
            TabIndex        =   26
            Top             =   2100
            Width           =   1755
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Merge plates if at least"
            Height          =   195
            Index           =   7
            Left            =   120
            TabIndex        =   24
            Top             =   2085
            Width           =   1590
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "squares."
            Height          =   195
            Index           =   14
            Left            =   3480
            TabIndex        =   23
            Top             =   1305
            Width           =   600
         End
         Begin VB.Label Label3 
            AutoSize        =   -1  'True
            Caption         =   "+ rnd *"
            Height          =   195
            Left            =   2340
            TabIndex        =   21
            Top             =   1725
            Width           =   465
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "New plate base speed:"
            Height          =   195
            Index           =   13
            Left            =   120
            TabIndex        =   19
            Top             =   1725
            Width           =   1635
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Bend by"
            Height          =   195
            Index           =   0
            Left            =   120
            TabIndex        =   18
            Top             =   1305
            Width           =   585
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "radians every"
            Height          =   195
            Index           =   1
            Left            =   1980
            TabIndex        =   17
            Top             =   1305
            Width           =   945
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Minimum distance between rift center and coast:"
            Height          =   195
            Index           =   11
            Left            =   120
            TabIndex        =   13
            Top             =   945
            Width           =   3420
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Number of tries to find an acceptable rift:"
            Height          =   195
            Index           =   10
            Left            =   120
            TabIndex        =   11
            Top             =   585
            Width           =   2865
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Percent chance that a rift will occur in a given step:"
            Height          =   195
            Index           =   9
            Left            =   120
            TabIndex        =   9
            Top             =   225
            Width           =   3630
         End
      End
      Begin VB.Frame fraErosion 
         Caption         =   "Parameters affecting erosion:"
         Height          =   1395
         Left            =   480
         TabIndex        =   3
         Top             =   5940
         Width           =   4275
         Begin VB.CheckBox chkDoErode 
            Caption         =   "Do erosion"
            Height          =   255
            Left            =   120
            TabIndex        =   29
            Top             =   300
            Value           =   1  'Checked
            Width           =   1155
         End
         Begin VB.TextBox txtZErode 
            Height          =   285
            Left            =   2520
            TabIndex        =   7
            Text            =   "16"
            ToolTipText     =   "Default 16"
            Top             =   960
            Width           =   375
         End
         Begin VB.TextBox txtErodeRnd 
            Height          =   285
            Left            =   2520
            TabIndex        =   5
            Text            =   "4"
            ToolTipText     =   "Default 4"
            Top             =   600
            Width           =   375
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Minimum altitude used in erosion:"
            Height          =   195
            Index           =   6
            Left            =   120
            TabIndex        =   6
            Top             =   1005
            Width           =   2325
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Rounding factor used in erosion:"
            Height          =   195
            Index           =   12
            Left            =   120
            TabIndex        =   4
            Top             =   645
            Width           =   2295
         End
      End
   End
End
Attribute VB_Name = "frmTecParameters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdOK_Click()
  Set frmTec.tec = New CTec
  Set frmTec.clim = New CClim
  Set frmTec.frac = New CCollageFractal
  
  With frmTec.tec
    .BendBy = CLng(txtBendBy.Text)
    .BendEvery = CLng(txtBendEvery.Text)
    .BumpTol = CLng(txtBumpTol.Text)
    If chkDoErode.Value = Checked Then
      .DoErode = 1
    Else
      .DoErode = 0
    End If
    .DrawEvery = 1
    .ErodeRnd = CLng(txtErodeRnd.Text)
    .ExportBitmap = False
    .HydroPct = CLng(txtHydroPct.Text)
    .MaxBump = CLng(txtMaxBump.Text)
    .MaxCtrTry = CLng(txtMaxCtrTry.Text)
    .MaxStep = CLng(txtMaxSteps.Text)
    .RiftDist = CLng(txtRiftDist.Text)
    .RiftPct = CLng(txtRiftPct.Text)
    .SpeedBase = CLng(txtSpeedBase.Text)
    .SpeedRng = CLng(txtSpeedRng.Text)
    .ZCoast = CLng(txtZCoast.Text)
    .ZErode = CLng(txtZErode.Text)
    .ZInit = CLng(txtZInit.Text)
    .ZShelf = CLng(txtZShelf.Text)
    .ZSubsume = CLng(txtZSubsume.Text)
  End With
  
  With frmTec.frac
    .ZCoast = frmTec.tec.ZCoast
    .ZShelf = frmTec.tec.ZShelf
  End With
  
  With frmTec.clim
    .ZCoast = frmTec.tec.ZCoast
    .ZShelf = frmTec.tec.ZShelf
    
    .BarSep = CLng(txtBarSep.Text)
    .BSize = 12
    .eccentricity = CDbl(txtEccentricity.Text)
    .EccPhase = CDbl(txtEccPhase.Text) * PI
    .FetchDelta = CLng(txtFetchDel.Text)
    .FlankDelta = CLng(txtFlankDel.Text)
    .HeatEquatorDelta = CLng(txtHEqDel.Text)
    .LandDelta = CSng(txtLandDel.Text)
    .LandTemperatureMean = CDbl(txtLConst.Text)
    .LandTemperatureRange = CDbl(txtLCos.Text)
    .LHMax = CLng(txtLHMax.Text)
    .LHMin = CLng(txtLHMin.Text)
    .LLMax = CLng(txtLLMax.Text)
    .LLMin = CLng(txtLLMin.Text)
    .LLThresh = CLng(txtLLThresh.Text)
    .LOThresh = CLng(txtLOThresh.Text)
    .MaxFetch = CLng(txtMaxFetch.Text)
    .MountainDelta = CLng(txtMountDel.Text)
    .NearFlankDelta = CLng(txtNrFDel.Text)
    .NearHeatEquatorDelta = CLng(txtNrHEqDel.Text)
    .OceanTemperatureMean = CDbl(txtOConst.Text)
    .OceanTemperatureRange = CDbl(txtOCos.Text)
    .OHMax = CLng(txtOHMax.Text)
    .OHMin = CLng(txtOHMin.Text)
    .OLMax = CLng(txtOLMax.Text)
    .OLMin = CLng(txtOLMin.Text)
    .OLThresh = CLng(txtOLThresh.Text)
    .OOThresh = CLng(txtOOThresh.Text)
    .RainConst = CLng(txtRainConst.Text)
    .Tilt = CDbl(txtTilt.Text)
  End With
  frmTec.cmdClimateRun.Enabled = False
  
  Unload Me
End Sub

Private Sub Form_Load()

  With frmTec.tec
    txtBendBy.Text = CStr(.BendBy)
    txtBendEvery.Text = CStr(.BendEvery)
    txtBumpTol.Text = CStr(.BumpTol)
    If .DoErode = 1 Then
      chkDoErode.Value = Checked
    Else
      chkDoErode.Value = Unchecked
    End If
    txtErodeRnd.Text = CStr(.ErodeRnd)
    .DrawEvery = 1
    .ExportBitmap = False
    txtHydroPct.Text = CStr(.HydroPct)
    txtMaxBump.Text = CStr(.MaxBump)
    txtMaxCtrTry.Text = CStr(.MaxCtrTry)
    txtMaxSteps.Text = CStr(.MaxStep)
    txtRiftDist.Text = CStr(.RiftDist)
    txtRiftPct.Text = CStr(.RiftPct)
    txtSpeedBase.Text = CStr(.SpeedBase)
    txtSpeedRng.Text = CStr(.SpeedRng)
    txtZCoast.Text = CStr(.ZCoast)
    txtZErode.Text = CStr(.ZErode)
    txtZInit.Text = CStr(.ZInit)
    txtZShelf.Text = CStr(.ZShelf)
    txtZSubsume.Text = CStr(.ZSubsume)
  End With
  
  With frmTec.frac
    frmTec.tec.ZCoast = CStr(.ZCoast)
    frmTec.tec.ZShelf = CStr(.ZShelf)
  End With
  
  With frmTec.clim
    frmTec.tec.ZCoast = CStr(.ZCoast)
    frmTec.tec.ZShelf = CStr(.ZShelf)
    
    txtBarSep.Text = CStr(.BarSep)
    .BSize = 12
    txtEccentricity.Text = CStr(.eccentricity)
    txtEccPhase.Text = CStr(.EccPhase / PI)
    txtFetchDel.Text = CStr(.FetchDelta)
    txtFlankDel.Text = CStr(.FlankDelta)
    txtHEqDel.Text = CStr(.HeatEquatorDelta)
    txtLandDel.Text = CStr(.LandDelta)
    txtLConst.Text = CStr(.LandTemperatureMean)
    txtLCos.Text = CStr(.LandTemperatureRange)
    txtLHMax.Text = CStr(.LHMax)
    txtLHMin.Text = CStr(.LHMin)
    txtLLMax.Text = CStr(.LLMax)
    txtLLMin.Text = CStr(.LLMin)
    txtLLThresh.Text = CStr(.LLThresh)
    txtLOThresh.Text = CStr(.LOThresh)
    txtMaxFetch.Text = CStr(.MaxFetch)
    txtMountDel.Text = CStr(.MountainDelta)
    txtNrFDel.Text = CStr(.NearFlankDelta)
    txtNrHEqDel.Text = CStr(.NearHeatEquatorDelta)
    txtOConst.Text = CStr(.OceanTemperatureMean)
    txtOCos.Text = CStr(.OceanTemperatureRange)
    txtOHMax.Text = CStr(.OHMax)
    txtOHMin.Text = CStr(.OHMin)
    txtOLMax.Text = CStr(.OLMax)
    txtOLMin.Text = CStr(.OLMin)
    txtOLThresh.Text = CStr(.OLThresh)
    txtOOThresh.Text = CStr(.OOThresh)
    txtRainConst.Text = (.RainConst)
    txtTilt.Text = CStr(.Tilt)
  End With
  
End Sub

